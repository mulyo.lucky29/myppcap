# My PPCAP
##
PPCAP adalah aplikasi mobile yang menghandle proses inventarisir item sparepart terutama untuk pengambilan gambar item agar lebih informatif di system. Sebelumnya digunakan kamera SLR untuk memfoto item tsb dan manual upload satu per satu ke system, ada potensi gambar item tertukar saat upload. Dengan adanya aplikasi ini process update/capture photo item akan di permudah dan lebih cepat serta tidak akan tertukar. juga dilengkapi dengan assign task list ke orang tertentu (login) untuk dibagi tugas mengambil gambar item agar pengerjaannya lebih cepat dan tidak redundant.
 
## Screen Example 
login <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myppcap/myppcap_login.png?raw=true)

menu system <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myppcap/myppcap_menu.png?raw=true)

load task pengambilan gambar item <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myppcap/myppcap_loadtask.png?raw=true)

proses pengambilan photo item <br>
bisa menggunakan foto yang sudah ada (dari gallery) atau ambil photo langsung dari lensa kamera device <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myppcap/myppcap_takepicture.png?raw=true)

## License
**Copyright 2022 Lucky Mulyo**

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.