package com.gudanggaramtbk.myppcap;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * An activity representing a list of Batches. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link BatchDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */

public class BatchListActivity extends AppCompatActivity {
    private SharedPreferences           config;
    private MySQLiteHelper              dbHelper;
    private String                      StrSessionNIK;
    private String                      StrSessionName;
    private String                      StrFileID;
    private String                      TaskDID;
    private boolean                     mTwoPane;
    private List<DS_TaskItemDetail>           ITEMS;
    private Map<String, DS_TaskItemDetail>    ITEM_MAP;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Load Task Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra("PIC_NIK" , StrSessionNIK);
                upIntent.putExtra("PIC_NAME", StrSessionName);
                upIntent.putExtra("PIC_NAME", StrFileID);

                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    // end of procedure onOptionsItemSelected

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        // load list
        ITEMS = dbHelper.get_RouteInfoBy(StrSessionNIK,"FileID",StrFileID,"");
        Log.d("[GudangGaram]", "Load List FileID (" + StrFileID + ") " + ITEMS.size());
        ITEM_MAP = new HashMap<String, DS_TaskItemDetail>();

        if(ITEMS.size() > 0){
            for (int i=0; i<ITEMS.size(); i++) {
                DS_TaskItemDetail omap = new DS_TaskItemDetail();
                omap.setTaskDID(ITEMS.get(i).getTaskDID());
                omap.setSeq(ITEMS.get(i).getSeq());
                omap.setLocRak(ITEMS.get(i).getLocRak());
                omap.setLocBin(ITEMS.get(i).getLocBin());
                omap.setLocCom(ITEMS.get(i).getLocCom());
                omap.setIsClosed(ITEMS.get(i).getIsClosed());

                ITEM_MAP.put(ITEMS.get(i).getTaskDID().toString(),omap);
                Log.d("[GudangGaram]", "Map List (" + Integer.toString(i) + ") : " + ITEMS.get(i).getTaskDID());
            }
        }
        // render list navigation
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(ITEMS));
    }
    // end of procedure setupRecyclerView

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batch_list);
        config = getSharedPreferences("MyPPCAPSetting", MODE_PRIVATE);

        // get information intent
        StrSessionName   = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK    = getIntent().getStringExtra("PIC_NIK");
        StrFileID        = getIntent().getStringExtra("FILE_ID");

        Log.d("[GudangGaram]", "BatchListActivity Parameters ");
        Log.d("[GudangGaram]", "=============================");
        Log.d("[GudangGaram]", "PIC_NIK  : " + StrSessionNIK);
        Log.d("[GudangGaram]", "PIC NAME : " + StrSessionName);
        Log.d("[GudangGaram]", "FILE_ID  : " + StrFileID);
        Log.d("[GudangGaram]", "=============================");

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // render navigation list
        View recyclerView = findViewById(R.id.batch_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
        if (findViewById(R.id.batch_detail_container) != null) {
            // The detail container view will be present only in the large-screen layouts (res/values-w900dp). If this view is present, then the activity should be in two-pane mode.
            mTwoPane = true;
            Log.d("[GudangGaram]", "Two Pane Master Detail");
        }
        else{
            Log.d("[GudangGaram]", "One Pane Master Detail");
            TextView BatchFileID = (TextView)findViewById(R.id.txt_batch_fileId);
            BatchFileID.setText(StrFileID);
        }

    }
    // end of procedure onCreate

    public void msgbox(String pTitle, String pMessage, Context ctx){
        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
            alertDialogBuilder
                    .setTitle(pTitle)
                    .setMessage(pMessage)
                    .setCancelable(false)
                    .setNegativeButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
        }
    }

    public class SimpleItemRecyclerViewAdapter extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {
        private final List<DS_TaskItemDetail> listRoute;

        public SimpleItemRecyclerViewAdapter(List<DS_TaskItemDetail> olist) {
            listRoute = olist;
        }

        @Override
        public int getItemCount() {
            return listRoute.size();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.batch_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            String xdid;
            String xseq;
            String xbin;
            String xrak;
            String xcomp;
            String xflag;


            holder.mItem = listRoute.get(position);
            xdid   = listRoute.get(position).getTaskDID().toString();
            xseq   = listRoute.get(position).getSeq().toString();
            xbin   = listRoute.get(position).getLocBin().toString();
            xrak   = listRoute.get(position).getLocRak().toString();
            xcomp  = listRoute.get(position).getLocCom().toString();
            xflag  = listRoute.get(position).getIsClosed().toString();

            Log.d("[GudangGaram]:", "onBindViewHolder  >> [xdid] [xSeq] [xRak] [xBin] [xCom] : [" + xdid +  "] [" + xseq + "] [" + xrak + "] [" + xbin + "] [" + xcomp + "]");

            holder.mDid.setText(xdid);
            holder.mSeq.setText(xseq);
            holder.mRak.setText(xrak);
            holder.mBin.setText(xbin);
            holder.mComp.setText(xcomp);
            holder.mFlag.setText(xflag);

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String TaskDID;
                    Integer SelectedSeq, NextSeq;
                    // this is key to lookup
                    TaskDID     = holder.mItem.getTaskDID().toString();
                    SelectedSeq = Integer.parseInt(holder.mItem.getSeq().toString());
                    NextSeq     = dbHelper.get_NextRouteToClose(TaskDID);

                    // if there is attempt to access jump other than sequence next route then prohibited
                    Log.d("[GudangGaram]", "BatchListActivity :: mPane Current : >> " + SelectedSeq + " Next : " + NextSeq);


                    if(SelectedSeq <= NextSeq){
                        if (mTwoPane) {
                            // sending TaskDID to fragment
                            Log.d("[GudangGaram]", "mTwoPane >>");
                            Bundle arguments = new Bundle();
                            arguments.putString("PIC_NAME",StrSessionName);
                            arguments.putString("PIC_NIK" ,StrSessionNIK);
                            arguments.putString("FILE_ID" ,StrFileID);
                            arguments.putString(BatchDetailFragment.ARG_TASK_DID, TaskDID);

                            BatchDetailFragment fragment = new BatchDetailFragment();
                            fragment.setArguments(arguments);
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.batch_detail_container, fragment)
                                    .commit();
                        } else {
                            Log.d("[GudangGaram]", "mOnePane >>");
                            Context context = v.getContext();
                            Intent intent = new Intent(context, BatchDetailActivity.class);
                            // sending TaskDID to fragment
                            intent.putExtra("PIC_NAME",StrSessionName);
                            intent.putExtra("PIC_NIK" ,StrSessionNIK);
                            intent.putExtra("FILE_ID" ,StrFileID);
                            intent.putExtra(BatchDetailFragment.ARG_TASK_DID, TaskDID);
                            context.startActivity(intent);
                        }
                    }
                    else{
                        msgbox("Navigation Validation","Jump Route Prohibited !", v.getContext());
                    }
                }
            });
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public DS_TaskItemDetail  mItem;
            public final TextView mFileID;
            public final View     mView;
            public final TextView mDid;
            public final TextView mSeq;
            public final TextView mRak;
            public final TextView mBin;
            public final TextView mComp;
            public final TextView mFlag;

            // batch list content
            public ViewHolder(View view) {
                super(view);
                mView = view;
                mFileID   = (TextView) view.findViewById(R.id.txt_batch_fileId);
                mDid      = (TextView) view.findViewById(R.id.lvi_did);
                mSeq      = (TextView) view.findViewById(R.id.lvi_seq);
                mRak      = (TextView) view.findViewById(R.id.lvi_rak);
                mBin      = (TextView) view.findViewById(R.id.lvi_bin);
                mComp     = (TextView) view.findViewById(R.id.lvi_comp);
                mFlag     = (TextView) view.findViewById(R.id.lvi_flag);
            }
        } // end of class ViewHolder


    } // end of class SimpleItemRecyclerViewAdapter
}
