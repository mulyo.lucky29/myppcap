package com.gudanggaramtbk.myppcap;

/**
 * Created by LuckyM on 8/10/2018.
 */

public interface SoapSendDataBaseTaskAsyncResponse {
    void PostSentAction(String output);
}
