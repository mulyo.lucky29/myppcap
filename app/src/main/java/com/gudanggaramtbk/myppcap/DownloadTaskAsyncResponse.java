package com.gudanggaramtbk.myppcap;

/**
 * Created by LuckyM on 3/6/2018.
 */

public interface DownloadTaskAsyncResponse {
    void PostDownloadAction(String output);
}
