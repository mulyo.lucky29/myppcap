package com.gudanggaramtbk.myppcap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TaskStackBuilder;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.FileProvider;
import android.util.FloatMath;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import org.w3c.dom.Text;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.R.attr.bitmap;
import static android.R.attr.thumbnail;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * A fragment representing a single Batch detail screen.
 * This fragment is either contained in a {@link BatchListActivity}
 * in two-pane mode (on tablets) or a {@link BatchDetailActivity}
 * on handsets.
 */
public class BatchDetailFragment extends Fragment implements View.OnClickListener {
    private SharedPreferences  config;
    private MySQLiteHelper     dbHelper;
    private ImageHelper        ihx;
    private Context            context;
    private ImageButton        Cmd_Cam1;
    private ImageButton        Cmd_Cam2;
    private ImageButton        Cmd_Cam3;
    private ImageButton        Cmd_Gal1;
    private ImageButton        Cmd_Gal2;
    private ImageButton        Cmd_Gal3;
    private ImageButton        Cmd_Save;
    private ImageButton        Cmd_PrevRoute;
    private ImageButton        Cmd_NextRoute;
    private Button             Cmd_FinishRoute;
    private ImageView          Img_1;
    private ImageView          Img_2;
    private ImageView          Img_3;
    private String             StrSessionNIK;
    private String             StrSessionName;
    private String             xTaskDID;
    private String             xFileID;
    private Integer            CurrRoute;
    private TextView           IsClosed;
    private TextView           RouteSeq;
    private TextView           LockRak;
    private TextView           LockBin;
    private TextView           LocComp;
    private TextView           ItemCode;
    private TextView           ItemDesc;
    private TextView           Txt_blobID1;
    private TextView           Txt_blobID2;
    private TextView           Txt_blobID3;
    private Spinner            CboReason;
    private Bitmap             BlankImage;

    boolean[]                  PHasChanged;

    private DS_TaskItemDetail  dsk;
    List<DS_TaskItemDetail>    mItem;

    Uri photoFileUri = null;
    String storeFilename;

    private static final String TAG = "Touch";


    // The fragment argument representing the item ID that this fragment represents.
    public static final String ARG_TASK_DID = "task_did";

     // Mandatory empty constructor for the fragment manager to instantiate the fragment (e.g. upon screen orientation changes).
    public BatchDetailFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("[GudangGaram]", " BatchDetailFragment :: onCreate");

        if (getArguments().containsKey(ARG_TASK_DID)) {
            StrSessionName = getArguments().getString("PIC_NAME");
            StrSessionNIK  = getArguments().getString("PIC_NIK");
            xFileID        = getArguments().getString("FILE_ID");
            xTaskDID       = getArguments().getString(ARG_TASK_DID);

            Log.d("[GudangGaram]", " BatchDetailFragment Parameters ");
            Log.d("[GudangGaram]", " ===============================");
            Log.d("[GudangGaram]", " PIC NIK  : " + StrSessionNIK);
            Log.d("[GudangGaram]", " PIC NAME : " + StrSessionName);
            Log.d("[GudangGaram]", " FILE ID  : " + xFileID);
            Log.d("[GudangGaram]", " Task DID : " + xTaskDID);
            Log.d("[GudangGaram]", " ===============================");

            // Load the dummy content specified by the fragment arguments. In a real-world scenario, use a Loader to load content from a content provider.
            //mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_TASK_DID));
            //mItem = DS_TaskItemDetail.

            Activity activity = this.getActivity();
            dsk      = new DS_TaskItemDetail();

            // declare helper
            dbHelper = new MySQLiteHelper(activity);
            dbHelper.getWritableDatabase();
            ihx      = new ImageHelper();

            mItem = dbHelper.get_RouteInfoBy("", "TaskDID",xTaskDID,"");

            // init for blank image
            BlankImage = BitmapFactory.decodeResource(getResources(), R.drawable.noimage);

            // initialized Status PHasChanged
            PHasChanged = new boolean[4];
            // set all false as init
            Arrays.fill(PHasChanged, false);

            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle("Route ("  + xFileID + ")");

            }
        }
    }


    private List<String> InitListCBReason(){
        List<String> olist = new ArrayList<String>();
        olist = dbHelper.getLOV("GGGG_INV_REASON_UPLOAD_GAMBAR");
        return olist;
    }

    private void initComboBox(Spinner spo, List<String> lvi)
    {
        Spinner spinner = spo;
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                PHasChanged[0] = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                PHasChanged[0] = false;
            }

        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("[GudangGaram]", " BatchDetailFragment :: onCreateView");

        View rootView = inflater.inflate(R.layout.batch_detail, container, false);
        IsClosed      = (TextView)    rootView.findViewById(R.id.txt_IsClosed);
        RouteSeq      = (TextView)    rootView.findViewById(R.id.txt_task_route_seq);
        LockRak       = (TextView)    rootView.findViewById(R.id.txt_LocRak);
        LockBin       = (TextView)    rootView.findViewById(R.id.txt_LocBin);
        LocComp       = (TextView)    rootView.findViewById(R.id.txt_LocComp);
        ItemCode      = (TextView)    rootView.findViewById(R.id.txt_itemCode);
        ItemDesc      = (TextView)    rootView.findViewById(R.id.txt_itemDesc);
        Txt_blobID1   = (TextView)    rootView.findViewById(R.id.txt_ps_blobID1);
        Txt_blobID2   = (TextView)    rootView.findViewById(R.id.txt_ps_blobID2);
        Txt_blobID3   = (TextView)    rootView.findViewById(R.id.txt_ps_blobID3);
        CboReason     = (Spinner)     rootView.findViewById(R.id.cbo_RouteReason);
        Img_1         = (ImageView)   rootView.findViewById(R.id.img_1);
        Img_2         = (ImageView)   rootView.findViewById(R.id.img_2);
        Img_3         = (ImageView)   rootView.findViewById(R.id.img_3);
        Cmd_Cam1      = (ImageButton) rootView.findViewById(R.id.cmd_cam1);
        Cmd_Cam2      = (ImageButton) rootView.findViewById(R.id.cmd_cam2);
        Cmd_Cam3      = (ImageButton) rootView.findViewById(R.id.cmd_cam3);
        Cmd_Gal1      = (ImageButton) rootView.findViewById(R.id.cmd_gal1);
        Cmd_Gal2      = (ImageButton) rootView.findViewById(R.id.cmd_gal2);
        Cmd_Gal3      = (ImageButton) rootView.findViewById(R.id.cmd_gal3);
        Cmd_PrevRoute = (ImageButton) rootView.findViewById(R.id.cmd_prev_route);
        Cmd_NextRoute = (ImageButton) rootView.findViewById(R.id.cmd_next_route);
        Cmd_Save      = (ImageButton) rootView.findViewById(R.id.cmd_save);

        Cmd_FinishRoute = (Button) rootView.findViewById(R.id.cmd_finish_route);

        // ------------------ set click listener --------------------
        Cmd_Cam1.setOnClickListener(this);
        Cmd_Cam2.setOnClickListener(this);
        Cmd_Cam3.setOnClickListener(this);
        Cmd_Gal1.setOnClickListener(this);
        Cmd_Gal2.setOnClickListener(this);
        Cmd_Gal3.setOnClickListener(this);
        // ------------------ navigation button ----------------------
        Cmd_NextRoute.setOnClickListener(this);
        Cmd_PrevRoute.setOnClickListener(this);
        Cmd_Save.setOnClickListener(this);
        Cmd_FinishRoute.setOnClickListener(this);

        Img_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_1 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_1.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, getContext());
            }
        });
        Img_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_2 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_2.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, getContext());
            }
        });
        Img_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_3 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_3.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, getContext());
            }
        });

        // load combobox reason
        initComboBox(CboReason,InitListCBReason());
        Load_Info_Route();

        return rootView;
    }

    private Bitmap loadBitmapDB(String pTaskDID, Integer pSeq){
        Integer fs;
        Bitmap Bitmap;
        byte[] Image;

        fs = dbHelper.countItemPicture(pTaskDID, pSeq);
        try{
            if(fs > 0){
                Log.d("[GudangGaram]", "Image Size " + pSeq + " : " + fs);
                Image  = dbHelper.loadItemPicture(pTaskDID,pSeq);
                Bitmap = BitmapFactory.decodeByteArray(Image, 0, Image.length);
            }
            else{
                Bitmap = BlankImage;
            }
        }
        catch(Exception e){
            Bitmap = BlankImage;
            Log.d("[GudangGaram]", "Exception Image " + pSeq + " : " + e.getMessage());
        }
        return  Bitmap;
    }

    private void Load_Info_Route() {
        Integer fs1,fs2, fs3;
        Bitmap Bitmap1, Bitmap2, Bitmap3;
        byte[] Image1;
        byte[] Image2;
        byte[] Image3;
        Integer maxRoute,currRoute;

        Log.d("[GudangGaram]", "BatchDetailFragment :: Load_Info_Route");

        if(mItem.size() > 0){
            // set all false as init
            Arrays.fill(PHasChanged, false);
            // iteration
            for (int i=0; i<mItem.size(); i++) {
                IsClosed.setText(mItem.get(i).getIsClosed().toString());
                RouteSeq.setText(mItem.get(i).getSeq().toString());
                LockRak.setText(mItem.get(i).getLocRak().toString());
                LockBin.setText(mItem.get(i).getLocBin().toString());
                LocComp.setText(mItem.get(i).getLocCom().toString());
                ItemCode.setText(mItem.get(i).getItemCode().toString());
                ItemDesc.setText(mItem.get(i).getItemDesc().toString());
                CboReason.setSelection(((ArrayAdapter)CboReason.getAdapter()).getPosition(mItem.get(i).getReason().toString()));

                Log.d("[GudangGaram]", "Route Seq " + RouteSeq.getText());

                Img_1.setImageBitmap(loadBitmapDB(mItem.get(i).getTaskDID().toString(), 1));
                Img_2.setImageBitmap(loadBitmapDB(mItem.get(i).getTaskDID().toString(), 2));
                Img_3.setImageBitmap(loadBitmapDB(mItem.get(i).getTaskDID().toString(), 3));

                // current route
                CurrRoute = mItem.get(i).getSeq();
                xTaskDID  = mItem.get(i).getTaskDID().toString();
            }

            maxRoute = dbHelper.get_RouteCount(xTaskDID);
            currRoute = Integer.parseInt(RouteSeq.getText().toString());

            if(countBlankImage() == 0){
                CboReason.setEnabled(false);
            }
            else{
                // if all image are blank or not fully taken then need to write reason then enabled Combo Reason
                CboReason.setEnabled(true);
            }

            if(dbHelper.FinishRouteStatus(xTaskDID) > 0){
                Cmd_FinishRoute.setVisibility(View.GONE);
            }
            else{
                if(currRoute == maxRoute){
                    Cmd_FinishRoute.setEnabled(true);
                    Cmd_FinishRoute.setVisibility(View.VISIBLE);
                }
                else{
                    Cmd_FinishRoute.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.cmd_cam1){
            Log.d("[GudangGaram]", "OnClick Cmd Cam1");
            EH_CMD_TAKE_PICTURE(1);
        }
        else if(id == R.id.cmd_cam2){
            Log.d("[GudangGaram]", "OnClick Cmd Cam2");
            EH_CMD_TAKE_PICTURE(2);
        }
        else if(id == R.id.cmd_cam3){
            Log.d("[GudangGaram]", "OnClick Cmd Cam3");
            EH_CMD_TAKE_PICTURE(3);
        }
        else if(id == R.id.cmd_gal1){
            Log.d("[GudangGaram]", "OnClick Cmd Gal1");
            EH_CMD_TAKE_GALLERY(1);
        }
        else if(id == R.id.cmd_gal2){
            Log.d("[GudangGaram]", "OnClick Cmd Gal2");
            EH_CMD_TAKE_GALLERY(2);
        }
        else if(id == R.id.cmd_gal3){
            Log.d("[GudangGaram]", "OnClick Cmd Gal3");
            EH_CMD_TAKE_GALLERY(3);
        }
        else if(id == R.id.cmd_prev_route){
            Log.d("[GudangGaram]", "OnClick Cmd Prev");
            EH_CMD_PREV();
        }
        else if(id == R.id.cmd_next_route){
            Log.d("[GudangGaram]", "OnClick Cmd Next");
            EH_CMD_NEXT();
        }
        else if(id == R.id.cmd_finish_route){
            Log.d("[GudangGaram]", "OnClick Cmd Finish Route");
            EH_CMD_FINISH_ROUTE();
        }
        else if(id == R.id.cmd_save){
            Log.d("[GudangGaram]", "OnClick Cmd Save All");
            EH_CMD_SAVE_ALL();
        }
    }

    public void EH_CMD_BACK_HOME(){
        Intent I_Main = new Intent(getContext(), MainActivity.class);
        I_Main.putExtra("PIC_NIK",  StrSessionNIK);
        I_Main.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_Main);
    }

    public void EH_CMD_FINISH_ROUTE(){
        Log.d("[GudangGaram]", "OnClick Cmd finish route");
        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder
                    .setTitle("Finish Route Confirm ")
                    .setMessage("Confirm Finish Route ?")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    if(isInputValid()){
                                        Log.d("[GudangGaram]", "EH_CMD_FINISH_ROUTE");
                                        EH_CMD_ACTION_CLOSE_ALL();
                                        Toast.makeText(getContext(),"Route Finish", Toast.LENGTH_LONG).show();
                                        EH_CMD_BACK_HOME();
                                    }
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_SAVE Exception " + e.getMessage().toString());
        }
    }

    public void msgbox(String pTitle, String pMessage){
        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder
                    .setTitle(pTitle)
                    .setMessage(pMessage)
                    .setCancelable(false)
                    .setNegativeButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_SAVE Exception " + e.getMessage().toString());
        }
    }

    public Integer countBlankImage(){
        byte[] bimg0, bimg1,bimg2, bimg3;
        Integer result = 0;
        //bimg0   = ihx.getByteFromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.noimage));

        bimg0   = ihx.getByteFromBitmap(BlankImage);
        bimg1   = ihx.getByteImageView(Img_1);
        bimg2   = ihx.getByteImageView(Img_2);
        bimg3   = ihx.getByteImageView(Img_3);
        // check if image1 blank
        if(Arrays.equals(bimg0, bimg1)){
            result = result + 1;
        }
        // check if image2 blank
        if(Arrays.equals(bimg0, bimg2)){
            result = result + 1;
        }
        // check if image3 blank
        if(Arrays.equals(bimg0, bimg3)){
            result = result + 1;
        }
        return result;
    }

    public Boolean isInputValid(){
        Boolean result = false;
        Integer blank_image = 0;
        String xReason;

        // if route not closed then check input validation otherwise no need to check lah
        if(IsClosed.getText().equals("N")){
            xReason = CboReason.getSelectedItem().toString();
            if(countBlankImage() == 3){
                if(xReason.equals(" ")){
                    msgbox("Input Validation","Picture Has Not Taken Yet, Please Mention Reason To Continue");
                    result = false;
                }
                else{
                    // if reason already mentioned for none of taken picture
                    result = true;
                }
            }
            else{
                result = true;
            }
        }
        else{
            // no need to check lah
            result = true;
        }

        return result;
    }

    public void EH_CMD_SAVE_ALL_ACTION(){
        Integer i;
        String xReason;
        Log.d("[GudangGaram]", "BatchDetailFragment :: EH_CMD_AUTOSAVE");

        xReason = CboReason.getSelectedItem().toString();
        try{
            // update reason
            dbHelper.updateReason(xTaskDID,xReason);
            PHasChanged[0] = false;
            // save picture
            for(i=1; i<=3; i++){
                if(PHasChanged[i]){
                     EH_CMD_SAVE_ACTION(xTaskDID,i);
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_AUTOSAVE " + e.getMessage().toString());
        }
    }

    public void EH_CMD_SAVE_ACTION(final String pTaskDID, final Integer pSeq){
        Integer pLobID = 0;
        byte[] pFileData  = {0};
        byte[] pBlankData, dbImageData;
        String pFileID, pItemID;
        Bitmap bitmap1,bitmap2, bitmap3;
        ByteArrayOutputStream baos1,baos2,baos3;

        Log.d("[GudangGaram]", "EH_CMD_SAVE_ACTION " + pSeq);

        if(pSeq == 1){
            pFileData = ihx.getByteImageView(Img_1);
        }
        else if(pSeq == 2){
            pFileData = ihx.getByteImageView(Img_2);
        }
        else if(pSeq == 3){
            pFileData = ihx.getByteImageView(Img_3);
        }
        pBlankData = ihx.getByteFromBitmap(BlankImage);

        if(pFileData.length > 0){
            Log.d("[GudangGaram]", "pFileData.length > 0");
            Log.d("[GudangGaram]", "pHasChanged [" + pSeq + "] Status Before Save " + PHasChanged[pSeq]);

            if(!Arrays.equals(pFileData,pBlankData)){
                //if(PHasChanged[pSeq] == true) {
                Log.d("[GudangGaram]", "pHasChanged [" + pSeq + "] Save InProgress");
                Integer imgcount =  dbHelper.countItemPicture(pTaskDID,pSeq);
                Log.d("[GudangGaram]", "imgcount : " + imgcount);

                // check in database if image already exist then just update
                if(imgcount > 0){
                    // check if image is different then do update else if still the same then do not
                    dbImageData = dbHelper.getItemPicture(pTaskDID,pSeq);
                    if(!Arrays.equals(pFileData,dbImageData)){
                        Log.d("[GudangGaram]", "do update image");
                        dbHelper.updateItemPicture(pTaskDID,pSeq,pFileData,-1);
                    }
                }
                else{
                    Log.d("[GudangGaram]", "do insert image");
                    pItemID = dbHelper.getItemID(pTaskDID).toString();
                    pFileID = dbHelper.getFileID(pTaskDID).toString();
                    dbHelper.addItemPicture(pTaskDID,pSeq,pFileData,pFileID, pItemID, "-1", "-1");
                }
                // change staus
                PHasChanged[pSeq] = false;
            }
            Log.d("[GudangGaram]", "pHasChanged [" + pSeq + "] Status After Save " + PHasChanged[pSeq]);
        }
    }

    public void EH_CMD_SAVE_ALL(){
        Log.d("[GudangGaram]", "OnClick Cmd Save pTaskDID = " + xTaskDID);
        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder
                    .setTitle("Save Page Confirm ")
                    .setMessage("Confirm Save Current Page ?")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    if(isInputValid()){
                                        Log.d("[GudangGaram]", "EH_CMD_SAVE_ALL Save Changes");
                                        EH_CMD_SAVE_ALL_ACTION();
                                        Toast.makeText(getContext(),"Save Current Page Done", Toast.LENGTH_LONG).show();
                                    }
                                    /*
                                    if((PHasChanged[0] == true) ||
                                            (PHasChanged[1] == true) ||
                                            (PHasChanged[2] == true) ||
                                            (PHasChanged[3] == true)){
                                            if(isInputValid()){
                                                Log.d("[GudangGaram]", "EH_CMD_SAVE_ALL Save Changes");
                                                EH_CMD_SAVE_ALL_ACTION();
                                                Toast.makeText(getContext(),"Save Current Page Done", Toast.LENGTH_LONG).show();
                                            }
                                    }
                                    else{
                                        Log.d("[GudangGaram]", "EH_CMD_SAVE_ALL Save Nothing");
                                        Toast.makeText(getContext(),"Save Nothing", Toast.LENGTH_LONG).show();
                                    }
                                    */
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_SAVE Exception " + e.getMessage().toString());
        }
    }

    public void EH_CMD_PREV(){
        Integer maxcount = 0;
        maxcount = dbHelper.get_RouteCount(xTaskDID);
        EH_CMD_SAVE_ALL_ACTION();
        if(CurrRoute > 1){
            CurrRoute = CurrRoute - 1;
        }
        else{
            CurrRoute = 1;
        }
        mItem = dbHelper.get_RouteInfoBy("","CursorFileID",xTaskDID,Integer.toString(CurrRoute));
        Load_Info_Route();
    }

    public void EH_CMD_NEXT(){
        Integer maxcount = 0;
        maxcount = dbHelper.get_RouteCount(xTaskDID);

        if(isInputValid()){
            // save it all first
            EH_CMD_SAVE_ALL_ACTION();
            // move next route cursor
            if(CurrRoute < maxcount){
                // close current route
                EH_CMD_ACTION_CLOSE();
                CurrRoute = CurrRoute + 1;
            }
            else{
                CurrRoute = maxcount;
            }
            mItem = dbHelper.get_RouteInfoBy("","CursorFileID",xTaskDID,Integer.toString(CurrRoute));
            // reload next route data
            Load_Info_Route();
        }
    }

    public void EH_CMD_ACTION_CLOSE(){
           dbHelper.updateClosedFlag(xTaskDID);
    }
    public void EH_CMD_ACTION_CLOSE_ALL(){
        // save it first
        EH_CMD_SAVE_ALL_ACTION();
        if(dbHelper.FinishRouteStatus(xTaskDID) > 0) {
            msgbox("Finish Route","This Route Already Finish !");
        }
        else{
            dbHelper.updateAllClosedFlag(xTaskDID);
        }
    }
    public void EH_CMD_POPUP_DIALOG(Bitmap oBitmap, Context ctx){
        Log.d("[GudangGaram]", "EH_CMD_POPUP_DIALOG");
        ihx.show_image_popup(oBitmap,ctx);
    }

    public void EH_CMD_TAKE_PICTURE(Integer index){
        Uri     photoURI;
        String  image_path;
        File    img;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        String sysdate              = dateFormat.format(new Date());
        storeFilename               = "PPCAP_" + sysdate + ".jpg";

        image_path = Environment.getExternalStorageDirectory() + "/" + storeFilename;
        img = new File (image_path);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            photoURI = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", img);
        }
        else {
            photoURI = Uri.fromFile(img);
        }

        try{
            Log.d("[GudangGaram]", "EH_CMD_TAKE_PICTURE " + index.toString() + " : " + storeFilename);
            Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            photoCaptureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            photoCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(photoCaptureIntent, index);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_TAKE_PICTURE Exception " + e.getMessage().toString());
        }
    }

    public void EH_CMD_TAKE_GALLERY(Integer index){
        Log.d("[GudangGaram]", "EH_CMD_TAKE_GALLERY " + index.toString());
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*.jpg");
        startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), index + 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode < 100){
            // intent from take picture request
            if(resultCode == Activity.RESULT_OK){
                // ====================================================
                // this only get low res thumbnail
                //Bundle extras = data.getExtras();
                //Bitmap bitmap = (Bitmap) extras.get("data");
                // ====================================================

                // load as bitmap from sdcard
                Bitmap mbitmap = ihx.getImageFileFromSDCard(storeFilename,true,"BC");
                if (requestCode == 1) {
                    Img_1.setImageBitmap(mbitmap);
                }
                else if(requestCode == 2) {
                    Img_2.setImageBitmap(mbitmap);
                }
                else if(requestCode == 3) {
                    Img_3.setImageBitmap(mbitmap);
                }

                try {
                    PHasChanged[requestCode] = true;
                }
                catch(Exception e){
                    Log.d("[GudangGaram]", "Exception " + e.getMessage());
                }
                finally {
                    storeFilename = "";
                }
            }

            if(countBlankImage() == 3){
                CboReason.setEnabled(true);
            }
            else{
                CboReason.setSelection(0);
                CboReason.setEnabled(false);
            }
        }
        else{
            if(resultCode == Activity.RESULT_OK){
                Uri selectedImageUri = data.getData();
                String selectedImagePath = selectedImageUri.getPath();

                if(requestCode == 101){
                    Img_1.setImageBitmap(ihx.getImageFromUri(Img_1,selectedImageUri,true,"BG"));
                }
                else if(requestCode == 102){
                    Img_2.setImageBitmap(ihx.getImageFromUri(Img_2,selectedImageUri,true,"BG"));
                }
                else if(requestCode == 103){
                    Img_3.setImageBitmap(ihx.getImageFromUri(Img_3,selectedImageUri,true,"BG"));
                }

                try{
                    PHasChanged[requestCode-100] = true;
                }
                catch(Exception e){
                    Log.d("[GudangGaram]", "Exception " + e.getMessage());
                }
            }
            if(countBlankImage() == 3){
                CboReason.setEnabled(true);
            }
            else{
                CboReason.setSelection(0);
                CboReason.setEnabled(false);
            }
        }
    }

}
