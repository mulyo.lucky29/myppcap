package com.gudanggaramtbk.myppcap;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 6/13/2018.
 */

public class CustomListAdapterTaskMain extends ArrayAdapter<DS_TaskItem> {
    Context                mcontext;
    List<DS_TaskItem>      ListRoute;
    private LayoutInflater mInflater;
    ListView               olisttask;
    ArrayList<Boolean>     positionArray;
    public boolean         checkBoxState[];
    String                 pNIK;
    String                 pName;


    public CustomListAdapterTaskMain(Context context, List<DS_TaskItem> list, String p_NIK, String p_Name)
    {
        super(context,0,list);
        mcontext  = context;
        ListRoute = list;
        pNIK      = p_NIK;
        pName     = p_Name;

        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Log.d("[GudangGaram]", "CustomListAdapterTaskMain Parameters :");
        Log.d("[GudangGaram]", "==================================== ");
        Log.d("[GudangGaram]", "pNik  : " + p_NIK);
        Log.d("[GudangGaram]", "pName : " + p_Name);
        Log.d("[GudangGaram]", "==================================== ");
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderMain holder;
        Log.d("[GudangGaram]", "CustomListAdapterTaskMain :: getView Begin");

        try {
            if(convertView == null) {
                convertView = mInflater.inflate(R.layout.rowlisttaskmain, parent, false);
                holder = new ViewHolderMain();

                holder.Mchk_M_select     = (CheckBox) convertView.findViewById(R.id.chk_M_select);
                holder.Mtxt_M_task_id    = (TextView) convertView.findViewById(R.id.txt_M_task_id);
                holder.Mtxt_M_file_id    = (TextView) convertView.findViewById(R.id.txt_M_file_id);
                holder.Mtxt_M_Status     = (TextView) convertView.findViewById(R.id.txt_M_Status);
                holder.Mcmd_M_go         = (Button) convertView.findViewById(R.id.cmd_M_go);

                convertView.setTag(holder);
            }
            else{
                holder = (ViewHolderMain) convertView.getTag();
                holder.Mchk_M_select.setOnCheckedChangeListener(null);
            }

            /*
            holder.Mchk_M_select.setFocusable(false);
            holder.Mchk_M_select.setChecked(positionArray.get(position));
            holder.Mchk_M_select.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(((CheckBox)v).isChecked()) {
                        olisttask.setItemChecked(position,true);
                        v.setSelected(true);
                        positionArray.set(position, true);
                        Log.d("[GudangGaram]", "check (" + position + ") true");
                    }
                    else{
                        olisttask.setItemChecked(position,false);
                        v.setSelected(false);
                        positionArray.set(position, false);
                        Log.d("[GudangGaram]", "check (" + position + ") false");
                    }
                }
            });
            */


        holder.Mcmd_M_go.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String xFileID;

                xFileID  = Integer.toString(((Button)v).getId());
                EH_CMD_CALL_Route_List(xFileID);
                Log.d("[GudangGaram]", "Mcmd Go :: getView : " + xFileID);
            }
        });


            CheckBox MxMchk_M_select         =  holder.Mchk_M_select;
            TextView MxMtxt_M_task_id        =  holder.Mtxt_M_task_id;
            TextView MxMtxt_M_file_id        =  holder.Mtxt_M_file_id;
            TextView MxMtxt_M_status         =  holder.Mtxt_M_Status;
            Button   MxMcmd_M_go             =  holder.Mcmd_M_go;

            DS_TaskItem o = getItem(position);
            MxMchk_M_select.setChecked(false);
            MxMtxt_M_task_id.setText(o.getTaskID().toString());
            MxMtxt_M_file_id.setText(o.getFileID().toString());
            MxMtxt_M_status.setText(o.getStatus().toString());
            MxMcmd_M_go.setId((int)o.getFileID());


        }
        catch(Exception e){
            Log.d("[GudangGaram]", "CustomListAdapterTaskMain :: getView Exception " + e.getMessage().toString());
        }


        return convertView;
    }

    private void EH_CMD_CALL_Route_List(String pFileID){
        Intent I_Batch;
        I_Batch = new Intent(getContext(),BatchListActivity.class);
        I_Batch.putExtra("PIC_NIK" ,  pNIK);
        I_Batch.putExtra("PIC_NAME",  pName);
        I_Batch.putExtra("FILE_ID" ,  pFileID);

        Log.d("[GudangGaram]", "EH_CMD_CALL_Route_List Called File ID: " + pFileID);

        mcontext.startActivity(I_Batch);
    }

}
