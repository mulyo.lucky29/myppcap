package com.gudanggaramtbk.myppcap;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Login extends AppCompatActivity {
    private static Spinner           Cbo_pic_nik;
    private static TextView          Txt_pic_name;
    private static Button            Cmd_Login;
    private static Button            Cmd_Exit;
    private MySQLiteHelper           dbHelper;
    private SharedPreferences        config;
    boolean doubleBackToExitPressedOnce         = false;
    private static final int REQUEST_PERMISSION = 1;

    private static String[] PERMISSIONS_APPS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

        /*
        PackageManager packageManager = context.getPackageManager();
        if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT) == false){
            Toast.makeText(getActivity(), "This device does not have a camera.", Toast.LENGTH_SHORT).show();
        }
        else{
        }
        */

    void check_apps_permission() {
        // popup permission granted
        int permission = ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        // if dont have permit before then popup
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    Login.this,
                    PERMISSIONS_APPS,
                    REQUEST_PERMISSION
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        config = getSharedPreferences("MyPPCAPSetting", MODE_PRIVATE);

        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        Cbo_pic_nik  = (Spinner)findViewById(R.id.cbo_nik_pic);
        Txt_pic_name = (TextView)findViewById(R.id.txt_nama_pic);
        Cmd_Login    = (Button)findViewById(R.id.cmd_login);
        Cmd_Exit     = (Button)findViewById(R.id.cmd_exit);

        Cmd_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // redirect to main page
                EH_cmd_login();
            }
        });

        Cmd_Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Build.VERSION.SDK_INT>=16 && Build.VERSION.SDK_INT<21){
                    finishAffinity();
                } else if(Build.VERSION.SDK_INT>=21){
                    finishAndRemoveTask();
                }
                /*
                int pid;
                finish();
                pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
                */
            }
        });

        // init combo nik pic
        initKVPComboBox(R.id.cbo_nik_pic);
        // check application permission
        check_apps_permission();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public void EH_cmd_login(){
        String v_txt_pic_nik;
        String v_txt_pic_name;

        v_txt_pic_nik   = Cbo_pic_nik.getSelectedItem().toString();
        v_txt_pic_name  = Txt_pic_name.getText().toString();

        Intent Main_intent = new Intent(this.getBaseContext(), MainActivity.class);
        Main_intent.putExtra("PIC_NIK",  v_txt_pic_nik);
        Main_intent.putExtra("PIC_NAME", v_txt_pic_name);
        startActivity(Main_intent);
    }

    private void initKVPComboBox(final int p_layout){
        Spinner spinner = (Spinner) findViewById(p_layout);
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();
        lvi = dbHelper.getEmployeeList();

        // Creating adapter for spinner
        ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag swt = (StringWithTag) parent.getItemAtPosition(position);
                String        key = (String) swt.tag;

                Log.d("[GudangGaram]", " KVP Value Selected >> " + swt);
                Log.d("[GudangGaram]", " KVP Key   Selected >> " + key.toString());

                Txt_pic_name.setText(key.toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

}
