package com.gudanggaramtbk.myppcap;

import java.util.List;

/**
 * Created by LuckyM on 6/11/2018.
 */

public interface SoapRetrieveListTaskAsyncResponse {
    void PostSentAction(String output);
}
