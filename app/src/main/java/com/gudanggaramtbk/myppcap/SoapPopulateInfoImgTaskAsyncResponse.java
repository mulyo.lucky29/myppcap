package com.gudanggaramtbk.myppcap;

import java.util.List;

/**
 * Created by luckym on 8/29/2018.
 */

public interface SoapPopulateInfoImgTaskAsyncResponse {
    void PostSentAction(List<DS_ItemImg> output);
}
