package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 8/28/2018.
 */

public class SoapPopulateInfoTaskAsync extends AsyncTask<String, String, String>  {
    private SharedPreferences         config;
    private Context                   context;
    private ProgressDialog            pd;
    private MySQLiteHelper            dbHelper;
    private String                    SentResponse;
    private String                    pType;
    private String                    pItemCode;
    private List<DS_ItemInfo>         Result;

    public SoapPopulateInfoTaskAsync(){ }
    public interface SoapPopulateInfoTaskAsyncResponse { void PostSentAction(List<DS_ItemInfo> output); }
    public SoapPopulateInfoTaskAsync.SoapPopulateInfoTaskAsyncResponse delegate = null;
    public SoapPopulateInfoTaskAsync(SoapPopulateInfoTaskAsync.SoapPopulateInfoTaskAsyncResponse delegate){ this.delegate = delegate; }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String pType,
                             String pItemCode){
        Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: setAttribute");
        this.context          = context;
        this.dbHelper         = dbHelper;
        this.config           = config;
        this.pType            = pType;
        this.pItemCode        = pItemCode;
        this.pd = new ProgressDialog(this.context);
        Result = new ArrayList<DS_ItemInfo>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: onPreExecute");
        pd.setMessage("Retrieve Info Item From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String file_url) {
        super.onPostExecute(file_url);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: onPostExecute");

        try {
            if (Result.size() == 0) {
                Toast.makeText(context, "No Data Result", Toast.LENGTH_LONG).show();
            }
            delegate.PostSentAction(Result);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: onPostExecute Exception " + e.getMessage());
        }
        Toast.makeText(context,"Retrieve Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        Integer v_item_id;
        String  v_item_code;
        String  v_item_desc;
        String  v_item_brand;
        String  v_item_spec;
        Integer v_fs_img1;
        Integer v_fs_img2;
        Integer v_fs_img3;
        Integer v_faid_img1;
        Integer v_faid_img2;
        Integer v_faid_img3;
        Integer v_ffid_img1;
        Integer v_ffid_img2;
        Integer v_ffid_img3;

        Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: doInBackground begin");
        //Web method call
        try {
            String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
            String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
            String OPERATION_NAME = "Get_Item_Info";
            String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);


            SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

            // =============== p_type ===================
            PropertyInfo prop_p_type = new PropertyInfo();
            prop_p_type.setName("p_type");
            prop_p_type.setValue(pType);
            prop_p_type.setType(String.class);
            request.addProperty(prop_p_type);

            // =============== p_item_code ===================
            PropertyInfo prop_p_itemCode = new PropertyInfo();
            prop_p_itemCode.setName("p_item_code");
            prop_p_itemCode.setValue(pItemCode);
            prop_p_itemCode.setType(String.class);
            request.addProperty(prop_p_itemCode);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj  = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));
                Result.clear();

                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);


                        DS_ItemInfo dii = new DS_ItemInfo();
                        v_item_id        = Integer.parseInt(obj3.getProperty(0).toString());
                        v_item_code      = obj3.getProperty(1).toString();
                        v_item_desc      = obj3.getProperty(2).toString();
                        v_item_brand     = obj3.getProperty(3).toString();
                        v_item_spec      = obj3.getProperty(4).toString();
                        v_fs_img1        = Integer.parseInt(obj3.getProperty(5).toString());
                        v_fs_img2        = Integer.parseInt(obj3.getProperty(6).toString());
                        v_fs_img3        = Integer.parseInt(obj3.getProperty(7).toString());
                        v_faid_img1      = Integer.parseInt(obj3.getProperty(8).toString());
                        v_faid_img2      = Integer.parseInt(obj3.getProperty(9).toString());
                        v_faid_img3      = Integer.parseInt(obj3.getProperty(10).toString());
                        v_ffid_img1      = Integer.parseInt(obj3.getProperty(11).toString());
                        v_ffid_img2      = Integer.parseInt(obj3.getProperty(12).toString());
                        v_ffid_img3      = Integer.parseInt(obj3.getProperty(13).toString());

                        dii.setItemID(v_item_id);
                        dii.setItemCode(v_item_code);
                        dii.setItemDesc(v_item_desc);
                        dii.setItemBrand(v_item_brand);
                        dii.setItemSpec(v_item_spec);
                        dii.setFS_Img1(v_fs_img1);
                        dii.setFS_Img2(v_fs_img2);
                        dii.setFS_Img3(v_fs_img3);
                        dii.setFA_Img1(v_faid_img1);
                        dii.setFA_Img2(v_faid_img2);
                        dii.setFA_Img3(v_faid_img3);
                        dii.setFF_Img1(v_ffid_img1);
                        dii.setFF_Img2(v_ffid_img2);
                        dii.setFF_Img3(v_ffid_img3);

                        Log.d("[GudangGaram]", "Item Id      : " + v_item_id);
                        Log.d("[GudangGaram]", "Item Name    : " + v_item_code);
                        Log.d("[GudangGaram]", "Item Desc    : " + v_item_desc);
                        Log.d("[GudangGaram]", "Item Brand   : " + v_item_brand);
                        Log.d("[GudangGaram]", "Item Spec    : " + v_item_spec);
                        Log.d("[GudangGaram]", "Item Spec    : " + v_item_spec);
                        Log.d("[GudangGaram]", "fs_img1      : " + v_item_spec);
                        Log.d("[GudangGaram]", "fs_img2      : " + v_item_spec);
                        Log.d("[GudangGaram]", "fs_img3      : " + v_item_spec);
                        Log.d("[GudangGaram]", "faid_img1    : " + v_item_spec);
                        Log.d("[GudangGaram]", "faid_img2    : " + v_item_spec);
                        Log.d("[GudangGaram]", "faid_img3    : " + v_item_spec);

                        Result.add(dii);
                    }
                }
                else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: doInBackground Exception :: Catch Http Transport : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: doInBackground end");
        }


        return "";
    }


}
