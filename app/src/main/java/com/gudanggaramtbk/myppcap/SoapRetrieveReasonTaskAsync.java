package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 8/8/2018.
 */

public class SoapRetrieveReasonTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                    ResultResponse;
    private MySQLiteHelper            dbHelper;
    private String                    SentResponse;
    private String                    TaskID;
    private String                    picNIK;

    public SoapRetrieveReasonTaskAsync(){}

    public interface SoapRetrieveReasonTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveReasonTaskAsync.SoapRetrieveReasonTaskAsyncResponse delegate = null;
    public SoapRetrieveReasonTaskAsync(SoapRetrieveReasonTaskAsync.SoapRetrieveReasonTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper    dbHelper,
                             SharedPreferences config){

        Log.d("[GudangGaram]", "SoapRetrieveReasonTaskAsync :: setAttribute");
        this.context          = context;
        this.dbHelper         = dbHelper;
        this.config           = config;
        this.pd = new ProgressDialog(this.context);
        //Result = new ArrayList<DS_TaskItemDetail>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", " SoapRetrieveReasonTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve List Reason From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }


    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveReason :: onPostExecute >> " + ResultResponse);
        try{
            delegate.PostSentAction(ResultResponse);
        }
        catch(Exception e){
        }
        finally {
            if(output.equals("-1")){
                Toast.makeText(context,"Retrieve Reason Done", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(context,"Retrieve Reason" + output + " Done", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Long     Result;
        String   v_context;
        String   v_value;
        String   v_meaning;
        Integer  ncount;
        Integer  nerr;

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Get_List_Reason";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        // flush master first

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);
            SoapObject obj, obj1, obj2, obj3;
            obj     = (SoapObject) envelope.getResponse();
            obj1    = (SoapObject) obj.getProperty("diffgram");
            obj2    = (SoapObject) obj1.getProperty("NewDataSet");

            ncount = obj2.getPropertyCount();
            nerr   = 0;
            Log.d("[GudangGaram]", ">>> count  >>> " + ncount.toString());
            for (int i = 0; i < ncount ;i++){
                obj3 = (SoapObject) obj2.getProperty(i);

                Log.d("[GudangGaram]", "----------------------------------------------------------");
                Log.d("[GudangGaram]", ">>> Context         >>> " + obj3.getProperty(0).toString());
                Log.d("[GudangGaram]", ">>> Value           >>> " + obj3.getProperty(1).toString());
                Log.d("[GudangGaram]", ">>> Meaning         >>> " + obj3.getProperty(2).toString());

                DS_TaskItemDetail vd = new DS_TaskItemDetail();
                v_context      = obj3.getProperty(0).toString();
                v_value        = obj3.getProperty(1).toString();
                v_meaning      = obj3.getProperty(2).toString();

                Result =  dbHelper.addLookup(v_context, v_value, v_meaning);

                if(Result > 0){
                }
                else{
                    nerr +=1;
                }
            }
            if(nerr > 0) {
                ResultResponse = "*";
            }
            else{
                ResultResponse = TaskID;
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrievePICTaskAsync  :: end doInBackground");
        }

        return "";
    }
}
