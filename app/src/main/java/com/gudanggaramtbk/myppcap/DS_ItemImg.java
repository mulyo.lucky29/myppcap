package com.gudanggaramtbk.myppcap;

import android.graphics.Bitmap;
import android.media.Image;

/**
 * Created by luckym on 8/29/2018.
 */

public class DS_ItemImg {

    // data structure for sending img
    private String  ItemCode;
    private String  AttachmentID;
    private String  LobID;
    private Integer AttachmentNo;
    private byte[]  FileData;

    private String  ImgID;
    private String  FileID;
    private String  ItemID;
    private Integer AttachmentSize;
    private Bitmap  FileDataBmp;
    private boolean Changed;
    private String  PicNik;

    // =============== getter =====================
    public String getImgID() { return ImgID; }
    public String getItemCode() { return ItemCode; }
    public String getFileID() { return FileID; }
    public String getItemID() {
        return ItemID;
    }
    public String getAttachmentID() {
        return AttachmentID;
    }
    public Integer getAttachmentNo() {
        return AttachmentNo;
    }
    public Integer getAttachmentSize() { return AttachmentSize; }
    public String getLobID() { return LobID; }
    public byte[] getFileData() {
        return FileData;
    }
    public boolean isChanged() {
        return Changed;
    }
    public String getPicNik() {
        return PicNik;
    }
    // =============== setter =====================
    public void setImgID(String imgID) { ImgID = imgID; }
    public void setItemCode(String itemCode) { ItemCode = itemCode; }
    public void setFileID(String fileID) { FileID = fileID; }
    public void setItemID(String itemID) {
        ItemID = itemID;
    }
    public void setAttachmentID(String attachmentID) {
        AttachmentID = attachmentID;
    }
    public void setAttachmentNo(Integer attachmentNo) {
        AttachmentNo = attachmentNo;
    }
    public void setAttachmentSize(Integer attachmentSize) { AttachmentSize = attachmentSize; }
    public void setLobID(String lobID) { LobID = lobID; }
    public void setFileData(byte[] fileData) {
        FileData = fileData;
    }
    public void setChanged(boolean changed) {
        Changed = changed;
    }
    public void setPicNik(String picNik) {
        PicNik = picNik;
    }
}
