package com.gudanggaramtbk.myppcap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import org.w3c.dom.Text;
import java.util.List;

public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {
    private MySQLiteHelper               dbHelper;
    private SharedPreferences            config;
    private ListView                     listTask;
    private List<DS_TaskItem>            lviTaskMain;
    private CustomListAdapterTaskMain    adaptertask;
    private String                       StrSessionNIK;
    private String                       StrSessionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //}
        config = getSharedPreferences("MyPPCAPSetting", MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        listTask = (ListView) findViewById(R.id.lst_main_task);

        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        StrSessionName = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK  = getIntent().getStringExtra("PIC_NIK");

        // ================== navigation drawer =============================
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        TextView nav_device_id = (TextView) hView.findViewById(R.id.txt_xdevice_id);
        TextView nav_pic_nik   = (TextView) hView.findViewById(R.id.txt_nav_pic_nik);
        TextView nav_pic_name  = (TextView) hView.findViewById(R.id.txt_nav_pic_name);
        nav_pic_nik.setText(StrSessionNIK);
        nav_pic_name.setText(StrSessionName);

        nav_device_id.setText(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();
        // ========= menu item restriction ==============
        if(StrSessionNIK.equals("999999999")){
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_load_task).setVisible(false);
            menu.findItem(R.id.nav_upload_picture).setVisible(false);
            menu.findItem(R.id.nav_capture_picture).setVisible(false);
            menu.findItem(R.id.nav_sync).setVisible(true);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);
        }
        else{
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_load_task).setVisible(true);
            menu.findItem(R.id.nav_upload_picture).setVisible(false);
            menu.findItem(R.id.nav_capture_picture).setVisible(true);
            menu.findItem(R.id.nav_sync).setVisible(true);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);

             EH_CMD_REFRESH_LIST(this);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.mn_flushData) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Warning, This Action Will Erase All Data and its Irreversible, Continue ?");

            // set dialog message
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Proceed",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    EH_CMD_EraseAll();
                                    EH_CMD_REFRESH_LIST(getBaseContext());
                                    EH_cmd_exit();
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.nav_upload_picture){
            EH_cmd_upload_picture();
        }
        else if(id == R.id.nav_capture_picture){
            EH_cmd_capture_picture();
        }
        else if(id == R.id.nav_load_task){
            EH_cmd_load_task();
        }
        else if(id == R.id.nav_sync){
            EH_cmd_sync();
        }
        else if(id == R.id.nav_setting){
            EH_cmd_setting();
        }
        else if(id == R.id.nav_about){
            EH_cmd_about();
        }
        else if(id == R.id.nav_exit){
            EH_cmd_exit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    // ========================= EVENT HANDLER NAVIGATION ==============================
    public void EH_cmd_upload_picture(){
        Intent I_Batch, xBatch;
        I_Batch = new Intent(this,BatchListActivity.class);
        I_Batch.putExtra("PIC_NIK",  StrSessionNIK);
        I_Batch.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_Batch);
    }

    public void EH_cmd_capture_picture(){
        Intent I_cBatch;
        I_cBatch = new Intent(this,CapturePSActivity.class);
        I_cBatch.putExtra("PIC_NIK",  StrSessionNIK);
        I_cBatch.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_cBatch);
    }

    public void EH_cmd_load_task(){
        Intent I_load_task;
        Log.d("[GudangGaram]", "Load Task Page Called");
        Toast.makeText(getApplicationContext(),"Load Task", Toast.LENGTH_LONG).show();
        I_load_task = new Intent(this, LoadTask.class);
        I_load_task.putExtra("PIC_NIK",  StrSessionNIK);
        I_load_task.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_load_task);
    }

    public void EH_cmd_sync(){
        Intent I_syncPicture;
        Toast.makeText(getApplicationContext(),"Sync Master Picture", Toast.LENGTH_LONG).show();
        I_syncPicture = new Intent(this, SyncAction.class);
        I_syncPicture.putExtra("PIC_NIK",  StrSessionNIK);
        I_syncPicture.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_syncPicture);
    }

    public void EH_cmd_setting(){
        Intent I_Setting;
        Log.d("[GudangGaram]", "Setting Page Called");
        Toast.makeText(getApplicationContext(),"Setting", Toast.LENGTH_LONG).show();
        I_Setting = new Intent(this, SettingActivity.class);
        I_Setting.putExtra("PIC_NIK",  StrSessionNIK);
        I_Setting.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_Setting);
    }

    public void EH_cmd_about(){
        Intent I_About;
        Log.d("[GudangGaram]", "About Page Called");
        Toast.makeText(getApplicationContext(),"About", Toast.LENGTH_LONG).show();
        I_About = new Intent(this, AboutActivity.class);
        I_About.putExtra("PIC_NIK",  StrSessionNIK);
        I_About.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_About);
    }

    public void EH_cmd_exit(){
        /*
        int pid;
        finish();
        pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        this.finishAffinity();
        */
        Intent objsignOut = new Intent(getBaseContext(),Login.class);
        objsignOut.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(objsignOut);
    }


    public void EH_CMD_REFRESH_LIST(Context ctx){
        Log.d("[GudangGaram]:", "Main Activity :: EH_CMD_REFRESH_LIST ");
        try {
            lviTaskMain = dbHelper.get_FileIDList(StrSessionNIK);
            adaptertask = new CustomListAdapterTaskMain(ctx, lviTaskMain,StrSessionNIK, StrSessionName);
            adaptertask.notifyDataSetChanged();
            listTask.setAdapter(adaptertask);
        }
        catch (Exception e) {
            Log.d("[GudangGaram]:", e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void EH_CMD_EraseAll(){
        dbHelper.flushAll();
    }
}
