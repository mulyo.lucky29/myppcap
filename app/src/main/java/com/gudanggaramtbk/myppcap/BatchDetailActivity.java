package com.gudanggaramtbk.myppcap;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * An activity representing a single Batch detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link BatchListActivity}.
 */
public class BatchDetailActivity extends AppCompatActivity  {
    private SharedPreferences config;
    private MySQLiteHelper    dbHelper;
    private BatchDetailFragment fragment;

    private String FileID, StrSessionName, StrSessionNIK;

    public void EH_CMD_UP(){
        Intent upIntent = NavUtils.getParentActivityIntent(this);
        upIntent.putExtra("PIC_NIK",  StrSessionNIK);
        upIntent.putExtra("PIC_NAME", StrSessionName);
        upIntent.putExtra("FILE_ID",  FileID);

        if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
            TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(upIntent)
                    .startActivities();
        }
        else {
            // passing intent login information before back
            NavUtils.navigateUpTo(this, upIntent);
        }
    }

    public void EH_CMD_SAVE(){
        fragment.EH_CMD_SAVE_ALL_ACTION();
    }

    public void EH_CMD_LEAVE_PAGE(){
        Log.d("[GudangGaram]", "EH_CMD_LEAVE_PAGE Begin ");
        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder
                    .setTitle("Leave Page Confirm")
                    .setMessage("Confirm Leave Page ?")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    EH_CMD_SAVE();
                                    EH_CMD_UP();
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_LEAVE_PAGE Exception " + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "EH_CMD_LEAVE_PAGE End");
        }
    }

    // need to be added if back to home
    @Override
    public void onBackPressed() {
        Log.d("[GudangGaram]", "BatchDetailActivity ::  onBackPressed");
        EH_CMD_LEAVE_PAGE();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed BatchDetailActivity Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                try{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder
                            .setTitle("Leave Page Confirm")
                            .setMessage("Confirm Leave Page ?")
                            .setCancelable(false)
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            EH_CMD_SAVE();
                                            EH_CMD_UP();
                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                catch(Exception e){
                    Log.d("[GudangGaram]", "EH_CMD_SAVE Exception " + e.getMessage().toString());
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        config = getSharedPreferences("MyPPCAPSetting", MODE_PRIVATE);

        // get information intent
        StrSessionName = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK  = getIntent().getStringExtra("PIC_NIK");
        FileID         = getIntent().getStringExtra("FILE_ID");

        Log.d("[GudangGaram]", " BatchDetailActivity Parameters ");
        Log.d("[GudangGaram]", " ==================================");
        Log.d("[GudangGaram]", " PIC_NIK  : " + StrSessionNIK);
        Log.d("[GudangGaram]", " PIC_NAME : " + StrSessionName);
        Log.d("[GudangGaram]", " FILE_ID  : " + FileID);
        Log.d("[GudangGaram]", " ==================================");

        setContentView(R.layout.activity_batch_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(BatchDetailFragment.ARG_TASK_DID, getIntent().getStringExtra(BatchDetailFragment.ARG_TASK_DID));
            arguments.putString("PIC_NIK",  StrSessionNIK);
            arguments.putString("PIC_NAME", StrSessionName);
            arguments.putString("FILE_ID",  FileID);

            // =========== create fragment ============
            fragment = new BatchDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.batch_detail_container, fragment)
                    .commit();
        }
    }

}
