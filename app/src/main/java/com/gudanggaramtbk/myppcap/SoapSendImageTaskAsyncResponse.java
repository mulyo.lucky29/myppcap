package com.gudanggaramtbk.myppcap;

/**
 * Created by LuckyM on 7/26/2018.
 */

public interface SoapSendImageTaskAsyncResponse {
    void PostSentAction(String output);
}
