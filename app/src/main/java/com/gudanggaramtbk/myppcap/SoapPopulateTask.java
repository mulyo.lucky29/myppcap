package com.gudanggaramtbk.myppcap;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;

import java.util.List;

/**
 * Created by LuckyM on 3/6/2018.
 */

public class SoapPopulateTask
{
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper    dbHelper;
    private LoadTask          pActivity;
    private ListView          populateTaskList;
    List<DS_TaskItem>         TResult;

    // override constructor
    public SoapPopulateTask(SharedPreferences PConfig, ListView lst){
        Log.d("[GudangGaram]", " SoapRetrieveTaskItem Constructor");
        this.config           = PConfig;
        this.populateTaskList = lst;
    }

    public void setContext(Context context){
        Log.d("[GudangGaram]", " SetContext");
        this.context = context;
    }

    public void setParentActivity(LoadTask pActivity){
        this.pActivity = pActivity;
    }

    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", " SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public List<DS_TaskItem> Retrieve(String pPicNIK) {
        SyncAction mx = new SyncAction();
        Log.d("[GudangGaram]", "Retrieve Task List Item For >> " + pPicNIK);
        try {
            SoapPopulateTaskTaskAsync SoapRequest = new SoapPopulateTaskTaskAsync(new SoapPopulateTaskTaskAsync.SoapRetrieveTaskTaskAsyncResponse(){
                @Override
                public void PostSentAction(List<DS_TaskItem> output) {
                    TResult = output;
                    Log.d("[GudangGaram]", "PostSentAction Output : " + output.size());
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config,pPicNIK,populateTaskList);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
        }
        return TResult;
    }
}
