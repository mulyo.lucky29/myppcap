package com.gudanggaramtbk.myppcap;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.List;

public class SyncAction extends AppCompatActivity {
    private SharedPreferences        config;
    private Context                  context;
    private MySQLiteHelper           dbHelper;
    private SoapRetrievePIC          downloadPIC;
    private SoapRetrieveReason       downloadReason;
    private SoapSendImage            sendImage;
    private SoapSendDataBase         SendDB;

    private Button                   Cmd_DMPIC;
    private Button                   Cmd_DMReason;
    private Button                   Cmd_SendImage;
    private List<String>             lFileID;
    private String                   StrSessionNIK;
    private String                   StrSessionName;
    private String                   Selected_File_ID;
    private String                   DeviceID;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Sync Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",  StrSessionNIK);
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_action);
        config = getSharedPreferences("MyPPCAPSetting", MODE_PRIVATE);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // get information intent
        StrSessionName = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK  = getIntent().getStringExtra("PIC_NIK");
        DeviceID = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        context = SyncAction.this;
        Selected_File_ID = "";

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        // initialize soap downloader master pic
        downloadPIC = new SoapRetrievePIC(config);
        downloadPIC.setContext(this);
        downloadPIC.setDBHelper(dbHelper);

        downloadReason = new SoapRetrieveReason(config);
        downloadReason.setContext(this);
        downloadReason.setDBHelper(dbHelper);

        // initialize soap send image
        sendImage   = new SoapSendImage(config);
        sendImage.setContext(this);
        sendImage.setAttribute(DeviceID);
        sendImage.setDBHelper(dbHelper);
        sendImage.setParentActivity(this);

        // init SoapSendDB
        SendDB = new SoapSendDataBase(config);
        SendDB.setContext(this);
        SendDB.setDBHelper(dbHelper);


        Cmd_DMPIC  = (Button)findViewById(R.id.cmd_down_mpic);
        Cmd_DMPIC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SYNC_M_PIC();
            }
        });

        Cmd_DMReason = (Button)findViewById(R.id.cmd_down_mreason);
        Cmd_DMReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SYNC_M_REASON();
            }
        });

        // initialize button send image
        Cmd_SendImage = (Button)findViewById(R.id.cmd_usdmiitem);
        Cmd_SendImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SEND_IMAGE();
            }
        });

        // ========= menu item restriction ==============
        if(StrSessionNIK.equals("999999999")){
            // kalo default
            Cmd_SendImage.setEnabled(false);
            Cmd_DMReason.setEnabled(true);
            Cmd_DMPIC.setEnabled(true);
        }
        else{
            // kalo login pake nik
            Cmd_SendImage.setEnabled(true);
            Cmd_DMReason.setEnabled(true);
            Cmd_DMPIC.setEnabled(true);
            loadComboBox();

            exportDb();
            /*
            if(dbHelper.isNetworkConnected(config, context)){
                try{
                    String device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    SendDB.Send(device_id);
                }
                catch(Exception e){
                }
                finally {
                }
            }
            else{
                Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
            }
            */
        }
    }

    public void loadComboBox(){
        Integer cnt;
        // init combobox
        Log.d("[GudangGaram]", "SyncAction :: loadComboBox :: ");
        lFileID = dbHelper.LoadFileID(StrSessionNIK);
        cnt = lFileID.size();
        initComboBox(R.id.cbo_sync_FileID,lFileID);
    }

    private void initComboBox(final int p_layout, List<String> lvi)
    {

        try{
            Log.d("[GudangGaram]", "SyncAction :: initComboBox :: " + lvi.size());
            Spinner spinner = (Spinner) findViewById(p_layout);
            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lvi);
            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // attaching data adapter to spinner
            spinner.setAdapter(dataAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String item = parent.getItemAtPosition(position).toString();
                    Selected_File_ID = item;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SyncAction :: Exception :: " + e.getMessage().toString());
        }
    }

    public void EH_CMD_SYNC_M_PIC(){
        int pid;
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                downloadPIC.Retrieve();
            }
            catch(Exception e){
                Toast.makeText(context,"Download Master PIC Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
            finally {
                // back to login
            }
        }
        else{
            Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
        }
    }

    public void EH_CMD_SYNC_M_REASON(){
        int pid;
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                downloadReason.Retrieve();
            }
            catch(Exception e){
                Toast.makeText(context,"Download Master Reason Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
            finally {
                // back to login
            }
        }
        else{
            Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
        }
    }

    public void EH_CMD_SEND_IMAGE(){
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                if(Selected_File_ID.equals("")){
                    Toast.makeText(context,"Please Select File ID", Toast.LENGTH_LONG).show();
                }
                else{
                    sendImage.Send(Selected_File_ID, StrSessionNIK);
                }
            }
            catch(Exception e){
                Toast.makeText(context,"Send Item Image Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
            finally {
                Toast.makeText(context,"Send Item Image Done ", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
        }
    }

    protected void exportDb() {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File dataDirectory = Environment.getDataDirectory();

        FileChannel source = null;
        FileChannel destination = null;

        String currentDBPath = "/data/" + getApplicationContext().getApplicationInfo().packageName + "/databases/MyPPCap.db";
        String backupDBPath = "MyPPCap.db";
        File currentDB = new File(dataDirectory, currentDBPath);
        File backupDB = new File(externalStorageDirectory, backupDBPath);

        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());

            Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (source != null) source.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (destination != null) destination.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
