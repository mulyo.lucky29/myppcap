package com.gudanggaramtbk.myppcap;

/**
 * Created by LuckyM on 8/13/2018.
 */

public interface SoapRetrieveImgTaskAsyncResponse {
    void PostSentAction(String output);
}

