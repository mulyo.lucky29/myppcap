package com.gudanggaramtbk.myppcap;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

/**
 * Created by LuckyM on 8/8/2018.
 */

public class SoapRetrieveReason {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper    dbHelper;
    private SyncAction        pActivity;

    // override constructor
    public SoapRetrieveReason(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapRetrieveReason :: Constructor");
        this.config           = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrieveReason :: SetContext");
        this.context = context;
    }

    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrieveReason :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Retrieve() {
        // flush table lookup before insert
        dbHelper.flushTable(dbHelper.getTableMLookup());
        Log.d("[GudangGaram]", "SoapRetrieveReason :: Retrieve List Reason Begin");
        try {
            // call WS To Retrieve Manifest from oracle and save it to local database table
            SoapRetrieveReasonTaskAsync SoapRequest = new SoapRetrieveReasonTaskAsync(new SoapRetrieveReasonTaskAsync.SoapRetrieveReasonTaskAsyncResponse() {
                @Override
                public void PostSentAction(String RTaskID) {
                    try {
                        if(RTaskID.equals("*") == true){
                        }
                        else {
                        }
                    }
                    catch (Exception e) {
                        Log.d("[GudangGaram]:", "SoapRetrieveReason Retrieve List Reason :: Exception " + e.getMessage().toString());
                    }
                    finally {
                        Log.d("[GudangGaram]:", "SoapRetrieveReason :: Retrieve List Reason End");
                    }
                }
            });

            SoapRequest.setAttribute(context, dbHelper, config);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]:", "Retrieve List Reason End");
        }
    }

}
