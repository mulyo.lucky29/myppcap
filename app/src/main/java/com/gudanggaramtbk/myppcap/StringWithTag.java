package com.gudanggaramtbk.myppcap;

/**
 * Created by LuckyM on 3/13/2018.
 */

public class StringWithTag {
    public String string;
    public Object tag;

    public StringWithTag(String string, Object tag) {
        this.string = string;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return string;
    }
}
