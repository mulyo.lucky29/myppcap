package com.gudanggaramtbk.myppcap;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 5/4/2018.
 */

public class SoapSendDataBase extends AppCompatActivity {
    private SharedPreferences config;
    private Context context;
    private SyncAction pActivity;
    private MySQLiteHelper dbHelper;

    // override constructor
    public SoapSendDataBase(SharedPreferences PConfig) {
        Log.d("[GudangGaram]: ", "SoapSendDataBase Constructor");
        this.config = PConfig;
    }

    public void setContext(Context context) {
        Log.d("[GudangGaram]: ", "SetContext");
        this.context = context;
    }
    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]: ", "SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Send(String device_id){
        try {
            // call WS To send Database
            SoapSendDataBaseTaskAsync SoapRequest = new SoapSendDataBaseTaskAsync(new SoapSendDataBaseTaskAsync.SoapSendDataBaseTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, device_id);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                Log.d("[GudangGaram]: ", "Using HC Higer");
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                Log.d("[GudangGaram]: ", "Using HC Lower");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]: ", "Send Database Exception >> " + e.getMessage().toString());
        }
    }

}
