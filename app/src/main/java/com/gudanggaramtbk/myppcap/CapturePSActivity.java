package com.gudanggaramtbk.myppcap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static java.security.AccessController.getContext;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;



public class CapturePSActivity extends AppCompatActivity implements View.OnClickListener{
    TabHost hostList;
    private String             StrSessionNIK;
    private String             StrSessionName;
    private String             DeviceID;
    private String             ChooseFilter;

    private ImageButton        Cmd_ps_Cam1;
    private ImageButton        Cmd_ps_Cam2;
    private ImageButton        Cmd_ps_Cam3;
    private ImageButton        Cmd_ps_Gal1;
    private ImageButton        Cmd_ps_Gal2;
    private ImageButton        Cmd_ps_Gal3;

    private Button             Cmd_ps_populate;
    private ImageButton        Cmd_ps_scan;
    private ImageButton        Cmd_ps_scanLot;
    private Button             Cmd_ps_send;

    private ImageView          Img_ps_1;
    private ImageView          Img_ps_2;
    private ImageView          Img_ps_3;

    private TextView           Txt_ps_ItemID;
    private TextView           Txt_ps_ItemCode;
    private TextView           Txt_ps_LotNumber;

    private TextView           Txt_ps_itemDesc;
    private TextView           Txt_ps_brand;
    private TextView           Txt_ps_spec;

    private TextView           Txt_ps_attachmentID1;
    private TextView           Txt_ps_attachmentID2;
    private TextView           Txt_ps_attachmentID3;
    private TextView           Txt_ps_blobID1;
    private TextView           Txt_ps_blobID2;
    private TextView           Txt_ps_blobID3;

    private RadioGroup         Chk_RadioGroup;
    private RadioButton        Chk_itemCode;
    private RadioButton        Chk_lotNumber;

    private SharedPreferences  config;
    private MySQLiteHelper     dbHelper;
    private ImageHelper        ihx;
    private Context            context;
    String                     storeFilename;

    private SoapPopulateInfo   populateInfo;
    private SoapSendImg        SendImg;

    private List<DS_ItemInfo>  Result;
    Bitmap                     BlankImage;
    boolean[]                  PHasChanged;


    /* gui controll */

    // ========================= setter ====================================
    public void setTxt_ps_ItemID(String txt_ps_ItemID) {
        Txt_ps_ItemID.setText(txt_ps_ItemID);
    }
    public void setTxt_ps_ItemCode(String txt_ps_ItemCode) { Txt_ps_ItemCode.setText(txt_ps_ItemCode); }
    public void setTxt_ps_LotNumber(String txt_ps_LotNumber) { Txt_ps_LotNumber.setText(txt_ps_LotNumber); }
    public void setTxt_ps_itemDesc(String txt_ps_itemDesc) { Txt_ps_itemDesc.setText(txt_ps_itemDesc); }
    public void setTxt_ps_brand(String txt_ps_brand) {
        Txt_ps_brand.setText(txt_ps_brand);
    }
    public void setTxt_ps_spec(String txt_ps_spec) {
        Txt_ps_spec.setText(txt_ps_spec);
    }
    public void setTxt_ps_attachmentID1(String txt_ps_attachmentID1) { Txt_ps_attachmentID1.setText(txt_ps_attachmentID1); }
    public void setTxt_ps_attachmentID2(String txt_ps_attachmentID2) { Txt_ps_attachmentID2.setText(txt_ps_attachmentID2); }
    public void setTxt_ps_attachmentID3(String txt_ps_attachmentID3) { Txt_ps_attachmentID3.setText(txt_ps_attachmentID3); }
    public void setTxt_ps_blobID1(String txt_ps_blobID1) {
        Txt_ps_blobID1.setText(txt_ps_blobID1);
    }
    public void setTxt_ps_blobID2(String txt_ps_blobID2) {
        Txt_ps_blobID2.setText(txt_ps_blobID2);
    }
    public void setTxt_ps_blobID3(String txt_ps_blobID3) {
        Txt_ps_blobID3.setText(txt_ps_blobID3);
    }
    // ======================== getter =====================================
    public String getTxt_ps_LotNumber() { return Txt_ps_LotNumber.getText().toString(); }


    public void flushInfo(){
        Txt_ps_ItemID.setText("-1");
        Txt_ps_ItemCode.setText("");
        Txt_ps_LotNumber.setText("");
        Txt_ps_LotNumber.setText("");

        Txt_ps_spec.setText("");
        Txt_ps_itemDesc.setText("");
        Txt_ps_brand.setText("");

        Txt_ps_attachmentID1.setText("");
        Txt_ps_attachmentID2.setText("");
        Txt_ps_attachmentID3.setText("");
        Txt_ps_blobID1.setText("");
        Txt_ps_blobID2.setText("");
        Txt_ps_blobID3.setText("");
    }

    public void flushImg(){
        Img_ps_1.setImageBitmap(BlankImage);
        Img_ps_2.setImageBitmap(BlankImage);
        Img_ps_3.setImageBitmap(BlankImage);
    }
    public void setImg_ps_1(Bitmap img_bmp) {
        if(img_bmp.getByteCount() > 0){
            Img_ps_1.setImageBitmap(img_bmp);
        }
        else {
            Img_ps_1.setImageBitmap(BlankImage);
        }
    }
    public void setImg_ps_2(Bitmap img_bmp) {
        if(img_bmp.getByteCount() > 0){
            Img_ps_2.setImageBitmap(img_bmp);
        }
        else {
            Img_ps_2.setImageBitmap(BlankImage);
        }
    }
    public void setImg_ps_3(Bitmap img_bmp) {
        if(img_bmp.getByteCount() > 0){
            Img_ps_3.setImageBitmap(img_bmp);
        }
        else {
            Img_ps_3.setImageBitmap(BlankImage);
        }
    }

    // ======================= getter ======================================
    public ImageView getImg_ps_1() {
        return Img_ps_1;
    }
    public ImageView getImg_ps_2() {
        return Img_ps_2;
    }
    public ImageView getImg_ps_3() {
        return Img_ps_3;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_ps);

        // set helper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        ihx = new ImageHelper();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        config  = getSharedPreferences("MyPPCAPSetting", MODE_PRIVATE);
        context = CapturePSActivity.this;

        BlankImage = BitmapFactory.decodeResource(getResources(), R.drawable.noimage);

        // get information intent
        StrSessionName = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK  = getIntent().getStringExtra("PIC_NIK");
        DeviceID       = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        hostList = (TabHost)findViewById(R.id.TabCapture);
        hostList.setup();

        try{
            //Tab 1
            TabHost.TabSpec spec = hostList.newTabSpec("Item Capture");
            spec.setContent(R.id.TabBodyItem);
            spec.setIndicator("Item Info");
            hostList.addTab(spec);

            //Tab 2
            spec = hostList.newTabSpec("Receiving List");
            spec.setContent(R.id.TabBodyImage);
            spec.setIndicator("Item Picture");
            hostList.addTab(spec);

            // focus on tab 1
            hostList.setCurrentTab(0);
        }
        catch(Exception e){
        }

        // initialized Status PHasChanged
        PHasChanged = new boolean[3];
        Arrays.fill(PHasChanged, false);


        Chk_itemCode         = (RadioButton) findViewById(R.id.chk_itemCode);
        Chk_lotNumber        = (RadioButton) findViewById(R.id.chk_lotNumber);
        Chk_RadioGroup       = (RadioGroup) findViewById(R.id.chk_radioGroup);

        Txt_ps_ItemID        = (TextView)    findViewById(R.id.txt_ps_item_id);
        Txt_ps_ItemCode      = (TextView)    findViewById(R.id.txt_ps_itemCode);
        Txt_ps_LotNumber     = (TextView)    findViewById(R.id.txt_ps_lotNumber);

        Txt_ps_itemDesc      = (TextView)    findViewById(R.id.txt_ps_itemDesc);
        Txt_ps_brand         = (TextView)    findViewById(R.id.txt_ps_brand);
        Txt_ps_spec          = (TextView)    findViewById(R.id.txt_ps_spec);

        Img_ps_1             = (ImageView)   findViewById(R.id.img_ps1);
        Img_ps_2             = (ImageView)   findViewById(R.id.img_ps2);
        Img_ps_3             = (ImageView)   findViewById(R.id.img_ps3);
        Cmd_ps_Cam1          = (ImageButton) findViewById(R.id.cmd_ps_cam1);
        Cmd_ps_Cam2          = (ImageButton) findViewById(R.id.cmd_ps_cam2);
        Cmd_ps_Cam3          = (ImageButton) findViewById(R.id.cmd_ps_cam3);
        Cmd_ps_Gal1          = (ImageButton) findViewById(R.id.cmd_ps_gal1);
        Cmd_ps_Gal2          = (ImageButton) findViewById(R.id.cmd_ps_gal2);
        Cmd_ps_Gal3          = (ImageButton) findViewById(R.id.cmd_ps_gal3);
        Cmd_ps_populate      = (Button)      findViewById(R.id.cmd_ps_populate);
        Cmd_ps_scan          = (ImageButton) findViewById(R.id.cmd_ps_scan);
        Cmd_ps_scanLot       = (ImageButton) findViewById(R.id.cmd_ps_scanLot);
        Cmd_ps_send          = (Button)      findViewById(R.id.cmd_ps_send);

        Txt_ps_attachmentID1 = (TextView)    findViewById(R.id.txt_ps_attachmentID1);
        Txt_ps_attachmentID2 = (TextView)    findViewById(R.id.txt_ps_attachmentID2);
        Txt_ps_attachmentID3 = (TextView)    findViewById(R.id.txt_ps_attachmentID3);
        Txt_ps_blobID1       = (TextView)    findViewById(R.id.txt_ps_blobID1);
        Txt_ps_blobID2       = (TextView)    findViewById(R.id.txt_ps_blobID2);
        Txt_ps_blobID3       = (TextView)    findViewById(R.id.txt_ps_blobID3);

        Chk_itemCode         = (RadioButton) findViewById(R.id.chk_itemCode);
        Chk_lotNumber        = (RadioButton) findViewById(R.id.chk_lotNumber);

        // ------------------ set click listener --------------------
        Cmd_ps_Cam1.setOnClickListener(this);
        Cmd_ps_Cam2.setOnClickListener(this);
        Cmd_ps_Cam3.setOnClickListener(this);
        Cmd_ps_Gal1.setOnClickListener(this);
        Cmd_ps_Gal2.setOnClickListener(this);
        Cmd_ps_Gal3.setOnClickListener(this);
        Cmd_ps_populate.setOnClickListener(this);
        Cmd_ps_scan.setOnClickListener(this);
        Cmd_ps_scanLot.setOnClickListener(this);
        Cmd_ps_send.setOnClickListener(this);


        // initialized soap service for Populate Info
        populateInfo = new SoapPopulateInfo(config);
        populateInfo.setContext(CapturePSActivity.this);
        populateInfo.setDBHelper(dbHelper);
        populateInfo.setParentActivity(CapturePSActivity.this);

        // initialized soap service for Sending Image
        SendImg = new SoapSendImg(config);
        SendImg.setContext(CapturePSActivity.this);
        SendImg.setAttribute(DeviceID);
        SendImg.setParentActivity(CapturePSActivity.this);
        SendImg.setDBHelper(dbHelper);

        Img_ps_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_1 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_ps_1.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, context);
            }
        });
        Img_ps_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_2 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_ps_2.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, context);
            }
        });
        Img_ps_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_3 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_ps_3.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, context);
            }
        });

        Chk_RadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // clear input
                flushInfo();
                flushImg();

                if(checkedId == R.id.chk_itemCode) {
                    Txt_ps_ItemCode.setEnabled(true);
                    Txt_ps_LotNumber.setEnabled(false);
                    Txt_ps_ItemCode.requestFocus();

                    Cmd_ps_scan.setEnabled(true);
                    Cmd_ps_scanLot.setEnabled(false);
                    ChooseFilter = "ItemCode";
                }
                else if(checkedId == R.id.chk_lotNumber) {
                    Txt_ps_ItemCode.setEnabled(false);
                    Txt_ps_LotNumber.setEnabled(true);
                    Txt_ps_LotNumber.requestFocus();

                    Cmd_ps_scan.setEnabled(false);
                    Cmd_ps_scanLot.setEnabled(true);
                    ChooseFilter = "LotNumber";
                }
            }
        });

        // init state filter search
        Txt_ps_ItemCode.setEnabled(true);
        Txt_ps_LotNumber.setEnabled(false);
        Txt_ps_ItemCode.requestFocus();
        Cmd_ps_scan.setEnabled(true);
        Cmd_ps_scanLot.setEnabled(false);
        ChooseFilter = "ItemCode";

    }

    /* event handler */

    @Override
    public void onClick(View v) {
        int id = v.getId();
            if(id == R.id.cmd_ps_cam1){
                Log.d("[GudangGaram]", "OnClick Cmd Cam1");
                EH_CMD_TAKE_PICTURE(1);
            }
            else if(id == R.id.cmd_ps_cam2){
                Log.d("[GudangGaram]", "OnClick Cmd Cam2");
                EH_CMD_TAKE_PICTURE(2);
            }
            else if(id == R.id.cmd_ps_cam3){
                Log.d("[GudangGaram]", "OnClick Cmd Cam3");
                EH_CMD_TAKE_PICTURE(3);
            }
            else if(id == R.id.cmd_ps_gal1){
                Log.d("[GudangGaram]", "OnClick Cmd Gal1");
                EH_CMD_TAKE_GALLERY(1);
            }
            else if(id == R.id.cmd_ps_gal2){
                Log.d("[GudangGaram]", "OnClick Cmd Gal2");
                EH_CMD_TAKE_GALLERY(2);
            }
            else if(id == R.id.cmd_ps_gal3){
                Log.d("[GudangGaram]", "OnClick Cmd Gal3");
                EH_CMD_TAKE_GALLERY(3);
            }
            else if(id == R.id.cmd_ps_populate){
                EH_CMD_POPULATE_INFO_ITEM();
            }
            else if(id == R.id.cmd_ps_send){
                EH_CMD_SEND();
            }
            else if(id == R.id.cmd_ps_scan){
                EH_CMD_SCAN();
            }
            else if(id == R.id.cmd_ps_scanLot){
                EH_CMD_SCAN_LOT();
            }
    }

    public void EH_CMD_POPULATE_INFO_ITEM(){
        String p_item_code;
        String p_barcode_lot;

        if(ChooseFilter.equals("ItemCode")){
            Toast.makeText(getApplicationContext(),"ItemCode", Toast.LENGTH_LONG).show();
            try{
                if(Txt_ps_ItemCode.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Please Mention Item Code", Toast.LENGTH_LONG).show();
                }
                else{
                    p_item_code   = Txt_ps_ItemCode.getText().toString();
                    Log.d("[GudangGaram]", "EH_CMD_POPULATE_INFO_ITEM (ItemCode): " + p_item_code);
                    Result = populateInfo.Retrieve(ChooseFilter,p_item_code);
                }
            }
            catch(Exception e){
                Log.d("[GudangGaram]","EH_CMD_POPULATE_INFO_ITEM (ItemCode) Exception :" + e.getMessage());
            }
        }
        else{
            Toast.makeText(getApplicationContext(),"BarcodeLot", Toast.LENGTH_LONG).show();
            try{
              if(Txt_ps_LotNumber.getText().length() == 0){
                  Toast.makeText(getApplicationContext(),"Please Mention Barcode Lot Number", Toast.LENGTH_LONG).show();
              }
              else{
                  p_barcode_lot = Txt_ps_LotNumber.getText().toString();
                  Log.d("[GudangGaram]", "EH_CMD_POPULATE_INFO_ITEM (LotNumber): " + p_barcode_lot);
                  Result = populateInfo.Retrieve(ChooseFilter,p_barcode_lot);
              }
            }
            catch(Exception e){
                Log.d("[GudangGaram]","EH_CMD_POPULATE_INFO_ITEM (LotNumber) Exception :" + e.getMessage());
            }
        }
    }

    public void EH_CMD_POPUP_DIALOG(Bitmap oBitmap, Context ctx){
        Log.d("[GudangGaram]", "EH_CMD_POPUP_DIALOG Begin");
        ihx.show_image_popup(oBitmap,ctx);
        Log.d("[GudangGaram]", "EH_CMD_POPUP_DIALOG End");
    }

    // need to be added if back to home
    public void EH_CMD_UP(){
        // move to home
        Log.d("[GudangGaram]", "EH_CMD_UP Begin ");
        Intent upIntent = NavUtils.getParentActivityIntent(this);
        if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
            TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(upIntent)
                    .startActivities();
        } else {
            // passing intent login information before back
            upIntent.putExtra("PIC_NIK",  StrSessionNIK);
            upIntent.putExtra("PIC_NAME", StrSessionName);
            NavUtils.navigateUpTo(this, upIntent);
        }
        Log.d("[GudangGaram]", "EH_CMD_UP End ");
    }

    public void EH_CMD_LEAVE_PAGE(){
        Log.d("[GudangGaram]", "EH_CMD_LEAVE_PAGE Begin ");
        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder
                    .setTitle("Leave Page Confirm")
                    .setMessage("Confirm Leave Page ?")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    EH_CMD_UP();
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_LEAVE_PAGE Exception " + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "EH_CMD_LEAVE_PAGE End");
        }
    }
    public void EH_CMD_SCAN_LOT(){
        Log.d("[GudangGaram]", "EH_CMD_SCAN LOT Begin");
        // clear all first
        try{
            Log.d("[GudangGaram]", "Scan Lot :: onActivityResult Expected  : " + IntentIntegrator.REQUEST_CODE);
            IntentIntegrator integrator = new IntentIntegrator(CapturePSActivity.this);
            integrator.setCaptureActivity(CaptureCustomeActivity.class);
            integrator.setBeepEnabled(true);
            integrator.initiateScan();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_SCAN LOT Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "EH_CMD_SCAN LOT End");
    }

    public void EH_CMD_SCAN(){
        Log.d("[GudangGaram]", "EH_CMD_SCAN Begin");
            // clear all first
            try{
                Log.d("[GudangGaram]", "Scan Item Code :: onActivityResult Expected  : " + IntentIntegrator.REQUEST_CODE);
                IntentIntegrator integrator = new IntentIntegrator(CapturePSActivity.this);
                integrator.setCaptureActivity(CaptureCustomeActivity.class);
                integrator.setBeepEnabled(true);
                integrator.initiateScan();
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "EH_CMD_SCAN Exception " + e.getMessage().toString());
            }
        Log.d("[GudangGaram]", "EH_CMD_SCAN End");
    }
    public void EH_CMD_TAKE_PICTURE(Integer index){
        Uri photoURI;
        Log.d("[GudangGaram]", "EH_CMD_TAKE_PICTURE " + index.toString() + " : " + storeFilename + " Begin");
        if(Txt_ps_ItemCode.getText().length() == 0){
            Toast.makeText(context,"Cannot Take Picture, Item Code Is Blank", Toast.LENGTH_LONG).show();
        }
        else{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
            String sysdate              = dateFormat.format(new Date());
            storeFilename               = "PPCAP_" + sysdate + ".jpg";
            String image_path = Environment.getExternalStorageDirectory() + "/" + storeFilename;
            File img = new File (image_path);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                photoURI = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", img);
            }
            else {
                photoURI = Uri.fromFile(img);
            }

            try{
                Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                photoCaptureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                photoCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(photoCaptureIntent, index);
            }
            catch (Exception e){
                Log.d("[GudangGaram]", "EH_CMD_TAKE_PICTURE Exception : " + e.getMessage());
            }
        }
        Log.d("[GudangGaram]", "EH_CMD_TAKE_PICTURE " + index.toString() + " : " + storeFilename + " end");
    }

    public void EH_CMD_TAKE_GALLERY(Integer index){
        Log.d("[GudangGaram]", "EH_CMD_TAKE_GALLERY " + index.toString() + " Begin");
        if(Txt_ps_ItemCode.getText().length() == 0){
            Toast.makeText(context,"Cannot Acces Gallery, Item Code Is Blank", Toast.LENGTH_LONG).show();
        }
        else {
            try {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*.jpg");
                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), index + 100);
            }
            catch (Exception e) {
                Log.d("[GudangGaram]", "EH_CMD_TAKE_GALLERY " + index.toString());
            }
        }
        Log.d("[GudangGaram]", "EH_CMD_TAKE_GALLERY " + index.toString() + " End");
    }


    public void EH_CMD_SEND_ACTION(){
        byte[] fileData ={0};
        byte[] blankData = {0};
        String v_attch1, v_attch2, v_attch3;
        String v_lob1, v_lob2, v_lob3;

        Log.d("[GudangGaram]", "EH_CMD_SEND_ACTION begin");
        //if((PHasChanged[0] == true) || (PHasChanged[1] == true) || (PHasChanged[2] == true)) {

            Log.d("[GudangGaram]", "EH_CMD_SEND_ACTION HasChanged");
            List<DS_ItemImg> lstDI = new ArrayList<>();
            lstDI.clear();

            try{
                for(int itr= 1; itr<=3; itr++ ){
                    DS_ItemImg ds_send = new DS_ItemImg();

                    ds_send.setItemID(Txt_ps_ItemID.getText().toString());
                    ds_send.setItemCode(Txt_ps_ItemCode.getText().toString());
                    ds_send.setAttachmentNo(itr);
                    if(itr == 1) {
                        if(Txt_ps_attachmentID1.getText().length() > 0){
                            v_attch1 =  Txt_ps_attachmentID1.getText().toString();
                        }
                        else{
                            v_attch1 = "-1";
                        }
                        if(Txt_ps_blobID1.getText().length() > 0){
                            v_lob1 = Txt_ps_blobID1.getText().toString();
                        }
                        else{
                            v_lob1 = "0";
                        }
                        ds_send.setAttachmentID(v_attch1);
                        ds_send.setLobID(v_lob1);
                        fileData =  ihx.getByteImageView(Img_ps_1);
                    }
                    else if(itr == 2){
                        if(Txt_ps_attachmentID2.getText().length() > 0){
                            v_attch2 = Txt_ps_attachmentID2.getText().toString();
                        }
                        else{
                            v_attch2 = "-1";
                        }
                        if(Txt_ps_blobID2.getText().length() > 0){
                            v_lob2 = Txt_ps_blobID2.getText().toString();
                        }
                        else{
                            v_lob2 = "0";
                        }
                        ds_send.setAttachmentID(v_attch2);
                        ds_send.setLobID(v_lob2);
                        fileData =  ihx.getByteImageView(Img_ps_2);
                    }
                    else if(itr == 3){
                        if(Txt_ps_attachmentID3.getText().length() > 0){
                            v_attch3 = Txt_ps_attachmentID3.getText().toString();
                        }
                        else{
                            v_attch3 = "-1";
                        }
                        if(Txt_ps_blobID3.getText().length() > 0){
                            v_lob3 = Txt_ps_blobID3.getText().toString();
                        }
                        else{
                            v_lob3 = "0";
                        }
                        ds_send.setAttachmentID(v_attch3);
                        ds_send.setLobID(v_lob3);
                        fileData =  ihx.getByteImageView(Img_ps_3);
                    }

                    if(fileData.length > 0){
                        ds_send.setFileData(fileData);
                    }
                    else{
                        ds_send.setFileData(blankData);
                    }

                    ds_send.setPicNik(StrSessionNIK);
                    lstDI.add(ds_send);
                }
                SendImg.Send(lstDI);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "EH_CMD_SEND_ACTION Exception : " + e.getMessage().toString());
            }
            finally {
                Toast.makeText(context,"Send Done", Toast.LENGTH_LONG).show();
            }
        //}
        //else{
        //    Log.d("[GudangGaram]", "EH_CMD_SEND_ACTION Nothing");
        //    Toast.makeText(context,"Send Nothing, No Changed Detected", Toast.LENGTH_LONG).show();
        //}
        //Log.d("[GudangGaram]", "EH_CMD_SEND_ACTION end");
    }

    /* ================= event handler action ========================*/
    public void EH_CMD_SEND(){
        Log.d("[GudangGaram]", "EH_CMD_SEND Begin");
        if(Txt_ps_ItemCode.getText().length() == 0){
            Toast.makeText(context,"Cannot Send Picture, Item Code Is Blank", Toast.LENGTH_LONG).show();
        }
        else {
            try {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder
                        .setTitle("Send Image Confirm")
                        .setMessage("Confirm Send Image To Oracle ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        EH_CMD_SEND_ACTION();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            } catch (Exception e) {
                Log.d("[GudangGaram]", "EH_CMD_SEND Exception " + e.getMessage().toString());
            }
        }
        Log.d("[GudangGaram]", "EH_CMD_SEND End");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Setting Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                EH_CMD_LEAVE_PAGE();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        EH_CMD_LEAVE_PAGE();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("[GudangGaram]", "onActivityResult : " + requestCode);

        // intent from barcode scanner
        if(requestCode == IntentIntegrator.REQUEST_CODE){
            if (resultCode == RESULT_CANCELED){
            }
            else
            {
                IntentResult ScanResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
                if(ScanResult != null){
                    String StrScanResult = ScanResult.getContents();
                    if(ChooseFilter.equals("ItemCode")){
                        Txt_ps_ItemCode.setText(StrScanResult);
                    }
                    else{
                        Txt_ps_LotNumber.setText(StrScanResult);
                    }
                    EH_CMD_POPULATE_INFO_ITEM();
                }
            }
        }
        else{
            // if not intent from scanner
            if(requestCode < 100){
                // intent from take picture request
                if(resultCode == Activity.RESULT_OK){
                    // load as bitmap from sdcard already compressed
                    Bitmap mbitmap = ihx.getImageFileFromSDCard(storeFilename,true,"SC");
                    if (requestCode == 1) {
                        Img_ps_1.setImageBitmap(mbitmap);
                        // mark that blob has changed
                        Txt_ps_blobID1.setText("-1");
                    }
                    else if(requestCode == 2) {
                        Img_ps_2.setImageBitmap(mbitmap);
                        // mark that blob has changed
                        Txt_ps_blobID2.setText("-1");
                    }
                    else if(requestCode == 3) {
                        Img_ps_3.setImageBitmap(mbitmap);
                        // mark that blob has changed
                        Txt_ps_blobID3.setText("-1");
                    }

                    // set flag has change
                    try {
                        Log.d("[GudangGaram]", "Request Code " + requestCode);
                        PHasChanged[requestCode-1] = true;
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]", "Capture Exception " + e.getMessage());
                    }
                    finally {
                        storeFilename = "";
                    }
                }
            }
            else{
                if(resultCode == Activity.RESULT_OK){
                    Uri selectedImageUri = data.getData();
                    String selectedImagePath = selectedImageUri.getPath();

                    if(requestCode == 101){
                        Img_ps_1.setImageBitmap(ihx.getImageFromUri(Img_ps_1,selectedImageUri,true,"SG"));
                        // mark that blob has changed
                        Txt_ps_blobID1.setText("-1");
                    }
                    else if(requestCode == 102){
                        Img_ps_2.setImageBitmap(ihx.getImageFromUri(Img_ps_2,selectedImageUri,true,"SG"));
                        // mark that blob has changed
                        Txt_ps_blobID2.setText("-1");
                    }
                    else if(requestCode == 103){
                        Img_ps_3.setImageBitmap(ihx.getImageFromUri(Img_ps_3,selectedImageUri,true,"SG"));
                        // mark that blob has changed
                        Txt_ps_blobID3.setText("-1");
                    }
                    // set flag has change
                    try{
                        PHasChanged[requestCode-101] = true;
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]", "Exception " + e.getMessage());
                    }
                }
            }
        }
    }
}
