package com.gudanggaramtbk.myppcap;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by LuckyM on 7/23/2018.
 */

public class ViewHolderMain {
    public CheckBox Mchk_M_select;
    public TextView Mtxt_M_task_id;
    public TextView Mtxt_M_file_id;
    public TextView Mtxt_M_Status;
    public Button   Mcmd_M_go;
}
