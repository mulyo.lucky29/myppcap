package com.gudanggaramtbk.myppcap;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 7/26/2018.
 */

public class SoapSendImage {
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper    dbHelper;
    private String            DeviceID;
    SyncAction                pActivity;

    // override constructor
    public SoapSendImage(SharedPreferences PConfig) {
        Log.d("[GudangGaram]", "SoapSendImage :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context) {
        Log.d("[GudangGaram]", "SoapSendImage :: SetContext");
        this.context = context;
    }
    public void setAttribute(String pDeviceID){
        this.DeviceID = pDeviceID;
    }
    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapSendImage :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Send(String pFileID, String pPicNIK) {
        String taskdid, seq, pwhere;
        long RecordCount = 0;

        pwhere = "WHERE TP_TaskDID IN ( " +
                      " SELECT TT_TaskDID " +
                      " FROM GGGG_PPCAP_TT_TASK " +
                      " WHERE TT_FileID = " + pFileID + " " +
                      "   AND TT_PIC_NIK = '" + pPicNIK + "') ";

        Log.d("[GudangGaram]", "SoapSendImage :: Send");
        Log.d("[GudangGaram]", "Device ID  : " + DeviceID);
        Log.d("[GudangGaram]", "Send Query : " + pwhere);

        try {
            // check if data to be sent is exists first to avoid duplicate
            RecordCount = dbHelper.getRecordCount("GGGG_PPCAP_TP_PICTURE", pwhere);
            Log.d("[GudangGaram]", "Count Record To Be Send :" + RecordCount);
            if (RecordCount > 0) {
                List<DS_TaskItemPicture> LFI = new ArrayList<DS_TaskItemPicture>();
                LFI = dbHelper.get_ImageFileID(pFileID, pPicNIK);
                Log.d("[GudangGaram]", "SoapSendImage :: EH_SEND_IMAGE :: SentImage COUNT " + LFI.size());

                if(LFI.size() > 0){
                    try{
                        for(int idx=0; idx < LFI.size(); idx++){
                            Log.d("[GudangGaram]", "======================= EH_SEND_IMAGE idx = : " + idx + " BEGIN ============================== ");

                            DS_TaskItemPicture tip = new DS_TaskItemPicture();
                            final String FileID   = LFI.get(idx).getFileID();
                            final String TaskDID  = LFI.get(idx).getTaskDID();
                            final Integer Seq     = LFI.get(idx).getAttachmentNo();
                            final String  PicNIK  = pPicNIK;

                            final String info     = Integer.toString(idx + 1) + " of " + Integer.toString(LFI.size());

                            if(Seq > 0){
                                tip.setFileID(FileID);
                                tip.setTaskDID(TaskDID);
                                tip.setAttachmentNo(Seq);
                                tip.setItemID(LFI.get(idx).getItemID());
                                tip.setAttachmentID(LFI.get(idx).getAttachmentID());
                                tip.setLobID(LFI.get(idx).getLobID());
                                tip.setCreationDate(LFI.get(idx).getCreationDate());
                                tip.setReason(LFI.get(idx).getReason());

                                // call WS To send Header To Oracle
                                SoapSendImageTaskAsync SoapRequest = new SoapSendImageTaskAsync(new SoapSendImageTaskAsync.SoapSendImageTaskAsyncResponse() {
                                    @Override
                                    public void PostSentAction(String p_task_did) {
                                        Log.d("[GudangGaram]", "PostSentAction : " + p_task_did);
                                        try{
                                            if(Integer.parseInt(p_task_did) > 0) {
                                                // after header sent then summon procedure to send do detail based on do_header_id
                                                dbHelper.deleteItemPicture(FileID, PicNIK, TaskDID, Seq.toString());
                                                Log.d("[GudangGaram]", "Delete File ID = " + FileID + " Task DID " + TaskDID + " Seq " + Seq.toString() + " Done");
                                                pActivity.loadComboBox();
                                            }
                                        }
                                        catch(Exception e){
                                            Log.d("[GudangGaram]", "SoapSendImage Exception : " + e.getMessage().toString());
                                        }
                                    }
                                });
                                SoapRequest.setParam(tip);
                                SoapRequest.setAttribute(context, dbHelper, config, DeviceID,info);
                                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                                    //work on sgs3 android 4.0.4
                                    //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                                    Log.d("[GudangGaram]", "Using HC Higer");
                                    SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                                }
                                else {
                                    Log.d("[GudangGaram]", "Using HC Lower");
                                    SoapRequest.execute(); // work on sgs2 android 2.3
                                }
                            }
                            else{
                                // kalo picture seq nya - 1 langsung delete
                                dbHelper.deleteItemPicture(FileID, PicNIK, TaskDID, Seq.toString());
                                Log.d("[GudangGaram]", " Delete File ID = " + FileID + " Task DID " + TaskDID + " Seq " + Seq.toString() + " Done");
                            }
                            Log.d("[GudangGaram]", "======================= EH_SEND_IMAGE idx = : " + idx + " END =============================== ");
                        } // end loop
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]", "SentDOHeaderToOracle Exception " + e.getMessage().toString());
                    }
                }
                else{
                    Log.d("[GudangGaram]", "SentDOHeaderToOracle No Data");
                }
            } else {
                Toast.makeText(context, "No Image Can Be Sent To Oracle", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]", "SoapSendImage :: Send :: Exception >> " + e.getMessage().toString());
        }
    }

}
