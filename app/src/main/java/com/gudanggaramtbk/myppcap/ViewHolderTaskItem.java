package com.gudanggaramtbk.myppcap;

import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by LuckyM on 6/8/2018.
 */

public class ViewHolderTaskItem {
    public CheckBox Xchk_select;
    public TextView Xtxt_task_id;
    public TextView Xtxt_file_id;
    public TextView Xtxt_file_name;
    public TextView Xtxt_task_date;
}

