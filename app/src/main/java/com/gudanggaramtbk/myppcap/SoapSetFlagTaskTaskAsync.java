package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 3/13/2018.
 */

public class SoapSetFlagTaskTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper    dbHelper;

    private String            Tp_task_id;
    private String            Tp_pic_nik;
    private String            Tp_device_id;
    private String            SentResponse;
    private ListView          listTaskMI;
    SoapPopulateTask          populateTaskList;

    public SoapSetFlagTaskTaskAsync(){
    }

    public interface SoapSetFlagTaskTaskAsyncResponse {
        void PostSentAction(String output);
    }

    public SoapSetFlagTaskTaskAsync.SoapSetFlagTaskTaskAsyncResponse delegate = null;
    public SoapSetFlagTaskTaskAsync(SoapSetFlagTaskTaskAsync.SoapSetFlagTaskTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             ListView listTaskMI,
                             String p_task_id,
                             String p_pic_nik,
                             String p_device_id){

        Log.d("[GudangGaram]", " SoapSetFlagTaskTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.listTaskMI     = listTaskMI;
        this.Tp_task_id     = p_task_id;
        this.Tp_pic_nik     = p_pic_nik;
        this.Tp_device_id   = p_device_id;
        this.pd = new ProgressDialog(this.context);

        // WS init
        populateTaskList = new SoapPopulateTask(config, listTaskMI);
        populateTaskList.setContext(context);
        populateTaskList.setDBHelper(dbHelper);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapSetFlagTaskTaskAsync :: onPreExecute");

        if(pd.isShowing()){
            pd.dismiss();
        }
        pd.setMessage("Flag Task From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        if(pd.isShowing()){
            pd.dismiss();
        }
        pd.setMessage(args[0].toString());
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        Log.d("[GudangGaram]", " SoapSetFlagTaskTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", " SoapSetFlagTaskTaskAsync :: onPostExecute >> " + SentResponse);
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                populateTaskList.Retrieve(Tp_pic_nik);
                Log.d("[GudangGaram]:", "SoapSetManifestFlagTaskAsync :: populateRakPersiapan.Retrieve");
            }
            catch(Exception e){
            }
        }
        delegate.PostSentAction(output);
        Toast.makeText(context,"Set Flag Task Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;

        String NAMESPACE        = config.getString("SoapNamespace", "");
        String SOAP_ADDRESS     = config.getString("SoapAddress", "");
        String OPERATION_NAME   = "Set_Flag_Task";

        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;
        Log.d("[GudangGaram]", " SOAP_ADDRESS   :" + SOAP_ADDRESS);
        Log.d("[GudangGaram]", " SOAP_ACTION    :" + SOAP_ACTION);
        SoapObject request = new SoapObject(NAMESPACE,OPERATION_NAME);
        Log.d("[GudangGaram]", " SOAP REQUEST   :" + request.toString());


        Log.d("[GudangGaram]", " SOAP Parameter");
        Log.d("[GudangGaram]", " p_task_id    : " + Tp_task_id);
        Log.d("[GudangGaram]", " p_pic_nik    : " + Tp_pic_nik);
        Log.d("[GudangGaram]", " p_device_id  : " + Tp_device_id);


        // =============== p_task_id ========================
        PropertyInfo prop_p_task_id = new PropertyInfo();
        prop_p_task_id.setName("p_task_id");
        prop_p_task_id.setValue(Tp_task_id);
        prop_p_task_id.setType(Integer.class);

        // =============== p_pic_nik ========================
        PropertyInfo prop_p_pic_nik = new PropertyInfo();
        prop_p_pic_nik.setName("p_pic_nik");
        prop_p_pic_nik.setValue(Tp_pic_nik);
        prop_p_pic_nik.setType(String.class);

        // =============== p_device_id ========================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(Tp_device_id);
        prop_p_device_id.setType(String.class);


        request.addProperty(prop_p_task_id);
        request.addProperty(prop_p_pic_nik);
        request.addProperty(prop_p_device_id);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            try{
                SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "HTTP RESPONSE Exception:" + e.getMessage().toString());
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishProgress("Flagged Task ID : " + Tp_task_id);
        }
        catch (Exception ex) {
            SentResponse = ex.getMessage().toString();
            Log.d("[GudangGaram]", "Catch : " + SentResponse);
            //response = "<?xml version='1.0' encoding='utf-8'?><Message response='FAIL' ErrorMessage='The System is under maintenance or having some techical issue.'></Message>";
        }

        return "";
    }
}
