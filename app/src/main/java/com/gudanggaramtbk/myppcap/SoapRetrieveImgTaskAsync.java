package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import org.kobjects.base64.Base64;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import static org.kobjects.base64.Base64.decode;

/**
 * Created by LuckyM on 8/13/2018.
 */

public class SoapRetrieveImgTaskAsync extends AsyncTask<String, String, String> {
        private SharedPreferences config;
        private Context context;
        private ProgressDialog pd;
        private String                    ResultResponse;
        private MySQLiteHelper            dbHelper;
        private String                    SentResponse;
        private String                    TaskDID;
        private Integer                   Seq;
        private String                    message;

        //private List<DS_TaskItemDetail>         Result;
        private ArrayAdapter<DS_TaskItemDetail> trxdadapter;
        public SoapRetrieveImgTaskAsync(Context context){
            super();
            this.context = context;
        }

        public interface SoapRetrieveImgTaskAsyncResponse {
            void PostSentAction(String output);
        }
        public SoapRetrieveImgTaskAsync.SoapRetrieveImgTaskAsyncResponse delegate = null;
        public SoapRetrieveImgTaskAsync(SoapRetrieveImgTaskAsync.SoapRetrieveImgTaskAsyncResponse delegate){
            this.delegate = delegate;
        }
        public void setContext(Context context){
        this.context = context;
    }
        public void setAttribute(Context context,
                                 MySQLiteHelper    dbHelper,
                                 SharedPreferences config,
                                 String            pTaskDID){

            Log.d("[GudangGaram]", " SoapRetrieveImgTaskAsync :: setAttribute");
            this.context          = context;
            this.dbHelper         = dbHelper;
            this.config           = config;
            this.TaskDID          = pTaskDID;
            this.pd = new ProgressDialog(this.context);
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("[GudangGaram]", "SoapRetrieveImgTaskAsync :: onPreExecute :: " + message);
            pd.setMessage("Retrieve Image From Oracle ");
            pd.setIndeterminate(false);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected  void onProgressUpdate(String... args) {
            super.onProgressUpdate(args);
            if(pd.isShowing()){
                pd.dismiss();
                pd.setMessage(args[0].toString());
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setCancelable(false);
                pd.show();
            }
            Log.d("[GudangGaram]", " SoapRetrieveImgTaskAsync :: onProgressUpdate :: " + args[0].toString());
        }

        @Override
        protected void onPostExecute(String output) {
            super.onPostExecute(output);
            Log.d("[GudangGaram]", "SoapRetrieveImageTask :: onPostExecute >> " + ResultResponse);
            try{
                delegate.PostSentAction(ResultResponse);
            }
            catch(Exception e){
            }
            finally {
                if(output.equals("-1")){
                    //Toast.makeText(context,"Retrieve Image ID = " + TaskDID + " Seq = " +  Seq + " Task Done", Toast.LENGTH_LONG).show();
                }
                else{
                    //Toast.makeText(context,"Retrieve Image ID = " + TaskDID + " Seq = " +  Seq + " Task Done", Toast.LENGTH_LONG).show();
                }
            pd.dismiss();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            Long     Result;
            String   v_TaskDID;
            String   v_fileID;
            Integer  v_Seq;
            String   v_ItemID;
            String   v_FA;
            String   v_FF;
            Integer  v_FS;
            String   v_Gbr;
            byte[]   vImg;
            Integer  ncount;
            Integer  nerr;
            byte[]   pCFileData;

            for(Seq=1; Seq<=3;Seq++){
                String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
                String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

                String OPERATION_NAME = "Get_List_Task_Image";
                String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

                SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
                Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

                // =============== p_task_id ===================
                PropertyInfo prop_p_task_did = new PropertyInfo();
                prop_p_task_did.setName("p_task_did");
                prop_p_task_did.setValue(TaskDID);
                prop_p_task_did.setType(String.class);
                // =============== p_pic_nik ===================
                PropertyInfo prop_p_seq = new PropertyInfo();
                prop_p_seq.setName("p_seq");
                prop_p_seq.setValue(Seq);
                prop_p_seq.setType(String.class);

                request.addProperty(prop_p_task_did);
                request.addProperty(prop_p_seq);

                Log.d("[GudangGaram]: ", "p_task_did      : " + TaskDID);
                Log.d("[GudangGaram]: ", "p_seq           : " + Seq);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);


                //Web method call
                try {
                    List<HeaderProperty> headers=new ArrayList<HeaderProperty>();
                    HeaderProperty headerProperty=new HeaderProperty("Accept-Encoding", "gzip,deflate");
                    headers.add(headerProperty);
                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,60000);
                    httpTransport.debug = true;
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    //httpTransport.call(SOAP_ACTION, envelope);
                    httpTransport.call(SOAP_ACTION, envelope,headers);

                    SoapObject obj, obj1, obj2, obj3;
                    obj     = (SoapObject) envelope.getResponse();
                    obj1    = (SoapObject) obj.getProperty("diffgram");
                    obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                    ncount = obj2.getPropertyCount();

                    nerr   = 0;
                    Log.d("[GudangGaram]", ">>> count  >>> " + ncount.toString());

                    ImageHelper ihx = new ImageHelper();


                    for (int i = 0; i < ncount ;i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);
                        Log.d("[GudangGaram]", ">>> obj3.count[" + i + "]  >>> " + obj3.getPropertyCount());

                        DS_TaskItemDetail vd = new DS_TaskItemDetail();
                        v_TaskDID    = obj3.getProperty(0).toString();
                        v_fileID     = obj3.getProperty(1).toString();
                        v_Seq        = Integer.parseInt(obj3.getProperty(2).toString());
                        v_ItemID     = obj3.getProperty(3).toString();
                        v_FS         = Integer.parseInt(obj3.getProperty(4).toString());
                        v_FA         = obj3.getProperty(5).toString();
                        v_FF         = obj3.getProperty(6).toString();

                        Log.d("[GudangGaram]", "----------------------------------------------------------");
                        Log.d("[GudangGaram]", "[ 0 ]>>> Task DID      >>> " + v_TaskDID);
                        Log.d("[GudangGaram]", "[ 1 ]>>> File ID       >>> " + v_fileID);
                        Log.d("[GudangGaram]", "[ 2 ]>>> Seq           >>> " + v_Seq);
                        Log.d("[GudangGaram]", "[ 3 ]>>> Item ID       >>> " + v_ItemID);
                        Log.d("[GudangGaram]", "[ 4 ]>>> FS            >>> " + v_FS);
                        Log.d("[GudangGaram]", "[ 5 ]>>> FAttach ID    >>> " + v_FA);
                        Log.d("[GudangGaram]", "[ 6 ]>>> FFile ID      >>> " + v_FF);
                        // image

                        try {
                            if (v_FS > 0) {
                                v_Gbr = obj3.getProperty(7).toString();
                                vImg = decode(v_Gbr);

                                if(vImg.length > ihx.getBitmapMaxSize()){
                                    pCFileData = ihx.compressImageByte(vImg);
                                }
                                else{
                                    pCFileData = vImg;
                                }

                                Log.d("[GudangGaram]", "[ 7 ]>>> Gbr         >>> " + v_Gbr);
                                Result = dbHelper.addItemPicture(v_TaskDID, v_Seq, pCFileData, v_fileID, v_ItemID, v_FA, v_FF);
                            }
                            else {
                                Log.d("[GudangGaram]", "[ 7 ]>>> Gbr Not Exist (Size = 0)");
                            }
                        }
                        catch (Exception e) {
                            Log.d("[GudangGaram]", "[ 7 ]>>> Gbr Exception >>> " + e.getMessage());
                        }
                    } // end loop
                }
                catch (Exception ex) {
                    Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
                    Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
                finally {
                    Log.d("[GudangGaram]", "SoapRetrieveTaskItemTaskAsync  :: end doInBackground");
                }

                if(Seq == 3){
                    ResultResponse = TaskDID.toString();
                }
                else{
                    ResultResponse = "-1";
                }

                publishProgress("Retrieve Image TaskDID = " + TaskDID.toString() + " [" + Integer.toString(Seq) + " of 3 ]");

                try {
                    Thread.sleep(500);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } // end loop

            return "";
        }
}
