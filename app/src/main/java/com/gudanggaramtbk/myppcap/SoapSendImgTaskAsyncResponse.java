package com.gudanggaramtbk.myppcap;

/**
 * Created by luckym on 8/29/2018.
 */

public interface SoapSendImgTaskAsyncResponse {
    void PostSentAction(String output);
}
