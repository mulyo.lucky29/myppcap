package com.gudanggaramtbk.myppcap;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by LuckyM on 8/18/2018.
 */

public class ImageHelper {
    private static final float MAX_HEIGHT         = 480.0f;
    private static final float MAX_WIDTH          = 640.0f;
    private static final float MIDDLE_DIVIDER     = 2.0f;
    private static final int HALF_DIVIDER         = 2;
    private static final int PIXEL_CAP_MULTIPLIER = 2;
    private static final int ORIENTATION_6        = 6;
    private static final int ORIENTATION_3        = 3;
    private static final int ORIENTATION_8        = 8;
    private static final int ORIENTATION_6_ROTATE = 90;
    private static final int ORIENTATION_3_ROTATE = 180;
    private static final int ORIENTATION_8_ROTATE = 270;
    private static final int COMPRESS_QUALITY     = 80;
    private static final int TEMP_STORAGE_SIZE    = 16 * 1024;
    private static final int BITMAP_MAX_SIZE      = 1536000; // 2048000; // 2 MB



    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    // The 3 states (events) which the user is trying to perform
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;
    // these PointF objects are used to record the point(s) the user is touching
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    private static final float MIN_ZOOM = 1f, MAX_ZOOM = 1f;

    public static int getBitmapMaxSize() {
        return BITMAP_MAX_SIZE;
    }

    public void ImageHelper() { }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * PIXEL_CAP_MULTIPLIER;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public static byte[] compressImageByte(byte[] pFileData) {
        int IMAGE_MAX_SIZE = 2048;
        int scale = 1;
        Bitmap b = null;
        Bitmap originalBitmap;
        Bitmap scaledBitmap;
        byte[] Rescalebyte = {0};

        FileInputStream fis = null;

        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        originalBitmap = BitmapFactory.decodeByteArray(pFileData, 0, pFileData.length, o);
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        scaledBitmap = BitmapFactory.decodeByteArray(pFileData, 0, pFileData.length, o2);

        ByteArrayOutputStream streamRescale = new ByteArrayOutputStream();
        try {
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, streamRescale);
            Rescalebyte = streamRescale.toByteArray();
            scaledBitmap.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Rescalebyte;
    }

    public byte[] getByteFromBitmap(Bitmap bmp){
        byte[]                result = {0};
        try{
            ByteArrayOutputStream baos;
            baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            result  =  baos.toByteArray();
        }
        catch(Exception e){
        }
        return result;
    }

    public byte[] getByteImageView(ImageView pimgv){
        Bitmap                bmp;
        ByteArrayOutputStream baos;
        ImageView             oImgV = pimgv;
        byte[]                result = {0};

        try{
            bmp = ((BitmapDrawable) oImgV.getDrawable()).getBitmap();
            baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            result  =  baos.toByteArray();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "getByteImgView Exception : " + e.getMessage());
        }
        return  result;
    }

    public void show_image_popup(Bitmap oBitmap, Context ctx){
        ImageView   imgv;
        Button      finishbtn;

        Log.d("[GudangGaram]", "ImageHelper :: show_image_popup begin");

        try{
            final Dialog myDialog = new Dialog(ctx);
            myDialog.setContentView(R.layout.imageview_dialog);
            myDialog.setTitle("View Image");

            imgv = (ImageView)myDialog.findViewById(R.id.imgv);
            imgv.setImageBitmap(oBitmap);
            imgv.setOnTouchListener(new View.OnTouchListener(){
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    ImageView view = (ImageView) v;
                    view.setScaleType(ImageView.ScaleType.MATRIX);
                    float scale;
                    dumpEvent(event);
                    // Handle touch events here...
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN: // first finger down only
                            savedMatrix.set(matrix);
                            start.set(event.getX(), event.getY());
                            Log.d("[GudangGaram]", "mode=DRAG"); // write to LogCat
                            mode = DRAG;
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_POINTER_UP:
                            mode = NONE;
                            Log.d("[GudangGaram]", "mode=NONE");
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            oldDist = spacing(event);
                            Log.d("[GudangGaram]", "oldDist=" + oldDist);
                            if (oldDist > 5f) {
                                savedMatrix.set(matrix);
                                midPoint(mid, event);
                                mode = ZOOM;
                                Log.d("[GudangGaram]", "mode=ZOOM");
                            }
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (mode == DRAG) {
                                matrix.set(savedMatrix);
                                matrix.postTranslate(event.getX() - start.x, event.getY()
                                        - start.y); /*
                                     * create the transformation in the matrix
                                     * of points
                                     */
                            } else if (mode == ZOOM) {
                                // pinch zooming
                                float newDist = spacing(event);
                                Log.d("[GudangGaram]", "newDist=" + newDist);
                                if (newDist > 5f) {
                                    matrix.set(savedMatrix);
                                    scale = newDist / oldDist;
                                /*
                                 * setting the scaling of the matrix...if scale > 1 means
                                 * zoom in...if scale < 1 means zoom out
                                 */
                                    matrix.postScale(scale, scale, mid.x, mid.y);
                                }
                            }
                            break;
                    }
                    view.setImageMatrix(matrix); // display the transformation on screen
                    return true;
                }
            });

            finishbtn=(Button)myDialog.findViewById(R.id.btnfinish);
            finishbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myDialog.cancel();
                }
            });
            myDialog.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ImageHelper :: show_image_popup Exception:" + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "ImageHelper :: show_image_popup End");
        }
    }

    private void dumpEvent(MotionEvent event) {
        String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
                "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN
                || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(
                    action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }
        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }
        sb.append("]");
        Log.d("Touch Event", sb.toString());
    }

    /** Determine the space between the first two fingers */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    /** Calculate the mid point of the first two fingers */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    public Bitmap putBitmapSignature(Bitmap src, String strType){
        Bitmap dest;
        SimpleDateFormat sdf        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestampsignature   = "PPCAP_" + strType + "_ " + sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
        File outputFile             = new File(Environment.getExternalStorageDirectory(), "T" + timestampsignature);

        if(src.getByteCount() > 0) {
            dest = src.copy(src.getConfig(), true);
            try
            {
                // open file pointer
                FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                // prepare for font timestamp signature
                Typeface tf = Typeface.create(Typeface.SERIF, Typeface.BOLD);
                // Initialize a new Paint instance to draw the text
                Paint tpaint = new Paint();
                tpaint.setStyle(Paint.Style.FILL);
                tpaint.setColor(Color.RED);
                tpaint.setAntiAlias(true);
                tpaint.setTextAlign(Paint.Align.RIGHT);
                tpaint.setTypeface(tf);
                tpaint.setTextSize(18);

                // initial canvas
                Canvas cvs = new Canvas(dest);
                cvs.drawBitmap(src, src.getWidth(), src.getHeight(), null);
                cvs.drawText(timestampsignature, src.getWidth() - timestampsignature.length() - 1, src.getHeight() - 20, tpaint);
                dest.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);

                fileOutputStream.flush();
                fileOutputStream.close();
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.d("[GudangGaram]", "putBitmapSignature :: File Not Found Exception " + e.getMessage());
            }
            catch (IOException e) {
                e.printStackTrace();
                Log.d("[GudangGaram]", "putBitmapSignature :: IO Exception " + e.getMessage());
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "putBitmapSignature :: Exception " + e.getMessage());
            }
            finally {
                outputFile.delete();
            }
        } // end if
        else{
            dest = null;
        } // end else

        return dest;
    }

    public Bitmap getImageFromUri(ImageView ivh, Uri sUri, Boolean UseSignature, String Prefix){
        byte[]    cbyte  = {0};
        byte[]    cpress = {0};
        Bitmap    cbmp, result;

        try{
            ivh.setImageURI(sUri);
            cbyte  = getByteImageView(ivh);
            // if size is big enough more than BITMAP_MAX_SIZE then do compress first
            if(cbyte.length > BITMAP_MAX_SIZE){
                cpress = compressImageByte(cbyte); ;
            }
            else{
                cpress = cbyte;
            }
            cbmp = BitmapFactory.decodeByteArray(cpress, 0, cpress.length);
        }
        catch(Exception e){
            cbmp = null;
        }

         // if use signature
         if(UseSignature){
            result = putBitmapSignature(cbmp, Prefix);
         }
         else{
            result = cbmp.copy(cbmp.getConfig(), true);
         }

        return result;
    }

    public Bitmap getImageFileFromSDCard(String filename, Boolean UseSignature, String Prefix){
        Bitmap tmp, cbmp, result;
        byte[] cbyte  = {0};
        byte[] cpress = {0};

        File imageFile      = new File(Environment.getExternalStorageDirectory(), filename);
        Log.d("[GudangGaram]", "getImageFileFromSDCard " + Environment.getExternalStorageDirectory().toString() + "/" + filename);

        try {
            FileInputStream fis = new FileInputStream(imageFile);
            tmp = BitmapFactory.decodeStream(fis);
            Log.d("[GudangGaram]", "getImageFileFromSDCard :: Read Bitmaps Original Captured Size :" + tmp.getByteCount());

            ByteArrayOutputStream streamtmp = new ByteArrayOutputStream();
            tmp.compress(Bitmap.CompressFormat.JPEG, 100, streamtmp);
            cbyte = streamtmp.toByteArray();

            // if size is big enough more than BITMAP_MAX_SIZE then do compress first
            if(cbyte.length > BITMAP_MAX_SIZE ){
                cpress = compressImageByte(cbyte);
            }
            else{
                cpress = cbyte;
            }
            cbmp = BitmapFactory.decodeByteArray(cpress, 0, cpress.length);

            Log.d("[GudangGaram]", "getImageFileFromSDCard :: Read Bitmaps Captured Size :" + cbmp.getByteCount());
        }
        catch (FileNotFoundException e) {
            cbmp = null;
            e.printStackTrace();
        }

        // if use signature
        if(UseSignature){
            result = putBitmapSignature(cbmp, Prefix);
        }
        else{
            result = cbmp.copy(cbmp.getConfig(), true);
        }

        Log.d("[GudangGaram]", "getImageFileFromSDCard :: Read Bitmaps Captured Edit Size :" + result.getByteCount());

        return result;
    }

    /*
    public static String getFileNameByUri(Context context, Uri uri)
    {
        String fileName="unknown";//default fileName
        Uri filePathUri = uri;
        if (uri.getScheme().toString().compareTo("content")==0)
        {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor.moveToFirst())
            {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
                filePathUri = Uri.parse(cursor.getString(column_index));
                fileName = filePathUri.getLastPathSegment().toString();
            }
        }
        else if (uri.getScheme().compareTo("file")==0)
        {
            fileName = filePathUri.getLastPathSegment().toString();
        }
        else
        {
            fileName = fileName+"_"+filePathUri.getLastPathSegment();
        }
        return fileName;
    }
    */

    /*
        public static String getFilename() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/NeedMobil/Images");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        String mImageName = "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        return mediaStorageDir.getAbsolutePath() + "/" + mImageName;
    }
     */

    /**
     * Reduces the size of an image without affecting its quality.
     * @param imagePath -Path of an image
     * @return String - filepath of resize image
     */
    /*
    public static String compressImage(String imagePath) {
        //bitmap = BitmapFactory.decodeStream(fis);

        Bitmap scaledBitmap           = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = MAX_WIDTH / MAX_HEIGHT;

        if (actualHeight > MAX_HEIGHT || actualWidth > MAX_WIDTH) {
            if (imgRatio < maxRatio) {
                imgRatio = MAX_HEIGHT / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) MAX_HEIGHT;
            } else if (imgRatio > maxRatio) {
                imgRatio = MAX_WIDTH / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) MAX_WIDTH;
            } else {
                actualHeight = (int) MAX_HEIGHT;
                actualWidth = (int) MAX_WIDTH;
            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[TEMP_STORAGE_SIZE];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;


        float middleX = actualWidth / MIDDLE_DIVIDER;
        float middleY = actualHeight / MIDDLE_DIVIDER;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);

        canvas.drawBitmap(bmp,
                middleX - bmp.getWidth() / HALF_DIVIDER,
                middleY - bmp.getHeight() / HALF_DIVIDER,
                new Paint(Paint.FILTER_BITMAP_FLAG));

        if (bmp != null) {
            bmp.recycle();
        }

        ExifInterface exif;

        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();

            if (orientation == ORIENTATION_6) {
                matrix.postRotate(ORIENTATION_6_ROTATE);
            } else if (orientation == ORIENTATION_3) {
                matrix.postRotate(ORIENTATION_3_ROTATE);
            } else if (orientation == ORIENTATION_8) {
                matrix.postRotate(ORIENTATION_8_ROTATE);
            }

            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(),
                    scaledBitmap.getHeight(), matrix, true);

        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filepath = getFilename();
        try {
            out = new FileOutputStream(filepath);
            //write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, COMPRESS_QUALITY, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filepath;
    }
    */

}
