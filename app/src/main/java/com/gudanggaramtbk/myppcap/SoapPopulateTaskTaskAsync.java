package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 3/6/2018.
 */

public class SoapPopulateTaskTaskAsync extends AsyncTask<String, Void, String>  {
    private SharedPreferences         config;
    private Context                   context;
    private ProgressDialog            pd;
    private MySQLiteHelper            dbHelper;
    private String                    SentResponse;
    private String                    picNIK;
    private ListView                  populateTaskList;
    private List<DS_TaskItem>         Result;
    private ArrayAdapter<DS_TaskItem> trxadapter;

    public SoapPopulateTaskTaskAsync(){
    }

    public interface SoapRetrieveTaskTaskAsyncResponse {
        void PostSentAction(List<DS_TaskItem> output);
    }

    public SoapPopulateTaskTaskAsync.SoapRetrieveTaskTaskAsyncResponse delegate = null;
    public SoapPopulateTaskTaskAsync(SoapPopulateTaskTaskAsync.SoapRetrieveTaskTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String pPicNIK,
                             ListView opopulateTaskList){

        Log.d("[GudangGaram]", " SoapRetrieveTaskItemTaskAsync :: setAttribute");
        this.context          = context;
        this.dbHelper         = dbHelper;
        this.config           = config;
        this.picNIK           = pPicNIK;
        this.populateTaskList = opopulateTaskList;
        this.pd = new ProgressDialog(this.context);

        Result = new ArrayList<DS_TaskItem>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", " SoapRetrieveTaskItemTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve Task Item Data From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    protected  void onProgressUpdate(String[] args) {
        //progressDialog.setMessage(args[0]);
    }

    @Override
    protected void onPostExecute(String file_url) {
        pd.dismiss();
        try {
            populateTaskList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            trxadapter = new CustomListAdapterTask(context, Result, populateTaskList);
            trxadapter.notifyDataSetChanged();
            populateTaskList.setAdapter(trxadapter);
            long xcount = populateTaskList.getCount();
            Log.d("[GudangGaram]:", "SoapPopulateTaskTaskAsync OnPostExecute xcount " + xcount);
            if (xcount == 0) {
                Toast.makeText(context, "No Data Result", Toast.LENGTH_LONG).show();
            }
            delegate.PostSentAction(Result);
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "SoapPopulateTaskTaskAsync OnPostExecute Exception " + e.getMessage());
        }
        Toast.makeText(context,"Retrieve Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        String v_task_id;
        String v_file_id;
        String v_file_name;
        String v_task_date;
        String ResultResponse = "-";


        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Get_List_Task";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_pic_nik ===================
        PropertyInfo prop_p_pic_nik = new PropertyInfo();
        prop_p_pic_nik.setName("p_pic_nik");
        prop_p_pic_nik.setValue(picNIK);
        prop_p_pic_nik.setType(String.class);
        request.addProperty(prop_p_pic_nik);

        Log.d("[GudangGaram]: ", "p_pic_nik      : " + picNIK);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);



        //Web method call
        try {
            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));
                Result.clear();

                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);

                        DS_TaskItem vh = new DS_TaskItem();
                        v_task_id = obj3.getProperty(0).toString();
                        v_file_id = obj3.getProperty(1).toString();
                        v_file_name = obj3.getProperty(2).toString();
                        v_task_date = obj3.getProperty(3).toString();

                        Log.d("[GudangGaram]", "Task ID   : " + v_task_id);
                        Log.d("[GudangGaram]", "File ID   : " + v_file_id);
                        Log.d("[GudangGaram]", "File Name : " + v_file_name);
                        Log.d("[GudangGaram]", "Task Date : " + v_task_date);

                        vh.setChecked(false);
                        vh.setTaskID(Integer.parseInt(v_task_id));
                        vh.setFileID(Integer.parseInt(v_file_id));
                        vh.setFileName(v_file_name);
                        vh.setTaskDate(v_task_date);

                        Result.add(vh);
                    }
                } else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveTaskItemTaskAsync  :: end doInBackground");
        }

        return "";
    }

}
