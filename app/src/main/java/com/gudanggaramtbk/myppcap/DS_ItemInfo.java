package com.gudanggaramtbk.myppcap;

/**
 * Created by luckym on 8/28/2018.
 */

public class DS_ItemInfo {
    private Integer ItemID;
    private String  ItemCode;
    private String  ItemDesc;
    private String  ItemBrand;
    private String  ItemSpec;
    private Integer FS_Img1;
    private Integer FS_Img2;
    private Integer FS_Img3;
    private Integer FA_Img1;
    private Integer FA_Img2;
    private Integer FA_Img3;
    private Integer FF_Img1;
    private Integer FF_Img2;
    private Integer FF_Img3;



    // ================== getter ========================
    public Integer getItemID()   { return ItemID; }
    public String getItemCode()  { return ItemCode; }
    public String getItemDesc()  { return ItemDesc; }
    public String getItemBrand() { return ItemBrand; }
    public String getItemSpec()  { return ItemSpec; }
    public Integer getFS_Img1()  { return FS_Img1; }
    public Integer getFS_Img2()  { return FS_Img2; }
    public Integer getFS_Img3()  { return FS_Img3; }
    public Integer getFA_Img1()  { return FA_Img1; }
    public Integer getFA_Img2()  { return FA_Img2; }
    public Integer getFA_Img3()  { return FA_Img3; }
    public Integer getFF_Img1()  { return FF_Img1; }
    public Integer getFF_Img2()  { return FF_Img2; }
    public Integer getFF_Img3()  { return FF_Img3; }


    // ================== setter ========================
    public void setItemID(Integer itemID)    { ItemID = itemID; }
    public void setItemCode(String itemCode) { ItemCode = itemCode; }
    public void setItemDesc(String itemDesc) { ItemDesc = itemDesc; }
    public void setItemBrand(String itemBrand) { ItemBrand = itemBrand; }
    public void setItemSpec(String itemSpec) { ItemSpec = itemSpec; }
    public void setFS_Img1(Integer FS_Img1)  { this.FS_Img1 = FS_Img1; }
    public void setFS_Img2(Integer FS_Img2)  { this.FS_Img2 = FS_Img2; }
    public void setFS_Img3(Integer FS_Img3)  { this.FS_Img3 = FS_Img3; }
    public void setFA_Img1(Integer FA_Img1)  { this.FA_Img1 = FA_Img1; }
    public void setFA_Img2(Integer FA_Img2)  { this.FA_Img2 = FA_Img2; }
    public void setFA_Img3(Integer FA_Img3)  { this.FA_Img3 = FA_Img3; }
    public void setFF_Img1(Integer FF_Img1)  { this.FF_Img1 = FF_Img1; }
    public void setFF_Img2(Integer FF_Img2)  { this.FF_Img2 = FF_Img2; }
    public void setFF_Img3(Integer FF_Img3)  { this.FF_Img3 = FF_Img3; }

}
