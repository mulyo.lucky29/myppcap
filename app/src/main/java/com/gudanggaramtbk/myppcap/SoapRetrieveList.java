package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.InputMismatchException;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * Created by LuckyM on 6/11/2018.
 */

public class SoapRetrieveList {
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper    dbHelper;
    private LoadTask          pActivity;
    private ListView          listTaskMI;
    private ProgressDialog    pdx;

    // override constructor
    public SoapRetrieveList(SharedPreferences PConfig, ListView lst){
        Log.d("[GudangGaram]", "SoapRetrieveList Constructor");
        this.config           = PConfig;
        this.listTaskMI       = lst;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SetContext");
        this.context = context;
    }
    public void setParentActivity(LoadTask pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Retrieve(final String  xtaskID,
                         final String  xPICNIK,
                         final String  xDeviceID) {
        final Integer  imagecounter, ctr;

        Log.d("[GudangGaram]", "Retrieve List Begin");
        try {
            // call WS To Retrieve task List from oracle and save it to local database table
            SoapRetrieveListTaskAsync SoapRequestT = new SoapRetrieveListTaskAsync(new SoapRetrieveListTaskAsync.SoapRetrieveListTaskAsyncResponse() {
                @Override
                public void PostSentAction(String RTaskID) {
                    Log.d("[GudangGaram]", "PostSentAction SoapRetrieveListTaskAsync");
                    Integer xpSeq = -1;
                    try {
                        if(RTaskID.equals("*") == true){
                        }
                        else {
                            try{
                                //==================== summon WS To Flag Task Downloaded ==============================
                                Log.d("[GudangGaram]:", "Update Flag Task With Task ID = " + RTaskID + " BEGIN");
                                SoapSetFlagTaskTaskAsync SoapRequestF = new SoapSetFlagTaskTaskAsync(new SoapSetFlagTaskTaskAsync.SoapSetFlagTaskTaskAsyncResponse() {
                                    @Override
                                    public void PostSentAction(String output) {
                                        List<DS_TaskItemDetail> dimage;
                                        String xpTaskDID = "";
                                        Log.d("[GudangGaram]", "PostSentAction SoapSetFlagTaskTaskAsync BEGIN");

                                        // retrieve all image
                                        dimage = dbHelper.get_RouteInfoBy(xPICNIK, "TaskID", xtaskID, null);
                                        if(dimage.size() > 0) {
                                            for (int ix=0; ix<dimage.size(); ix++) {
                                                xpTaskDID = dimage.get(ix).getTaskDID().toString();
                                                try{
                                                    //==================== summon WS To Retrieve Image based on pTaskDID  ==============================
                                                    Log.d("[GudangGaram]:", "Retrieve Image For pTaskDID = " + xpTaskDID + " BEGIN");
                                                    SoapRetrieveImgTaskAsync SoapRequestI = new SoapRetrieveImgTaskAsync(new SoapRetrieveImgTaskAsync.SoapRetrieveImgTaskAsyncResponse() {
                                                        @Override
                                                        public void PostSentAction(String RTaskID){
                                                            Log.d("[GudangGaram]", "PostSentAction SoapRetrieveImgTaskAsync ");
                                                        }
                                                    });
                                                    //SoapRequest.setAttribute(context, dbHelper, config,pTaskDID, pSeq);
                                                    SoapRequestI.setAttribute(context, dbHelper, config,xpTaskDID);
                                                    if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                                                        //work on sgs3 android 4.0.4
                                                        //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                                                        SoapRequestI.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                                                    }
                                                    else {
                                                        SoapRequestI.execute(); // work on sgs2 android 2.3
                                                    }
                                                }
                                                catch(Exception e){
                                                    Log.d("[GudangGaram]:", "Retrieve Image For pTaskDID = " + xpTaskDID + " Exception " + e.getMessage().toString());
                                                }
                                                finally {
                                                    Log.d("[GudangGaram]:", "Retrieve Image For pTaskDID = " + xpTaskDID + " END");
                                                }
                                            } // end for
                                        } // end if
                                        Log.d("[GudangGaram]", "PostSentAction SoapSetFlagTaskTaskAsync END");
                                    }
                                });

                                SoapRequestF.setAttribute(context, dbHelper, config,listTaskMI, RTaskID,xPICNIK,xDeviceID);
                                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                                    //work on sgs3 android 4.0.4
                                    //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                                    SoapRequestF.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                                }
                                else {
                                    SoapRequestF.execute(); // work on sgs2 android 2.3
                                }
                            }
                            catch(Exception e){
                                Log.d("[GudangGaram]:", "Update Flag Task With Task ID = " + RTaskID + " Exception : " + e.getMessage().toString());
                            }
                            finally {
                                Log.d("[GudangGaram]:", "Update Flag Task With Task ID = " + RTaskID + " END ");
                            }

                        }
                    }
                    catch (Exception e) {
                        Log.d("[GudangGaram]", "RetrieveTask Exception " + e.getMessage().toString());
                    }
                    finally {
                        Log.d("[GudangGaram]", "RetrieveTask End");
                    }
                }
            });

            SoapRequestT.setAttribute(context, dbHelper, config, xtaskID, xPICNIK);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequestT.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequestT.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]", "Retrieve List End");
        }
    }

}
