package com.gudanggaramtbk.myppcap;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.media.Image;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.kobjects.base64.Base64.decode;
import com.gudanggaramtbk.myppcap.ImageHelper;

/**
 * Created by LuckyM on 3/6/2018.
 */

public class MySQLiteHelper extends SQLiteOpenHelper  {
    private static final int DATABASE_VERSION         = 1;
    private static final String DATABASE_NAME         = "MyPPCap.db";

    public static String getTableTtTask() { return TABLE_TT_TASK; }
    public static String getTableTpPicture() { return TABLE_TP_PICTURE; }
    public static String getTableMPic() { return TABLE_M_PIC; }
    public static String getTableMLookup() { return TABLE_M_LOOKUP; }
    public static String getViewLogin() { return VIEW_LOGIN; }
    public static String getViewTaskHeader() { return VIEW_TASK_HEADER; }

    private static final String TABLE_TT_TASK         = "GGGG_PPCAP_TT_TASK";
    private static final String TABLE_TP_PICTURE      = "GGGG_PPCAP_TP_PICTURE";
    private static final String TABLE_M_PIC           = "GGGG_PPCAP_M_PIC";
    private static final String TABLE_M_LOOKUP        = "GGGG_PPCAP_M_LOOKUP";
    private static final String VIEW_LOGIN            = "GGGG_PPCAP_V_LOGIN";
    private static final String VIEW_TASK_HEADER      = "GGGG_PPCAP_TTH_TASK_V";

    // =========== Master PIC ================================
    private static final String KEY_M_PIC_PERSON_ID         = "Person_ID";
    private static final String KEY_M_PIC_NIK               = "PIC_NIK";
    private static final String KEY_M_PIC_NAME              = "PIC_Name";

    // =========== Master Lookup =============================
    private static final String KEY_M_LOOKUP_CONTEXT        = "LContext";
    private static final String KEY_M_LOOKUP_VALUE          = "LValue";
    private static final String KEY_M_LOOKUP_MEANNG         = "LMeaning";


    // =========== task table item  ==========================
    private static final String KEY_TT_ID                   = "TT_ID";
    private static final String KEY_TT_TASK_DID             = "TT_TaskDID";
    private static final String KEY_TT_TASK_ID              = "TT_TaskID";
    private static final String KEY_TT_FILE_ID              = "TT_FileID";
    private static final String KEY_TT_SEQ                  = "TT_Seq";
    private static final String KEY_TT_ITEM_ID              = "TT_ItemID";
    private static final String KEY_TT_ITEM_CODE            = "TT_ItemCode";
    private static final String KEY_TT_ITEM_DESC            = "TT_ItemDesc";
    private static final String KEY_TT_LOC_RAK              = "TT_LocRak";
    private static final String KEY_TT_LOC_BIN              = "TT_LocBin";
    private static final String KEY_TT_LOC_COM              = "TT_LocCom";
    private static final String KEY_TT_RAK_SEQ              = "TT_LocRakSeq";
    private static final String KEY_TT_BIN_SEQ              = "TT_LockBinSeq";
    private static final String KEY_TT_PIC_NIK              = "TT_PIC_NIK";
    private static final String KEY_TT_PIC_PERSON_ID        = "TT_PIC_PersonID";
    private static final String KEY_TT_PIC_NAME             = "TT_PIC_Name";
    private static final String KEY_TT_GBR1                 = "TT_Img1";
    private static final String KEY_TT_GBR2                 = "TT_Img2";
    private static final String KEY_TT_GBR3                 = "TT_Img3";
    private static final String KEY_TT_REASON               = "TT_Reason";
    private static final String KEY_TT_CLOSED               = "TT_Closed";
    private static final String KEY_TT_CREATED_DATE         = "TT_Created_Date";

    // ========== master table picture =========================
    private static final String KEY_TP_ID                   = "TP_ID";
    private static final String KEY_TP_TASK_DID             = "TP_TaskDID";
    private static final String KEY_TP_FILE_ID              = "TP_FileID";
    private static final String KEY_TP_ITEM_ID              = "TP_ItemID";
    private static final String KEY_TP_ATTACHMENT_ID        = "TP_AttachmentID";
    private static final String KEY_TP_ATTACHMENT_NO        = "TP_AttachmentNo";
    private static final String KEY_TP_LOB_ID               = "TP_LobID";
    private static final String KEY_TP_FILE_DATA            = "TP_FileData";
    private static final String KEY_TP_CREATED_DATE         = "TP_Created_Date";


    private static final float MAX_HEIGHT         = 480.0f;
    private static final float MAX_WIDTH          = 640.0f;
    private static final float MIDDLE_DIVIDER     = 2.0f;
    private static final int HALF_DIVIDER         = 2;
    private static final int PIXEL_CAP_MULTIPLIER = 2;
    private static final int ORIENTATION_6        = 6;
    private static final int ORIENTATION_3        = 3;
    private static final int ORIENTATION_8        = 8;
    private static final int ORIENTATION_6_ROTATE = 90;
    private static final int ORIENTATION_3_ROTATE = 180;
    private static final int ORIENTATION_8_ROTATE = 270;
    private static final int COMPRESS_QUALITY     = 80;
    private static final int TEMP_STORAGE_SIZE    = 16 * 1024;

    // =============================================================
    private static Cursor XMICursor;
    private static Cursor XRouteCursor;
    public String Get_DatabaseName(){
        return DATABASE_NAME;
    }

    private static final String[] TT_COLUMNS = {
            KEY_TT_TASK_DID,
            KEY_TT_TASK_ID,
            KEY_TT_FILE_ID,
            KEY_TT_SEQ,
            KEY_TT_ITEM_ID,
            KEY_TT_ITEM_CODE,
            KEY_TT_ITEM_DESC,
            KEY_TT_LOC_RAK,
            KEY_TT_LOC_BIN,
            KEY_TT_LOC_COM,
            KEY_TT_RAK_SEQ,
            KEY_TT_BIN_SEQ,
            KEY_TT_PIC_NIK,
            KEY_TT_PIC_PERSON_ID,
            KEY_TT_PIC_NAME,
            KEY_TT_CREATED_DATE
    };

    private static final String[] TP_COLUMS = {
            KEY_TP_ID,
            KEY_TP_TASK_DID,
            KEY_TP_FILE_ID,
            KEY_TP_ITEM_ID,
            KEY_TP_ATTACHMENT_ID,
            KEY_TP_ATTACHMENT_NO,
            KEY_TP_LOB_ID,
            KEY_TP_FILE_DATA,
            KEY_TP_CREATED_DATE
    };


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    private static final String CREATE_LOGIN_VW = " CREATE VIEW IF NOT EXISTS " + VIEW_LOGIN + " " +
                                                  "  AS " +
                                                  "  SELECT PersonID, NIK, Name " +
                                                  "  FROM ( " +
                                                    " SELECT " + KEY_M_PIC_PERSON_ID + " as PersonID," +
                                                           " " + KEY_M_PIC_NIK  + " as NIK," +
                                                           " " + KEY_M_PIC_NAME + " as Name " +
                                                    " FROM " + TABLE_M_PIC + " UNION ALL " +
                                                    " SELECT -1 as PersonID, '999999999' as NIK, 'Default' as Name); ";

     private static final String DROP_LOGIN_VW = " DROP VIEW IF EXISTS " + VIEW_LOGIN  + ";";
     private static final String DROP_TTH_VW   = " DROP VIEW IF EXISTS " + VIEW_TASK_HEADER + ";";


    private static final String CREATE_TTH_VW = " CREATE VIEW IF NOT EXISTS " + VIEW_TASK_HEADER + " "  +
                                                   " AS " +
                                                   " SELECT " + KEY_TT_TASK_ID + " , "
                                                              + KEY_TT_FILE_ID + " , "
                                                              + KEY_TT_PIC_NIK + " , "
                                                              + " SUM((CASE WHEN COALESCE(" + KEY_TT_CLOSED + ",'N') = 'Y' THEN 1 ELSE 0 END)) TT_CloseCount, "
                                                              + " COUNT(*) TT_Total " +
                                                   " FROM " + TABLE_TT_TASK +
                                                   " GROUP BY " + KEY_TT_TASK_ID + " , " + KEY_TT_FILE_ID + " , " + KEY_TT_PIC_NIK + " " +
                                                   " ORDER BY " + KEY_TT_TASK_ID + ";";


     private static final String CREATE_MPIC_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_PIC + " ( " +
                        KEY_M_PIC_PERSON_ID + " INTEGER PRIMARY KEY, " +
                        KEY_M_PIC_NIK       + " TEXT, " +
                        KEY_M_PIC_NAME      + " TEXT); ";


     private static  final String CREATE_MLOOKUP_TABLE = " CREATE TABLE IF NOT EXISTS " + TABLE_M_LOOKUP + " ( " +
             KEY_M_LOOKUP_CONTEXT + " TEXT, " +
             KEY_M_LOOKUP_VALUE   + " TEXT, " +
             KEY_M_LOOKUP_MEANNG  + " TEXT); ";

    private static final String CREATE_TT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TT_TASK + " ( " +
            KEY_TT_TASK_DID             + " INTEGER PRIMARY KEY, " +
            KEY_TT_TASK_ID              + " INTEGER, " +
            KEY_TT_FILE_ID              + " INTEGER, " +
            KEY_TT_SEQ                  + " INTEGER, " +
            KEY_TT_ITEM_ID              + " INTEGER, " +
            KEY_TT_ITEM_CODE            + " TEXT, " +
            KEY_TT_ITEM_DESC            + " TEXT, " +
            KEY_TT_LOC_RAK              + " TEXT, " +
            KEY_TT_LOC_BIN              + " TEXT, " +
            KEY_TT_LOC_COM              + " TEXT, " +
            KEY_TT_RAK_SEQ              + " INTEGER, " +
            KEY_TT_BIN_SEQ              + " INTEGER, " +
            KEY_TT_PIC_NIK              + " TEXT, " +
            KEY_TT_PIC_PERSON_ID        + " INTEGER, " +
            KEY_TT_PIC_NAME             + " TEXT, " +
            KEY_TT_GBR1                 + " BLOB, " +
            KEY_TT_GBR2                 + " BLOB, " +
            KEY_TT_GBR3                 + " BLOB, " +
            KEY_TT_REASON               + " TEXT, " +
            KEY_TT_CLOSED               + " TEXT, " +
            KEY_TT_CREATED_DATE         + " TEXT); ";

    public static final String CREATE_TP_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TP_PICTURE + " ( " +
            KEY_TP_ID                   + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_TP_TASK_DID             + " INTEGER, " +
            KEY_TP_FILE_ID              + " INTEGER, " +
            KEY_TP_ITEM_ID              + " INTEGER," +
            KEY_TP_ATTACHMENT_ID        + " INTEGER, " +
            KEY_TP_ATTACHMENT_NO        + " INTEGER, " +
            KEY_TP_LOB_ID               + " INTEGER, " +
            KEY_TP_FILE_DATA            + " BLOB, " +
            KEY_TP_CREATED_DATE         + " TEXT, " +
            " UNIQUE (" + KEY_TP_TASK_DID + "," + KEY_TP_ATTACHMENT_NO + ")); ";

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("[GudangGaram]", "OnCreate MySQLiteHelper");
        db.execSQL(CREATE_MPIC_TABLE);
        db.execSQL(CREATE_MLOOKUP_TABLE);
        db.execSQL(CREATE_TT_TABLE);
        db.execSQL(CREATE_TP_TABLE);
        db.execSQL(CREATE_LOGIN_VW);
        db.execSQL(CREATE_TTH_VW);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older master table if existed
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_ITEM);
        //onCreate(db);
    }

    public void flushAll(){
        //execute(DROP_LOGIN_VW);
        //execute(DROP_TTH_VW);

        flushTable(TABLE_M_PIC);
        flushTable(TABLE_M_LOOKUP);
        flushTable(TABLE_TT_TASK);
        flushTable(TABLE_TP_PICTURE);
    }

    // ================================= db operation util =========================================
    public void flushTable(String pTableName){
        Log.d("[GudangGaram]", "Flush Table : " + pTableName);
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete(pTableName, null, null);
        //db.close();
    }

    public void execute(String pquery){
        Log.d("[GudangGaram]", "Execute : " + pquery);
        SQLiteDatabase db= this.getWritableDatabase();
        db.execSQL(pquery);
        //db.close();
    }

    public boolean isNetworkConnected(SharedPreferences config, Context ctx)
    {
        String xfactor;
        String Message;
        Message = "Checking Connection ... Please Wait ...";

        xfactor = "S";
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        String SSoapAddress = config.getString("SoapAddress", "");
        String SSoapNamespace = config.getString("SoapNamespace", "");
        String SSoapMethodTest = "Get_Test_Result";

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SSoapAddress);
            httpTransport.debug = true;
            httpTransport.call(SSoapNamespace + SSoapMethodTest, envelope);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }
        catch(Exception ex){
            xfactor = ex.getMessage().toString();
            Toast.makeText(ctx, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
        }
        if(xfactor == "S"){
            Toast.makeText(ctx, "Test Connection Succeed ", Toast.LENGTH_LONG).show();
            return true;
        }
        else{
            return false;
        }
    }

    public int getRecordCount(String pTableName, String pWhere) {
        SQLiteDatabase db =  this.getReadableDatabase();
        try {
            String count = "SELECT count(*) FROM " + pTableName + " " + pWhere;
            Cursor mcursor = db.rawQuery(count, null);
            mcursor.moveToFirst();
            int icount = mcursor.getInt(0);
            if(icount>0){
                return icount;
            }
            else{
                return 0;
            }
        }
        catch(SQLiteException e) {
            e.printStackTrace();
            return 0;
        }
        finally {
            //db.close();
        }
    }


    public long addLookup(
            String pContext,
            String pValue,
            String pMeaning){
        long savestatus;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_M_LOOKUP_CONTEXT          , pContext);
        values.put(KEY_M_LOOKUP_VALUE            , pValue);
        values.put(KEY_M_LOOKUP_MEANNG           , pMeaning);

        savestatus = db.insert(TABLE_M_LOOKUP,null,values); // key/value -> keys = column names/ values = column values
        //db.close();
        return savestatus;
    }


    public long addPIC(
            String pPersonID,
            String pNIK,
            String pName){
        long savestatus;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        try{
            values.put(KEY_M_PIC_PERSON_ID      , Integer.parseInt(pPersonID));
            values.put(KEY_M_PIC_NIK            , pNIK);
            values.put(KEY_M_PIC_NAME           , pName);
            savestatus = db.insert(TABLE_M_PIC,null,values); // key/value -> keys = column names/ values = column values
        }
        catch(Exception e){
            savestatus = -1;
        }
        finally {
            //db.close();
        }

        return savestatus;
    }

    public long addItem(
            String   pTaskDID,
            String   pTaskID,
            String   pFileID,
            String   pSeq,
            String   pItemID,
            String   pItemCode,
            String   pItemDesc,
            String   pLocRak,
            String   pLocBin,
            String   pLocCom,
            String   pRakSeq,
            String   pBinSeq,
            String   pPicNik,
            String   pPICPersonID,
            String   pPICName,
            Integer  pFS1,
            Integer  pFS2,
            Integer  pFS3,
            String   pFA1,
            String   pFA2,
            String   pFA3,
            String   pFF1,
            String   pFF2,
            String   pFF3
    )
    {
        // ====================================================================================================
        // this function is purpose to Download Item task after user click Sync in Form Sync
        // ====================================================================================================

        long savestatus;
        String pClosed = "N";
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        try{
            values.put(KEY_TT_TASK_DID          , Integer.parseInt(pTaskDID));
            values.put(KEY_TT_TASK_ID           , Integer.parseInt(pTaskID));
            values.put(KEY_TT_FILE_ID           , Integer.parseInt(pFileID));
            values.put(KEY_TT_SEQ               , Integer.parseInt(pSeq));
            values.put(KEY_TT_ITEM_ID           , Integer.parseInt(pItemID));
            values.put(KEY_TT_ITEM_CODE         , pItemCode);
            values.put(KEY_TT_ITEM_DESC         , pItemDesc);
            values.put(KEY_TT_LOC_RAK           , pLocRak);
            values.put(KEY_TT_LOC_BIN           , pLocBin);
            values.put(KEY_TT_LOC_COM           , pLocCom);
            values.put(KEY_TT_RAK_SEQ           , Integer.parseInt(pRakSeq));
            values.put(KEY_TT_BIN_SEQ           , Integer.parseInt(pBinSeq));
            values.put(KEY_TT_PIC_NIK           , pPicNik);
            values.put(KEY_TT_PIC_PERSON_ID     , Integer.parseInt(pPICPersonID));
            values.put(KEY_TT_PIC_NAME          , pPICName);
            values.put(KEY_TT_CLOSED            , pClosed);
            values.put(KEY_TT_CREATED_DATE      , sysdate);
            savestatus = db.insert(TABLE_TT_TASK,null,values); // key/value -> keys = column names/ values = column values
        }
        catch(Exception e){
            savestatus = -1;
        }
        finally {

        }
        return savestatus;
    }

    // ============================================================== PICTURE ====================================================
    public byte[] getItemPicture(String pTaskDID, Integer pSeq){
        String kueri;
        byte[] result = {0};
        SQLiteDatabase db =  this.getReadableDatabase();

        kueri = " SELECT TP_FileData " +
                " FROM GGGG_PPCAP_TP_PICTURE " +
                " WHERE TP_TaskDID = " + pTaskDID + " " +
                "   AND TP_AttachmentNo = " + pSeq + ";";

        try{
            XMICursor = db.rawQuery(kueri, null);
            if(XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    result  = XMICursor.getBlob(0);
                    XMICursor.moveToNext();
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "getItemPicture Exception : " + e.getMessage().toString());
        }
        finally {
            //db.close();
        }

        Log.d("[GudangGaram]", "getItemPicture Result : "  + result + " Query : " + kueri);
        return result;

    }

    public Integer countItemPicture(String pTaskDID, Integer pSeq){
        String kueri;
        Integer result = 0;
        SQLiteDatabase db =  this.getReadableDatabase();

            kueri = " SELECT count(*) " +
                    " FROM GGGG_PPCAP_TP_PICTURE " +
                    " WHERE TP_TaskDID = " + pTaskDID + " " +
                    "   AND TP_AttachmentNo = " + pSeq + ";";

        try{
                XMICursor = db.rawQuery(kueri, null);
                if(XMICursor.getCount() > 0) {
                    XMICursor.moveToFirst();
                    while (!XMICursor.isAfterLast())
                    {
                        result  = XMICursor.getInt(0);
                        XMICursor.moveToNext();
                    }
                }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "countItemPicture Exception : " + e.getMessage().toString());
        }
        finally {
            //db.close();
        }

        Log.d("[GudangGaram]", "countItemPicture Result : "  + result + " Query : " + kueri);
        return result;
    }

    public Integer countItemP(String pTaskDID){
        Integer result = 0;
        SQLiteDatabase db =  this.getReadableDatabase();
        String kueri = " SELECT count(*) " +
                " FROM GGGG_PPCAP_TP_PICTURE " +
                " WHERE TP_TaskDID = " + pTaskDID + ";";

        try{
            XMICursor = db.rawQuery(kueri, null);
            if(XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    result  = XMICursor.getInt(0);
                    XMICursor.moveToNext();
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "countItemP Execption  : "  + e.getMessage());
        }
        finally {
            //db.close();
        }
        return result;
    }

    public byte[] loadItemPicture(String pTaskDID, Integer pSeq){
            Integer xcount,xrcount,xbcount = 0;
            Integer bsize  = 0;
            Integer blocksize = 50000;
            Integer elapse = 0;
            byte[] result = new byte[0];
            String kueri_big = "";

            SQLiteDatabase db =  this.getReadableDatabase();

            String kueri = " SELECT Length(TP_FileData) " +
                           " FROM GGGG_PPCAP_TP_PICTURE " +
                           " WHERE TP_TaskDID = " + pTaskDID + " " +
                           " AND TP_AttachmentNo = " + pSeq + " ";

            Cursor XMACursor = db.rawQuery(kueri, null);
            xcount = XMACursor.getCount();
            try {
                if (XMACursor != null && XMACursor.moveToFirst()) {
                    if(xcount > 0) {
                        while (!XMACursor.isAfterLast()) {
                            bsize = XMACursor.getInt(0);
                            Log.d("[GudangGaram]", "Load Image PtaskDID = " + pTaskDID + " Seq =" + pSeq + " Size = " + bsize);
                            XMACursor.moveToNext();
                        }
                        if(bsize > blocksize){
                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            int iter = (bsize + blocksize - 1) / blocksize;
                            Log.d("[GudangGaram]", "Processing Large Blob With " + iter + " Iteration ");
                            for(int chunk = 0; chunk <= iter; chunk ++){
                                if(chunk == iter){
                                    int remaining = bsize - elapse;
                                    kueri_big = " SELECT substr(TP_FileData," + remaining + ", " + blocksize + ") " +
                                            " FROM GGGG_PPCAP_TP_PICTURE " +
                                            " WHERE TP_TaskDID = " + pTaskDID + " " +
                                            " AND TP_AttachmentNo = " + pSeq + " ";
                                    elapse = elapse + remaining;
                                }
                                else{
                                    int blockchunk = (chunk * blocksize) + 1;
                                    kueri_big = " SELECT substr(TP_FileData," + blockchunk + ", " + blocksize + ") " +
                                            " FROM GGGG_PPCAP_TP_PICTURE " +
                                            " WHERE TP_TaskDID = " + pTaskDID + " " +
                                            " AND TP_AttachmentNo = " + pSeq + " ";
                                    elapse = blockchunk;
                                }
                                Log.d("[GudangGaram]", "Process Chunk Query [" + chunk + "] elapse " + elapse + " : " + kueri_big);

                                Cursor XMBCursor = db.rawQuery(kueri_big, null);
                                xbcount = XMBCursor.getCount();
                                try{
                                    if (XMBCursor != null && XMBCursor.moveToFirst()) {
                                        if (xbcount > 0) {
                                            while (!XMBCursor.isAfterLast()) {
                                                outputStream.write(XMBCursor.getBlob(0));
                                                XMBCursor.moveToNext();
                                            }
                                        }
                                    }
                                }
                                catch(Exception e){
                                    Log.d("[GudangGaram]", "Processing Large Blob Exception : " + e.getMessage().toString());
                                }
                                finally {
                                    XMBCursor.close();
                                }
                            }
                            result = outputStream.toByteArray();
                        }
                        else{
                            Log.d("[GudangGaram]", "Processing Normal Blob");
                            String kueri_normal = " SELECT TP_FileData " +
                                    " FROM GGGG_PPCAP_TP_PICTURE " +
                                    " WHERE TP_TaskDID = " + pTaskDID + " " +
                                    " AND TP_AttachmentNo = " + pSeq + " ";
                            Cursor XMRCursor = db.rawQuery(kueri_normal, null);
                            xrcount = XMRCursor.getCount();
                            try{
                                if (XMRCursor != null && XMRCursor.moveToFirst()) {
                                    if (xrcount > 0) {
                                        while (!XMRCursor.isAfterLast()) {
                                            result = XMRCursor.getBlob(0);
                                            XMRCursor.moveToNext();
                                        }
                                    }
                                }
                            }
                            catch(Exception e){
                                Log.d("[GudangGaram]", "Processing Normal Blob Exception : " + e.getMessage());
                            }
                            finally {
                                XMRCursor.close();
                            }
                        } // end if
                    }// end if
                } // end if
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "Image PtaskDID = " + pTaskDID + " Seq =" + pSeq + " load Exception : " + e.getMessage().toString());
            }
            finally {
                //db.close();
                XMACursor.close();
            }
            Log.d("[GudangGaram]", "Image (" + pSeq + ") load :" + kueri + " xcount = " + xcount);

        return result;
    }

    public long updateReason(String pTaskDID,String pReason) {
        // ====================================================================================================
        // this function is purpose to Update Reason based on TaskDID Parameters
        // ====================================================================================================
        long result;
        String where;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TT_REASON, pReason);
        where = KEY_TT_TASK_DID   + " = " + pTaskDID;

        result = db.update(TABLE_TT_TASK, values,where,null);

        Log.d("[GudangGaram]", "updateReason pTaskDID = " + pTaskDID + " pReason = " + pReason + " Status = "+ result);

        //db.close();
        return result;
    }

    public long updateClosedFlag(String pTaskDID) {
        // ====================================================================================================
        // this function is purpose to Update Closed Flag based on TaskDID Parameters
        // ====================================================================================================
        long result;
        String where;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TT_CLOSED, "Y");
        where = KEY_TT_TASK_DID   + " = " + pTaskDID + " AND COALESCE(" + KEY_TT_CLOSED + ",'N') = 'N' ";

        result = db.update(TABLE_TT_TASK, values,where,null);

        Log.d("[GudangGaram]", "updateClosed Flag pTaskDID = " + pTaskDID);

        //db.close();
        return result;
    }

    public long updateAllClosedFlag(String pTaskDID) {
        // ====================================================================================================
        // this function is purpose to Update All Route Closed Flag based on TaskDID Parameters
        // ====================================================================================================
        long result;
        String where;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TT_CLOSED, "Y");
        where = " 1 = 1 " +
                " AND " + KEY_TT_TASK_ID + " IN (SELECT " + KEY_TT_TASK_ID + " FROM " + TABLE_TT_TASK + " WHERE " + KEY_TT_TASK_DID + " = " + pTaskDID + ") " +
                " AND " + KEY_TT_PIC_NIK  + " IN (SELECT " + KEY_TT_PIC_NIK + " FROM " + TABLE_TT_TASK + " WHERE " + KEY_TT_TASK_DID + " = " + pTaskDID + ") " +
                " AND COALESCE(" + KEY_TT_CLOSED + ",'N') = 'N' ";

        result = db.update(TABLE_TT_TASK, values,where,null);

        Log.d("[GudangGaram]", "updateAllClosed Flag pTaskDID = " + pTaskDID);

        //db.close();
        return result;
    }

    public Integer getItemID(String pTaskDID){
        Integer result = 0;
        String kueri = " SELECT TT_ItemID " +
                " FROM GGGG_PPCAP_TT_TASK " +
                " WHERE TT_TaskDID = " + pTaskDID + "; ";

        SQLiteDatabase db = this.getReadableDatabase();
        XRouteCursor = db.rawQuery(kueri, null);
        try{
            XRouteCursor.moveToFirst();
            while (!XRouteCursor.isAfterLast())
            {
                result = XRouteCursor.getInt(0);
                XRouteCursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            XRouteCursor.close();
            //db.close();
        }
        return result;
    }


    public long addItemPicture(String  pTaskDID, Integer  pSeq, byte[] pFileData,String pFileID, String pItemID, String pAttachID, String pLobID)
    {
            // ====================================================================================================
            // this function is purpose to create Master Picture Item after user click Sync in Form Sync
            // ====================================================================================================
            Log.d("[GudangGaram]", "addItemPicture");
            long savestatus = -1;
            String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

        try{
            values.put(KEY_TP_TASK_DID              , pTaskDID);
            values.put(KEY_TP_FILE_ID               , pFileID);
            values.put(KEY_TP_ITEM_ID               , pItemID);
            values.put(KEY_TP_ATTACHMENT_ID         , pAttachID);
            values.put(KEY_TP_ATTACHMENT_NO         , pSeq);
            values.put(KEY_TP_LOB_ID                , pLobID);
            values.put(KEY_TP_FILE_DATA             , pFileData);
            values.put(KEY_TP_CREATED_DATE          , sysdate);

            savestatus = db.insert(TABLE_TP_PICTURE,null,values); // key/value -> keys = column names/ values = column values
        }
        catch(Exception e){
        }
        finally {
            Log.d("[GudangGaram]", "addItemPicture savestatus :" + savestatus);
        }

        return savestatus;
    }

    public long updateItemPicture(
                String pTaskDID,
                Integer pSeq,
                byte[] pFileData,
                Integer pchangeflag)
    {
            // ====================================================================================================
            // this function is purpose to Update Blob Master Picture based on TaskDID Parameters
            // ====================================================================================================
            long updatestatus;
            String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
            String where   = KEY_TP_TASK_DID   + " = " + pTaskDID + " AND " + KEY_TP_ATTACHMENT_NO + " = " + pSeq;

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_TP_FILE_DATA              , pFileData);
            values.put(KEY_TP_CREATED_DATE           , sysdate);
            values.put(KEY_TP_LOB_ID                 , pchangeflag);
            //values.put(KEY_TP_LOB_ID                 , -1);

            updatestatus = db.update(TABLE_TP_PICTURE, values,where,null);
            //db.close();

            Log.d("[GudangGaram]", "updateItemPicture Query = "+ where + " Result = "+ updatestatus);

        return updatestatus;
    }

    public long deleteItemPicture(String pFileID, String pPicNIK, String pTaskDID, String pSeq){
            String pWhere;
            long deletestatus;
            Integer countpic;

            // ====================================================================================================
            // this function is purpose to delete Master Picture based on Task Detail ID Parameters
            // ====================================================================================================

            pWhere = KEY_TP_TASK_DID + " = " + pTaskDID +
                    " AND " + KEY_TP_ATTACHMENT_NO + " = " + pSeq;

            SQLiteDatabase db = this.getWritableDatabase();
            deletestatus = db.delete(TABLE_TP_PICTURE, pWhere ,null);
            // kalo picture itemnya sudah habis di delete maka delete item task nya
            Log.d("[GudangGaram]: ", " deleteItemPicture >> " + pWhere);

            countpic = countItemP(pTaskDID);
            Log.d("[GudangGaram]: ", " countpic = " + countpic);

            if(countpic == 0){
                Log.d("[GudangGaram]: ", " countItemP = 0 Delete Task ");
                // delete task related to picture yang sudah di sync ke oracle
                delItem(pFileID, pPicNIK);
            }

            //db.close();
            return deletestatus;
    }

    public long delItem(String pFileID, String pPicNIK){
        String pWhere;
        long deletestatus;

        // ====================================================================================================
        // this function is purpose to delete Master Item Task based on Task Detail Item ID Parameters
        // ====================================================================================================
        pWhere = KEY_TT_FILE_ID  + " = " + pFileID + " AND " + KEY_TT_PIC_NIK + " = '" + pPicNIK + "' ";

        Log.d("[GudangGaram]: ", " deleteTaskItem >> " + pWhere);

        SQLiteDatabase db = this.getWritableDatabase();
        deletestatus = db.delete(TABLE_TT_TASK, pWhere,null);
        return deletestatus;
    }

    public List<String> getLOV(String ML_Context){
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = "SELECT X." + KEY_M_LOOKUP_VALUE + ", X." + KEY_M_LOOKUP_VALUE + ", X." + KEY_M_LOOKUP_MEANNG + " " +
                "FROM (" +
                    " SELECT " + KEY_M_LOOKUP_CONTEXT + "," +
                                 KEY_M_LOOKUP_VALUE  + "," +
                                 KEY_M_LOOKUP_MEANNG +
                    " FROM "     + TABLE_M_LOOKUP +
                    " WHERE "    + KEY_M_LOOKUP_CONTEXT + " = '" + ML_Context + "' " +
                    " UNION ALL " +
                    " SELECT '" + ML_Context + "' AS " + KEY_M_LOOKUP_CONTEXT + "," +
                             "' ' AS " + KEY_M_LOOKUP_VALUE + "," +
                             "' ' AS " + KEY_M_LOOKUP_MEANNG + " " +
                    ") X " +
                " ORDER BY X." + KEY_M_LOOKUP_VALUE + ";";


        Log.d("[GudangGaram]", " getLOV Query >> " + kueri);

        XMICursor = db.rawQuery(kueri, null);
        try{
            if(XMICursor.getCount() > 0){
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    lvi.add(XMICursor.getString(1));
                    XMICursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            XMICursor.close();
            //db.close();
        }

        return lvi;
    }


    public List<StringWithTag> getEmployeeList(){
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();

        SQLiteDatabase db =  this.getReadableDatabase();
        String kueri = " SELECT NIK, NAME " +
                       " FROM GGGG_PPCAP_V_LOGIN " +
                       " ORDER BY NIK ";

        XMICursor = db.rawQuery(kueri, null);
        try{
            if(XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    String key  = XMICursor.getString(1);
                    String value = XMICursor.getString(0);
                    Log.d("[GudangGaram]", "KVP Load : (Key,Value) -> " + key + "," + value);
                    lvi.add(new StringWithTag(value, key));
                    XMICursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            XMICursor.close();
            //db.close();
        }
        return lvi;
    }

    public Integer FinishRouteStatus(String pTaskDID){
        Integer result = 0;
        String kueri = " SELECT (CASE WHEN ((TT_CloseCount = TT_Total) AND (TT_Total > 0)) THEN TT_TaskID ELSE -1 END) TT_TaskID " +
                       " FROM GGGG_PPCAP_TTH_TASK_V " +
                       " WHERE TT_TaskID IN (" +
                            " SELECT TT_TaskID " +
                            " FROM GGGG_PPCAP_TT_TASK " +
                            " WHERE TT_TaskDID = " + pTaskDID + ") " +
                       "  AND TT_PIC_NIK IN ( " +
                            " SELECT TT_PIC_NIK " +
                            " FROM GGGG_PPCAP_TT_TASK " +
                            " WHERE TT_TaskDID = " + pTaskDID + "); ";

        SQLiteDatabase db = this.getReadableDatabase();
        XRouteCursor = db.rawQuery(kueri, null);
        try{
            XRouteCursor.moveToFirst();
            while (!XRouteCursor.isAfterLast())
            {
                result = XRouteCursor.getInt(0);
                XRouteCursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            XRouteCursor.close();
            //db.close();
        }
        return result;
    }

    public Integer getFileID(String pTaskDID){
        Integer result = 0;
        String kueri = " SELECT TT_FileID " +
                " FROM GGGG_PPCAP_TT_TASK " +
                " WHERE TT_TaskDID = " + pTaskDID + "; ";

        SQLiteDatabase db = this.getReadableDatabase();
        XRouteCursor = db.rawQuery(kueri, null);
        try{
            XRouteCursor.moveToFirst();
            while (!XRouteCursor.isAfterLast())
            {
                result = XRouteCursor.getInt(0);
                XRouteCursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            XRouteCursor.close();
           // db.close();
        }
        return result;
    }

    public Integer get_RouteCount(String pTaskDID){
        Integer result = 0;
        String kueri = " SELECT count(TT_TaskDID) " +
                       " FROM GGGG_PPCAP_TT_TASK " +
                       " WHERE TT_FileID IN (" +
                            " SELECT TT_FileID " +
                            " FROM GGGG_PPCAP_TT_TASK " +
                            " WHERE TT_TaskDID = " + pTaskDID + ") " +
                       "  AND TT_PIC_NIK IN ( " +
                            " SELECT TT_PIC_NIK " +
                            " FROM GGGG_PPCAP_TT_TASK " +
                            " WHERE TT_TaskDID = " + pTaskDID + "); ";
        SQLiteDatabase db = this.getReadableDatabase();

        XRouteCursor = db.rawQuery(kueri, null);
        try{
            XRouteCursor.moveToFirst();
            while (!XRouteCursor.isAfterLast())
            {
                result = XRouteCursor.getInt(0);
                XRouteCursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            XRouteCursor.close();
            //db.close();
        }
        return result;
    }

    public Integer get_NextRouteToClose(String pTaskDID){
        Integer result = 0;
        String kueri   = "";

        if(FinishRouteStatus(pTaskDID) > 0){
            kueri = "SELECT MAX(TT_Seq) NextSeq " +
                    "FROM GGGG_PPCAP_TT_TASK " +
                    "WHERE 1 = 1 " +
                    "  AND TT_TaskDID = " + pTaskDID +
                    "  AND TT_PIC_NIK IN ( " +
                        " SELECT TT_PIC_NIK " +
                        " FROM GGGG_PPCAP_TT_TASK " +
                        " WHERE TT_TaskDID = " + pTaskDID + "); ";

        }
        else{
            kueri = " SELECT coalesce((MIN(TT_Seq)),1) NextSeq " +
                    " FROM GGGG_PPCAP_TT_TASK " +
                    " WHERE 1 = 1 " +
                    " AND TT_Closed = 'N' " +
                    " AND TT_FileID IN (" +
                    " SELECT TT_FileID " +
                    " FROM GGGG_PPCAP_TT_TASK " +
                    " WHERE TT_TaskDID = " + pTaskDID + ") " +
                    "  AND TT_PIC_NIK IN ( " +
                         " SELECT TT_PIC_NIK " +
                         " FROM GGGG_PPCAP_TT_TASK " +
                         " WHERE TT_TaskDID = " + pTaskDID + "); ";
        }


        SQLiteDatabase db = this.getReadableDatabase();
        XRouteCursor = db.rawQuery(kueri, null);
        try{
            XRouteCursor.moveToFirst();
            while (!XRouteCursor.isAfterLast())
            {
                result = XRouteCursor.getInt(0);
                XRouteCursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            XRouteCursor.close();
            //db.close();
        }
        return result;
    }


    public List<String> LoadFileID(String p_NIK) {
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getReadableDatabase();


        String kueri = " SELECT TT_FileID " +
                " FROM GGGG_PPCAP_TTH_TASK_V " +
                " WHERE 1 = 1 " +
                "   AND TT_PIC_NIK = '" + p_NIK + "' " +
                "   AND TT_CloseCount > 0 " +
                "   AND TT_CloseCount = TT_Total " +
                " ORDER BY TT_FileID; ";

        /*
        // for testing purpose only
        String kueri = " SELECT TT_FileID " +
                " FROM GGGG_PPCAP_TTH_TASK_V " +
                " WHERE 1 = 1 " +
                "   AND TT_PIC_NIK = '" + p_NIK + "' " +
                " ORDER BY TT_FileID; ";
        */

        XRouteCursor = db.rawQuery(kueri, null);
        try{
            if(XRouteCursor!=null){
                XRouteCursor.moveToFirst();
                while (!XRouteCursor.isAfterLast())
                {
                    lvi.add(XRouteCursor.getString(0));
                    XRouteCursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            XRouteCursor.close();
            //db.close();
        }

        return lvi;
    }

    public List<DS_TaskItem> get_FileIDList(String pNikPIC){
        Integer counter;
        SQLiteDatabase db = this.getReadableDatabase();
        List<DS_TaskItem> filebatch = new ArrayList<DS_TaskItem>();

                String kueri = " SELECT TT_TaskID, " +
                               "TT_FileID, " +
                               "(CASE WHEN ((TT_CloseCount = TT_Total) AND (TT_Total > 0)) " +
                                    " THEN 'Finish' " +
                                    " ELSE 'Not Finish' " +
                                " END) TT_Status " +
                       " FROM GGGG_PPCAP_TTH_TASK_V " +
                       " WHERE TT_PIC_NIK = '" + pNikPIC + "' " +
                       " ORDER BY TT_TaskID; ";


        Log.d("[GudangGaram]:", "get_FileIDList >> " + kueri);

        counter = 0;
        XRouteCursor = db.rawQuery(kueri, null);
        try{
            XRouteCursor.moveToFirst();
            while (!XRouteCursor.isAfterLast())
            {
                DS_TaskItem ti = new DS_TaskItem();
                ti.setTaskID(XRouteCursor.getInt(0));
                ti.setFileID(XRouteCursor.getInt(1));
                ti.setStatus(XRouteCursor.getString(2));
                ti.setTaskDate("");
                ti.setFileName("");
                ti.setChecked(false);

                Log.d("[GudangGaram]:", "get_FileIDList [" + counter + "] ");

                counter += 1;
                filebatch.add(ti);
                XRouteCursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "get_FileIDList >> " + e.getMessage().toString());
        }
        finally {
            XRouteCursor.close();
            //db.close();
            Log.d("[GudangGaram]:", "get_FileIDList >> Done ");
        }
        return filebatch;
    }

    public List<DS_TaskItemDetail> get_RouteInfoBy(String pPicNIK, String pType, String pKey1, String pKey2) {
        Integer cntr;
        List<DS_TaskItemDetail>         items    = new ArrayList<DS_TaskItemDetail>();
        Map<String,DS_TaskItemDetail>   item_map = new HashMap<String, DS_TaskItemDetail>();

        SQLiteDatabase db = this.getReadableDatabase();

        String kueri =
                " SELECT TT_TaskDID, " +
                " TT_Seq, " +
                " TT_LocRak, " +
                " TT_LocBin, " +
                " TT_LocCom, " +
                " TT_ItemCode, " +
                " TT_ItemDesc, " +
                " coalesce(length(TT_Img1),0) TT_FS1, " +
                " coalesce(length(TT_Img2),0) TT_FS2, " +
                " coalesce(length(TT_Img3),0) TT_FS3, " +
                        " coalesce(TT_Reason,' ') TT_Reason, " +
                        " coalesce(TT_Closed,'N') TT_Closed " +
                " FROM GGGG_PPCAP_TT_TASK " +
                        " WHERE 1 = 1 ";

        if(pType.equals("FileID")){
            kueri = kueri + " AND TT_FileID  = " + pKey1 + " ";
            kueri = kueri + " AND TT_PIC_NIK = '" + pPicNIK + "' ";
        }
        else if(pType.equals("TaskDID")){
            kueri = kueri + " AND TT_TaskDID = " + pKey1 + " ";
        }
        else if(pType.equals("TaskID")){
            kueri = kueri + " AND TT_PIC_NIK = '" + pPicNIK + "' ";
            kueri = kueri + " AND TT_TaskID = " + pKey1 + " ";
        }
        else if(pType.equals("CursorFileID")){
            kueri = kueri + " AND TT_FileID IN (" +
                                    " SELECT TT_FileID " +
                                    " FROM GGGG_PPCAP_TT_TASK " +
                                    " WHERE TT_TaskDID = " + pKey1 + ") " +
                            " AND TT_PIC_NIK IN ( " +
                                " SELECT TT_PIC_NIK " +
                                " FROM GGGG_PPCAP_TT_TASK " +
                                " WHERE TT_TaskDID = " + pKey1 + ") " +
                            " AND TT_Seq = " + pKey2;

        }
        kueri = kueri + " ORDER BY TT_FileID, TT_Seq; ";

        Log.d("[GudangGaram]", "get_RouteInfoBy >> " + kueri);

        XRouteCursor = db.rawQuery(kueri, null);
        try{
            Log.d("[GudangGaram]", "get_RouteInfoBy Query Records Result >> " + XRouteCursor.getCount());
            XRouteCursor.moveToFirst();
            cntr = 0;
            while (!XRouteCursor.isAfterLast())
            {
                DS_TaskItemDetail tid = new DS_TaskItemDetail();
                tid.setTaskDID(XRouteCursor.getInt(0));
                tid.setSeq(XRouteCursor.getInt(1));
                tid.setLocRak(XRouteCursor.getString(2));
                tid.setLocBin(XRouteCursor.getString(3));
                tid.setLocCom(XRouteCursor.getString(4));
                tid.setItemCode(XRouteCursor.getString(5));
                tid.setItemDesc(XRouteCursor.getString(6));
                tid.setFS_Img1(XRouteCursor.getInt(7));
                tid.setFS_Img2(XRouteCursor.getInt(8));
                tid.setFS_Img3(XRouteCursor.getInt(9));
                tid.setReason(XRouteCursor.getString(10));
                tid.setIsClosed(XRouteCursor.getString(11));

                if(XRouteCursor.getInt(7) > 0){
                    Log.d("[GudangGaram]", "File1 ISI");
                    //tid.setImg1(XRouteCursor.getBlob(10));
                }
                if(XRouteCursor.getInt(8) > 0){
                    Log.d("[GudangGaram]", "File2 ISI");
                    //tid.setImg2(XRouteCursor.getBlob(11));
                }
                if(XRouteCursor.getInt(9) > 0){
                    Log.d("[GudangGaram]", "File3 ISI");
                    //tid.setImg3(XRouteCursor.getBlob(12));
                }

                cntr +=1;
                Log.d("[GudangGaram]", "Load get_RouteForFileID " + cntr);
                items.add(tid);
                item_map.put(Integer.toString(XRouteCursor.getInt(0)),tid);

                XRouteCursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "get_RouteInfoBy Exeption " + e.getMessage().toString());
        }
        finally {
            XRouteCursor.close();
            //db.close();
        }

        return items;
    }

    public List<DS_TaskItemPicture> get_ImageFileID(String pFileID, String pPicNIK) {
        String kueri;
        Integer cntr;
        List<DS_TaskItemPicture> pictures = new ArrayList<DS_TaskItemPicture>();

        SQLiteDatabase db = this.getReadableDatabase();

            kueri = "  SELECT COALESCE(P.TP_ID, -1) TP_ID, " +
                    " T.TT_TaskDID TP_TaskDID, " +
                    " T.TT_ItemID, " +
                    " COALESCE(P.TP_AttachmentID,-1) TP_AttachmentID, " +
                    " COALESCE(P.TP_AttachmentNO,1) TP_AttachmentNo, " +
                    " COALESCE(P.TP_LobID,0) TP_LobID, " +
                    " P.TP_Created_Date, " +
                    " T.TT_FileID TP_File_ID,  " +
                    " COALESCE(T.TT_Reason,' ') TT_Reason " +
                    " FROM GGGG_PPCAP_TT_TASK T " +
                    " LEFT OUTER JOIN GGGG_PPCAP_TP_PICTURE  P  ON P.TP_TaskDID  = T.TT_TaskDID " +
                    " WHERE 1 = 1 " +
                    "   AND T.TT_FileID  = " + pFileID + " " +
                    "   AND T.TT_PIC_NIK = '" + pPicNIK + "' " +
                    " ORDER BY T.TT_TaskDID, COALESCE(P.TP_AttachmentNO,-1); ";

        Log.d("[GudangGaram]", "Query get_ImageFileID >> " + kueri);

        XRouteCursor = db.rawQuery(kueri, null);
        try{
            Log.d("[GudangGaram]", "get_RouteInfoBy Query Records Result >> " + XRouteCursor.getCount());
            XRouteCursor.moveToFirst();
            cntr = 0;
            while (!XRouteCursor.isAfterLast())
            {
                DS_TaskItemPicture tid = new DS_TaskItemPicture();

                tid.setTaskDID(XRouteCursor.getString(1));
                tid.setItemID(XRouteCursor.getString(2));
                tid.setAttachmentID(XRouteCursor.getString(3));
                tid.setAttachmentNo(XRouteCursor.getInt(4));
                tid.setLobID(XRouteCursor.getString(5));
                tid.setCreationDate(XRouteCursor.getString(6));
                tid.setFileID(XRouteCursor.getString(7));
                tid.setReason(XRouteCursor.getString(8));
                //tid.setFileData(XRouteCursor.getBlob(9));

                cntr +=1;
                Log.d("[GudangGaram]", "Load get_RouteForFileID " + cntr);
                pictures.add(tid);

                XRouteCursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "get_RouteInfoBy Exception " + e.getMessage().toString());
        }
        finally {
            XRouteCursor.close();
            //db.close();
        }

        return pictures;
    }


    public List<DS_TaskItem> getBatchList(String pPicNIK){
        List<DS_TaskItem> pbatch = new ArrayList<DS_TaskItem>();
        SQLiteDatabase db = this.getReadableDatabase();

        String kueri = " SELECT TT_TaskID, TT_FileID " +
                       " FROM GGGG_PPCAP_TTH_TASK_V " +
                       " WHERE TT_PIC_NIK = '" + pPicNIK + "' " +
                       " ORDER BY TT_TaskID ";

        XMICursor = db.rawQuery(kueri, null);
        try{
            XMICursor.moveToFirst();
            while (!XMICursor.isAfterLast())
            {
                DS_TaskItem ix = new DS_TaskItem();
                ix.setTaskID(XMICursor.getInt(0));
                ix.setFileID(XMICursor.getInt(1));
                pbatch.add(ix);
                XMICursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            XMICursor.close();

            //db.close();
        }
        return pbatch;
    }

}
