package com.gudanggaramtbk.myppcap;

/**
 * Created by LuckyM on 3/13/2018.
 */

public interface SoapSetFlagTaskTaskAsyncResponse {
    void PostDownloadAction(String output);
}
