package com.gudanggaramtbk.myppcap;

import java.util.List;

/**
 * Created by LuckyM on 3/6/2018.
 */

public interface SoapPopulateTaskTaskAsyncResponse {
    void PostSentAction(List<DS_TaskItem> listoutput);
}
