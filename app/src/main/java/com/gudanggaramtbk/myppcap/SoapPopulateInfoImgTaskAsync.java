package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

import static org.kobjects.base64.Base64.decode;

/**
 * Created by luckym on 8/29/2018.
 */

public class SoapPopulateInfoImgTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper            dbHelper;
    private String                    pItemCode;
    private String                    pType;
    private Integer                   pSeq;
    List<DS_ItemImg>                  Result;

    public SoapPopulateInfoImgTaskAsync(){ }
    public interface SoapPopulateInfoImgTaskAsyncResponse {
        void PostSentAction(List<DS_ItemImg> output);
    }
    public SoapPopulateInfoImgTaskAsync.SoapPopulateInfoImgTaskAsyncResponse delegate = null;
    public SoapPopulateInfoImgTaskAsync(SoapPopulateInfoImgTaskAsync.SoapPopulateInfoImgTaskAsyncResponse delegate){ this.delegate = delegate; }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String pType,
                             String pItemCode,
                             Integer pSeq){
        Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: setAttribute");
        this.context          = context;
        this.dbHelper         = dbHelper;
        this.config           = config;
        this.pType            = pType;
        this.pItemCode        = pItemCode;
        this.pSeq             = pSeq;
        this.pd = new ProgressDialog(this.context);
        Result = new ArrayList<DS_ItemImg>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: onPreExecute");
        pd.setMessage("Retrieve Image From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        if(pd.isShowing()){
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
        Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: onProgressUpdate :: " + args[0].toString());
    }


    @Override
    protected void onPostExecute(String file_url) {
        super.onPostExecute(file_url);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: onPostExecute");
        try {
            if (Result.size() == 0) {
                Toast.makeText(context, "No Image Result", Toast.LENGTH_LONG).show();
            }
            delegate.PostSentAction(Result);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: onPostExecute Exception " + e.getMessage());
        }
        Toast.makeText(context,"Retrieve Image Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        String  v_file_id;
        String  v_item_id;
        String  v_item_code;
        Integer v_seq;
        Integer v_fs_img;
        String  v_faid_img;
        String  v_ffid_img;
        String  v_Img;
        byte[]  pCFileData;

        Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: doInBackground begin");
        //Web method call
        ImageHelper ih = new ImageHelper();
        try {
            String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
            String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
            String OPERATION_NAME = "Get_Item_Img";
            String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);

            SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

            // =============== p_type ===================
            PropertyInfo prop_p_type = new PropertyInfo();
            prop_p_type.setName("p_type");
            prop_p_type.setValue(pType);
            prop_p_type.setType(String.class);


            // =============== p_item_code ===================
            PropertyInfo prop_p_itemCode = new PropertyInfo();
            prop_p_itemCode.setName("p_item_code");
            prop_p_itemCode.setValue(pItemCode);
            prop_p_itemCode.setType(String.class);

            // =============== p_seq ===================
            PropertyInfo prop_p_Seq = new PropertyInfo();
            prop_p_Seq.setName("p_seq");
            prop_p_Seq.setValue(pSeq);
            prop_p_Seq.setType(String.class);


            request.addProperty(prop_p_type);
            request.addProperty(prop_p_itemCode);
            request.addProperty(prop_p_Seq);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj  = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));
                Result.clear();

                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);
                        DS_ItemImg im = new DS_ItemImg();
                        /*
                            output reult format :
                            ===================================
                            0. ITEM_ID
                            1. ITEM_CODE
                            2. SEQ
                            3. FS_IMG
                            4. FAID_IMG
                            5. FFID_IMG
                            6. FIMG
                            ===================================
                         */

                        v_item_code = obj3.getProperty(0).toString();
                        v_item_id   = obj3.getProperty(1).toString();
                        v_seq       = Integer.parseInt(obj3.getProperty(2).toString());
                        v_fs_img    = Integer.parseInt(obj3.getProperty(3).toString());
                        v_faid_img  = obj3.getProperty(4).toString();
                        v_ffid_img  = obj3.getProperty(5).toString();
                        v_Img       = obj3.getProperty(6).toString();

                        im.setItemID(v_item_id);
                        im.setItemCode(v_item_code);
                        im.setAttachmentNo(v_seq);
                        im.setAttachmentSize(v_fs_img);
                        im.setAttachmentID(v_faid_img);
                        im.setImgID(v_ffid_img);
                        pCFileData = ih.compressImageByte(decode(v_Img));
                        im.setFileData(pCFileData);

                        Log.d("[GudangGaram]","SoapPopulateInfoImgTaskAsync :: doInBackground Parameters :");
                        Log.d("[GudangGaram]","===================================================");
                        Log.d("[GudangGaram]", "ITEM CODE : " + obj3.getProperty(0).toString());
                        Log.d("[GudangGaram]", "ITEM ID   : " + obj3.getProperty(1).toString());
                        Log.d("[GudangGaram]", "SEQ       : " + obj3.getProperty(2).toString());
                        Log.d("[GudangGaram]", "FS IMG    : " + obj3.getProperty(3).toString());
                        Log.d("[GudangGaram]", "FAID IMG  : " + obj3.getProperty(4).toString());
                        Log.d("[GudangGaram]", "FFID IMG  : " + obj3.getProperty(5).toString());
                        Log.d("[GudangGaram]", "FIMG      : " + obj3.getProperty(6).toString());
                        Log.d("[GudangGaram]","===================================================");

                        Result.add(im);

                        publishProgress("Retrieve Item " + v_item_code + " Image Seq " + v_seq);
                        try {
                            Thread.sleep(500);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: doInBackground Exception :: Catch Http Transport : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: doInBackground end");
        }


        return "";
    }
}
