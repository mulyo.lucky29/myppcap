package com.gudanggaramtbk.myppcap;

import java.util.List;

/**
 * Created by LuckyM on 8/28/2018.
 */

public interface SoapPopulateInfoTaskAsyncResponse {
    void PostSentAction(List<DS_ItemInfo> listoutput);
}
