package com.gudanggaramtbk.myppcap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 6/8/2018.
 */

public class CustomListAdapterTask extends ArrayAdapter<DS_TaskItem> {
        Context            mcontext;
        List<DS_TaskItem>  TrxList;
        ListView           olisttask;
        ArrayList<Boolean> positionArray;

        private LayoutInflater mInflater;
        public boolean checkBoxState[];

    public CustomListAdapterTask (Context context, List<DS_TaskItem> list, ListView listTask)
        {
            super(context,0,list);
            mcontext = context;
            TrxList = list;
            checkBoxState=new boolean[list.size()];
            olisttask = listTask;
            mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            positionArray = new ArrayList<Boolean>(list.size());
            for(int i =0;i<list.size();i++){
                positionArray.add(false);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolderTaskItem holder;
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlisttask, parent, false);
            holder = new ViewHolderTaskItem();
            holder.Xchk_select             = (CheckBox) convertView.findViewById(R.id.chk_select);
            holder.Xtxt_task_id            = (TextView) convertView.findViewById(R.id.txt_task_id);
            holder.Xtxt_file_id            = (TextView) convertView.findViewById(R.id.txt_file_id);
            holder.Xtxt_file_name          = (TextView) convertView.findViewById(R.id.txt_file_name);
            holder.Xtxt_task_date          = (TextView) convertView.findViewById(R.id.txt_task_date);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderTaskItem) convertView.getTag();
            holder.Xchk_select.setOnCheckedChangeListener(null);
        }

        holder.Xchk_select.setFocusable(false);
        holder.Xchk_select.setChecked(positionArray.get(position));
        holder.Xchk_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()) {
                    olisttask.setItemChecked(position,true);
                    v.setSelected(true);
                    positionArray.set(position, true);
                    Log.d("[GudangGaram]: ", "check (" + position + ") true");
                }
                else{
                    olisttask.setItemChecked(position,false);
                    v.setSelected(false);
                    positionArray.set(position, false);
                    Log.d("[GudangGaram]: ", "check (" + position + ") false");
                }
            }
        });
            CheckBox Tchk_select          =  holder.Xchk_select;
            TextView Txt_task_id          =  holder.Xtxt_task_id;
            TextView Txt_file_id          =  holder.Xtxt_file_id;
            TextView Txt_file_name        =  holder.Xtxt_file_name;
            TextView Txt_task_date        =  holder.Xtxt_task_date;

            DS_TaskItem o = getItem(position);
            Tchk_select.setChecked(olisttask.isItemChecked(position));
            Txt_task_id.setText(o.getTaskID().toString());
            Txt_file_id.setText(o.getFileID().toString());
            Txt_file_name.setText(o.getFileName().toString());
            Txt_task_date.setText(o.getTaskDate().toString());

        return convertView;
    }
}
