package com.gudanggaramtbk.myppcap;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class LoadTask extends AppCompatActivity {
    private SharedPreferences config;
    private MySQLiteHelper              dbHelper;
    private Button                      Cmd_Populate;
    private Button                      Cmd_Submit;
    private Button                      Cmd_Close;
    private ListView                    listTaskMI;
    private List<DS_TaskItem>           lvitask;
    private SoapPopulateTask            populateTaskList;
    private SoapRetrieveList            downloadTaskItem;
    private String                      x_pic_nik;
    private String                      w_device_id;

    private String                   StrSessionNIK;
    private String                   StrSessionName;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Load Task Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",  StrSessionNIK);
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_task);

        config = getSharedPreferences("MyPPCAPSetting", MODE_PRIVATE);
        // get information intent
        StrSessionName   = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK    = getIntent().getStringExtra("PIC_NIK");
        w_device_id      = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("[GudangGaram]:", "LoadTask Load with Parameter  >> " + StrSessionNIK);

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Cmd_Populate     = (Button)findViewById(R.id.cmd_populate_task);
        Cmd_Submit       = (Button)findViewById(R.id.cmd_retrieve);
        Cmd_Close        = (Button)findViewById(R.id.cmd_close);

        Cmd_Populate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    EH_CMD_POPULATE_TASK();
                }
                catch(Exception e){
                }
            }
        });
        Cmd_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SUBMIT();
            }
        });
        Cmd_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_CLOSE();
            }
        });

        listTaskMI = (ListView) findViewById(R.id.lst_taskMI);
        listTaskMI.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        // initialized soap service
        populateTaskList = new SoapPopulateTask(config, listTaskMI);
        populateTaskList.setContext(this);
        populateTaskList.setDBHelper(dbHelper);

        // soap for download task item
        downloadTaskItem = new SoapRetrieveList(config, listTaskMI);
        downloadTaskItem.setContext(this);
        downloadTaskItem.setDBHelper(dbHelper);
    }

    // Event Handler
    public void EH_CMD_POPULATE_TASK(){
        // call WS to retrieve Task List from oracle
        try{
                Log.d("[GudangGaram]:", "populateTaskList.Retrieve >> ");
                //lvitask =
                        populateTaskList.Retrieve(StrSessionNIK);
                //Log.d("[GudangGaram]:", "Post Result Count  : " + lvitask.size());
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "populateTaskList.Retrieve Exception >> " + e.getMessage().toString());
        }
    }

    public void EH_CMD_SUBMIT(){
        if(listTaskMI.getCount() > 0){
            if(listTaskMI.getCheckedItemCount() > 0){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder
                        .setTitle("Retrieve Data Confirm")
                        .setMessage("Confirm Retrieve Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_DOWNLOAD_TASK_ITEM();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            else{
                Toast.makeText(getApplicationContext(),"No Data Selected", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(getApplicationContext(),"No Data Can Be Retrived", Toast.LENGTH_LONG).show();
        }
    }

    public void EH_DOWNLOAD_TASK_ITEM(){
        SparseBooleanArray checkedItems = listTaskMI.getCheckedItemPositions();
        Log.d("[GudangGaram]:", "checkedItems >> " +  checkedItems.size());
        try {
            if (checkedItems != null) {
                for (int i = 0; i < checkedItems.size(); i++) {
                    if (checkedItems.valueAt(i)) {
                        View HoldView = listTaskMI.getAdapter().getView(checkedItems.keyAt(i),null, null);
                        ViewHolderTaskItem holder = (ViewHolderTaskItem) HoldView.getTag();
                        String w_task_id = holder.Xtxt_task_id.getText().toString();

                        Toast.makeText(getApplicationContext(),"Download " + i, Toast.LENGTH_LONG).show();
                        // call WS to retrieve list to local database
                        downloadTaskItem.Retrieve(w_task_id,StrSessionNIK,w_device_id);
                    }
                }
            }
        }
        catch(Exception e){
        }
    }

    public void EH_CMD_CLOSE(){
        /*
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        */
        Intent I_main;
        Log.d("[GudangGaram]: ","EH_CMD_CLOSE");
        I_main = new Intent(this, MainActivity.class);
        I_main.putExtra("PIC_NIK",  StrSessionNIK);
        I_main.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_main);
    }

}
