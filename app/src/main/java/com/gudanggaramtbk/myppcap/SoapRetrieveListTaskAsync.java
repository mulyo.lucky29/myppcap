package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.kobjects.base64.Base64;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;

import static org.kobjects.base64.Base64.decode;

/**
 * Created by LuckyM on 6/11/2018.
 */

public class SoapRetrieveListTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                    ResultResponse;
    private MySQLiteHelper            dbHelper;
    private String                    SentResponse;
    private String                    TaskID;
    private String                    picNIK;

    //private List<DS_TaskItemDetail>         Result;
    private ArrayAdapter<DS_TaskItemDetail> trxdadapter;

    public SoapRetrieveListTaskAsync(){
    }
    public interface SoapRetrieveListTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveListTaskAsync.SoapRetrieveListTaskAsyncResponse delegate = null;
    public SoapRetrieveListTaskAsync(SoapRetrieveListTaskAsync.SoapRetrieveListTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper    dbHelper,
                             SharedPreferences config,
                             String            pTaskID,
                             String            pPicNIK){

        Log.d("[GudangGaram]", " SoapRetrieveTaskItemTaskAsync :: setAttribute");
        this.context          = context;
        this.dbHelper         = dbHelper;
        this.config           = config;
        this.TaskID           = pTaskID;
        this.picNIK           = pPicNIK;
        this.pd = new ProgressDialog(this.context);
        //Result = new ArrayList<DS_TaskItemDetail>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveListTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve List Task From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onProgressUpdate(String ... values) {
        super.onProgressUpdate(values);
        Log.d("[GudangGaram]", "SoapRetrieveListTaskAsync :: onProgressUpdate");
        pd.setMessage(values[0].toString());
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveListTask :: onPostExecute >> " + ResultResponse);
        try{
            delegate.PostSentAction(ResultResponse);
        }
        catch(Exception e){
        }
        finally {
            if(output.equals("-1")){
                Toast.makeText(context,"Retrieve List Task Done", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(context,"Retrieve List Task" + output + " Done", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static String decompress(String zipText) throws IOException {
        byte[] compressed = Base64.decode(zipText);
        if (compressed.length > 4)
        {
            GZIPInputStream gzipInputStream = new GZIPInputStream(
                    new ByteArrayInputStream(compressed, 4,
                            compressed.length - 4));

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int value = 0; value != -1;) {
                value = gzipInputStream.read();
                if (value != -1) {
                    baos.write(value);
                }
            }
            gzipInputStream.close();
            baos.close();
            String sReturn = new String(baos.toByteArray(), "UTF-8");
            return sReturn;
        }
        else
        {
            return "";
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Long     Result;
        String   v_TaskDID;
        String   v_TaskID;
        String   v_FileID;
        String   v_Seq;
        String   v_ItemID;
        String   v_ItemCode;
        String   v_ItemDesc;
        String   v_LocRak;
        String   v_LocBin;
        String   v_LocCom;
        String   v_RakSeq;
        String   v_BinSeq;
        String   v_PicNik;
        String   v_PICPersonID;
        String   v_PICName;

        String   v_FA1, v_FA2, v_FA3;
        String   v_FF1, v_FF2, v_FF3;
        Integer  v_FS1, v_FS2, v_FS3;
        String   v_Gbr1, v_Gbr2, v_Gbr3;
        byte[]   vImg1, vImg2, vImg3;

        Integer  ncount;
        Integer  nerr;

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Get_List_Task_Detail";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_task_id ===================
        PropertyInfo prop_p_task_id = new PropertyInfo();
        prop_p_task_id.setName("p_task_id");
        prop_p_task_id.setValue(TaskID);
        prop_p_task_id.setType(String.class);
        // =============== p_pic_nik ===================
        PropertyInfo prop_p_pic_nik = new PropertyInfo();
        prop_p_pic_nik.setName("p_pic_nik");
        prop_p_pic_nik.setValue(picNIK);
        prop_p_pic_nik.setType(String.class);

        request.addProperty(prop_p_pic_nik);
        request.addProperty(prop_p_task_id);


        Log.d("[GudangGaram]: ", "p_task_id      : " + TaskID);
        Log.d("[GudangGaram]: ", "p_pic_nik      : " + picNIK);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);


        //Web method call
        try {
            List<HeaderProperty> headers=new ArrayList<HeaderProperty>();
            HeaderProperty headerProperty=new HeaderProperty("Accept-Encoding", "gzip,deflate");
            headers.add(headerProperty);

            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,60000);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            //httpTransport.call(SOAP_ACTION, envelope);
            httpTransport.call(SOAP_ACTION, envelope,headers);

            SoapObject obj, obj1, obj2, obj3;
            obj     = (SoapObject) envelope.getResponse();
            obj1    = (SoapObject) obj.getProperty("diffgram");
            obj2    = (SoapObject) obj1.getProperty("NewDataSet");

            ncount = obj2.getPropertyCount();
            nerr   = 0;
            Log.d("[GudangGaram]", ">>> count  >>> " + ncount.toString());

            for (int i = 0; i < ncount ;i++){
                obj3 = (SoapObject) obj2.getProperty(i);
                Log.d("[GudangGaram]", ">>> obj3.count[" +  i +  "]  >>> " +  obj3.getPropertyCount());

                /*
                    parameter declaration :
                    =======================
                    public long addItem(
                        0.  String   pTaskDID,
                        1.  String   pTaskID,
                        2.  String   pFileID,
                        3.  String   pSeq,
                        4.  String   pItemID,
                        5.  String   pItemCode,
                        6.  String   pItemDesc,
                        7.  String   pLocRak,
                        8.  String   pLocBin,
                        9.  String   pLocCom,
                        10. String   pRakSeq,
                        11. String   pBinSeq,
                        12. String   pPicNik,
                        13. String   pPICPersonID,
                        14. String   pPICName,
                        15. Integer  pFS1,
                        16. Integer  pFS2,
                        17. Integer  pFS3,
                        18. String   pFA1,
                        19. String   pFA2,
                        20. String   pFA3,
                        21. String   pFF1,
                        22. String   pFF2,
                        23. String   pFF3
                 )
                 */

                DS_TaskItemDetail vd = new DS_TaskItemDetail();
                v_TaskDID     = obj3.getProperty(0).toString();
                v_TaskID      = obj3.getProperty(1).toString();
                v_FileID      = obj3.getProperty(2).toString();
                v_Seq         = obj3.getProperty(3).toString();
                v_ItemID      = obj3.getProperty(4).toString();
                v_ItemCode    = obj3.getProperty(5).toString();
                v_ItemDesc    = obj3.getProperty(6).toString();
                v_LocRak      = obj3.getProperty(7).toString();
                v_LocBin      = obj3.getProperty(8).toString();
                v_LocCom      = obj3.getProperty(9).toString();
                v_RakSeq      = obj3.getProperty(10).toString();
                v_BinSeq      = obj3.getProperty(11).toString();
                v_PicNik      = obj3.getProperty(12).toString();
                v_PICPersonID = obj3.getProperty(13).toString();
                v_PICName     = obj3.getProperty(14).toString();
                v_FS1         = Integer.parseInt(obj3.getProperty(15).toString());
                v_FS2         = Integer.parseInt(obj3.getProperty(16).toString());
                v_FS3         = Integer.parseInt(obj3.getProperty(17).toString());
                v_FA1         = obj3.getProperty(18).toString();
                v_FA2         = obj3.getProperty(19).toString();
                v_FA3         = obj3.getProperty(20).toString();
                v_FF1         = obj3.getProperty(21).toString();
                v_FF2         = obj3.getProperty(22).toString();
                v_FF3         = obj3.getProperty(23).toString();


                Log.d("[GudangGaram]", "----------------------------------------------------------");
                Log.d("[GudangGaram]", "[ 0 ]>>> Task DID      >>> " + obj3.getProperty(0).toString());
                Log.d("[GudangGaram]", "[ 1 ]>>> Task ID       >>> " + obj3.getProperty(1).toString());
                Log.d("[GudangGaram]", "[ 2 ]>>> File ID       >>> " + obj3.getProperty(2).toString());
                Log.d("[GudangGaram]", "[ 3 ]>>> Seq           >>> " + obj3.getProperty(3).toString());
                Log.d("[GudangGaram]", "[ 4 ]>>> Item ID       >>> " + obj3.getProperty(4).toString());
                Log.d("[GudangGaram]", "[ 5 ]>>> Item Code     >>> " + obj3.getProperty(5).toString());
                Log.d("[GudangGaram]", "[ 6 ]>>> Item Desc     >>> " + obj3.getProperty(6).toString());
                Log.d("[GudangGaram]", "[ 7 ]>>> Loc Rak       >>> " + obj3.getProperty(7).toString());
                Log.d("[GudangGaram]", "[ 8 ]>>> Loc Bin       >>> " + obj3.getProperty(8).toString());
                Log.d("[GudangGaram]", "[ 9 ]>>> Loc Com       >>> " + obj3.getProperty(9).toString());
                Log.d("[GudangGaram]", "[10 ]>>> Rak Seq       >>> " + obj3.getProperty(10).toString());
                Log.d("[GudangGaram]", "[11 ]>>> Bin Seq       >>> " + obj3.getProperty(11).toString());
                Log.d("[GudangGaram]", "[12 ]>>> PIC Nik       >>> " + obj3.getProperty(12).toString());
                Log.d("[GudangGaram]", "[13 ]>>> PIC Person ID >>> " + obj3.getProperty(13).toString());
                Log.d("[GudangGaram]", "[14 ]>>> PIC Name      >>> " + obj3.getProperty(14).toString());
                Log.d("[GudangGaram]", "[15 ]>>> FS1           >>> " + obj3.getProperty(15).toString());
                Log.d("[GudangGaram]", "[16 ]>>> FS2           >>> " + obj3.getProperty(16).toString());
                Log.d("[GudangGaram]", "[17 ]>>> FS3           >>> " + obj3.getProperty(17).toString());
                Log.d("[GudangGaram]", "[18 ]>>> FAttach ID 1  >>> " + obj3.getProperty(18).toString());
                Log.d("[GudangGaram]", "[19 ]>>> FAttach ID 2  >>> " + obj3.getProperty(19).toString());
                Log.d("[GudangGaram]", "[20 ]>>> FAttach ID 3  >>> " + obj3.getProperty(20).toString());
                Log.d("[GudangGaram]", "[21 ]>>> FFile ID 1    >>> " + obj3.getProperty(21).toString());
                Log.d("[GudangGaram]", "[22 ]>>> FFile ID 2    >>> " + obj3.getProperty(22).toString());
                Log.d("[GudangGaram]", "[23 ]>>> FFile ID 3    >>> " + obj3.getProperty(23).toString());

                Result =  dbHelper.addItem(
                        v_TaskDID,
                        v_TaskID,
                        v_FileID,
                        v_Seq,
                        v_ItemID,
                        v_ItemCode,
                        v_ItemDesc,
                        v_LocRak,
                        v_LocBin,
                        v_LocCom,
                        v_RakSeq,
                        v_BinSeq,
                        v_PicNik,
                        v_PICPersonID,
                        v_PICName,
                        v_FS1,
                        v_FS2,
                        v_FS3,
                        v_FA1,
                        v_FA2,
                        v_FA3,
                        v_FF1,
                        v_FF2,
                        v_FF3
                        );

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress("Add Item Task DID : " + v_TaskDID + " (" + Integer.toString(i+1) + " of " + Integer.toString(ncount) + ")");

                if(Result > 0){
                    Log.d("[GudangGaram]", "Add Item Task DID: " + v_TaskDID);
                }
                else{
                    nerr +=1;
                }
            }
            if(nerr > 0) {
                ResultResponse = "*";
            }
            else{
                ResultResponse = TaskID;
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveTaskItemTaskAsync  :: end doInBackground");
        }

        return "";
    }

}
