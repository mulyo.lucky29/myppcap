package com.gudanggaramtbk.myppcap;

/**
 * Created by LuckyM on 8/8/2018.
 */

public interface SoapRetrieveReasonTaskAsyncResponse {
    void PostDownloadAction(String output);
}
