package com.gudanggaramtbk.myppcap;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by LuckyM on 6/8/2018.
 */

public class DS_TaskItemDetail {
    private Integer TaskDID;
    private Integer TaskID;
    private Integer FileID;
    private Integer Seq;
    private Integer ItemID;
    private String  ItemCode;
    private String  ItemDesc;
    private String  LocRak;
    private String  LocBin;
    private String  LocCom;
    private Integer RakSeq;
    private Integer BinSeq;
    private String  PICNik;
    private Integer PICPersonID;
    private String  PICName;
    private String  CreatedDate;
    private String  Reason;
    private String  IsClosed;

    private Integer FS_Img1;
    private Integer FS_Img2;
    private Integer FS_Img3;

    private Integer FA_Img1;
    private Integer FA_Img2;
    private Integer FA_Img3;

    private Integer FF_Img1;
    private Integer FF_Img2;
    private Integer FF_Img3;

    private byte[]  Img1;
    private byte[]  Img2;
    private byte[]  Img3;

    // getter
    public Integer getTaskDID() {
        return TaskDID;
    }
    public Integer getTaskID() {
        return TaskID;
    }
    public Integer getSeq() {
        return Seq;
    }
    public Integer getItemID() {
        return ItemID;
    }
    public String getItemCode() {
        return ItemCode;
    }
    public String getItemDesc() {
        return ItemDesc;
    }
    public String getLocRak() {
        return LocRak;
    }
    public String getLocBin() {
        return LocBin;
    }
    public String getLocCom() {
        return LocCom;
    }
    public Integer getRakSeq() {
        return RakSeq;
    }
    public Integer getBinSeq() {
        return BinSeq;
    }
    public String getPICNik() {
        return PICNik;
    }
    public Integer getPICPersonID() {
        return PICPersonID;
    }
    public String getPICName() {
        return PICName;
    }
    public String getCreatedDate() {
        return CreatedDate;
    }
    public Integer getFileID() {
        return FileID;
    }
    public Integer getFS_Img1() { return FS_Img1; }
    public Integer getFS_Img2() { return FS_Img2; }
    public Integer getFS_Img3() { return FS_Img3; }
    public Integer getFA_Img1() { return FA_Img1; }
    public Integer getFA_Img2() { return FA_Img2; }
    public Integer getFA_Img3() { return FA_Img3; }
    public Integer getFF_Img1() { return FF_Img1; }
    public Integer getFF_Img2() { return FF_Img2; }
    public Integer getFF_Img3() { return FF_Img3; }
    public byte[] getImg1() { return Img1; }
    public byte[] getImg2() { return Img2; }
    public byte[] getImg3() { return Img3; }
    public String getReason() {
        return Reason;
    }
    public String getIsClosed() {
        return IsClosed;
    }
    // setter
    public void setTaskDID(Integer taskDID) {
        TaskDID = taskDID;
    }
    public void setTaskID(Integer taskID) {
        TaskID = taskID;
    }
    public void setSeq(Integer seq) {
        Seq = seq;
    }
    public void setItemID(Integer itemID) {
        ItemID = itemID;
    }
    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }
    public void setItemDesc(String itemDesc) {
        ItemDesc = itemDesc;
    }
    public void setLocRak(String locRak) {
        LocRak = locRak;
    }
    public void setLocBin(String locBin) {
        LocBin = locBin;
    }
    public void setLocCom(String locCom) {
        LocCom = locCom;
    }
    public void setRakSeq(Integer rakSeq) {
        RakSeq = rakSeq;
    }
    public void setBinSeq(Integer binSeq) {
        BinSeq = binSeq;
    }
    public void setPICNik(String PICNik) {
        this.PICNik = PICNik;
    }
    public void setPICPersonID(Integer PICPersonID) {
        this.PICPersonID = PICPersonID;
    }
    public void setPICName(String PICName) {
        this.PICName = PICName;
    }
    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
    public void setFileID(Integer fileID) {
        FileID = fileID;
    }
    public void setFS_Img1(Integer FS_Img1) { this.FS_Img1 = FS_Img1; }
    public void setFS_Img2(Integer FS_Img2) { this.FS_Img2 = FS_Img2; }
    public void setFS_Img3(Integer FS_Img3) { this.FS_Img3 = FS_Img3; }
    public void setFF_Img1(Integer FF_Img1) { this.FF_Img1 = FF_Img1; }
    public void setFF_Img2(Integer FF_Img2) { this.FF_Img2 = FF_Img2; }
    public void setFF_Img3(Integer FF_Img3) { this.FF_Img3 = FF_Img3; }
    public void setFA_Img1(Integer FA_Img1) { this.FA_Img1 = FA_Img1; }
    public void setFA_Img2(Integer FA_Img2) { this.FA_Img2 = FA_Img2; }
    public void setFA_Img3(Integer FA_Img3) { this.FA_Img3 = FA_Img3; }
    public void setImg1(byte[] img1) { Img1 = img1; }
    public void setImg2(byte[] img2) { Img2 = img2; }
    public void setImg3(byte[] img3) { Img3 = img3; }
    public void setReason(String reason) {
        Reason = reason;
    }
    public void setIsClosed(String isClosed) {
        IsClosed = isClosed;
    }

}
