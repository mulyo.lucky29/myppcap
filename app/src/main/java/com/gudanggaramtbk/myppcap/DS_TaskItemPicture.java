package com.gudanggaramtbk.myppcap;

import java.sql.Blob;

/**
 * Created by LuckyM on 3/12/2018.
 */

public class DS_TaskItemPicture {
    private String  ImgID;
    private String  TaskDID;
    private String  Reason;
    private String  FileID;
    private String  ItemID;
    private String  AttachmentID;
    private Integer AttachmentNo;
    private String  LobID;
    private byte[]  FileData;
    private String  CreationDate;
    private boolean Changed;

    // =============== getter =====================
    public String getImgID() { return ImgID; }
    public String getTaskDID() { return TaskDID; }
    public String getFileID() { return FileID; }
    public String getItemID() {
        return ItemID;
    }
    public String getCreationDate() {
        return CreationDate;
    }
    public String getAttachmentID() {
        return AttachmentID;
    }
    public Integer getAttachmentNo() {
        return AttachmentNo;
    }
    public String getLobID() { return LobID; }
    public byte[] getFileData() {
        return FileData;
    }
    public String getReason() {
        return Reason;
    }
    public boolean isChanged() {
        return Changed;
    }
    // =============== setter =====================
    public void setImgID(String imgID) { ImgID = imgID; }
    public void setTaskDID(String taskDID) { TaskDID = taskDID; }
    public void setFileID(String fileID) { FileID = fileID; }
    public void setItemID(String itemID) {
        ItemID = itemID;
    }
    public void setCreationDate(String creationDate) {
        CreationDate = creationDate;
    }
    public void setAttachmentID(String attachmentID) {
        AttachmentID = attachmentID;
    }
    public void setAttachmentNo(Integer attachmentNo) {
        AttachmentNo = attachmentNo;
    }
    public void setLobID(String lobID) { LobID = lobID; }
    public void setFileData(byte[] fileData) {
        FileData = fileData;
    }
    public void setReason(String reason) {
        Reason = reason;
    }
    public void setChanged(boolean changed) {
        Changed = changed;
    }

}
