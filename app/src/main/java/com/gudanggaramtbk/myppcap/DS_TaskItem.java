package com.gudanggaramtbk.myppcap;

/**
 * Created by LuckyM on 6/8/2018.
 */


public class DS_TaskItem {
    private Boolean Checked;
    private Number  TaskID;
    private Number  FileID;
    private String  FileName;
    private String  Status;
    private String  TaskDate;

    // getter
    public Boolean getChecked() {
        return Checked;
    }
    public Number getTaskID() {
        return TaskID;
    }
    public Number getFileID() {
        return FileID;
    }
    public String getFileName() {
        return FileName;
    }
    public String getTaskDate() {
        return TaskDate;
    }
    public String getStatus() { return Status; }

    // setter
    public void setChecked(Boolean checked) {
        Checked = checked;
    }
    public void setTaskID(Number taskID) {
        TaskID = taskID;
    }
    public void setFileID(Number fileID) {
        FileID = fileID;
    }
    public void setFileName(String fileName) {
        FileName = fileName;
    }
    public void setTaskDate(String taskDate) {
        TaskDate = taskDate;
    }
    public void setStatus(String status) { Status = status; }

}
