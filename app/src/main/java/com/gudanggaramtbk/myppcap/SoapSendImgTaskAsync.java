package com.gudanggaramtbk.myppcap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by luckym on 8/29/2018.
 */

public class SoapSendImgTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper     dbHelper;
    private String             SentResponse;
    private DS_ItemImg         xp_Im;
    private Integer            LPosition;
    private String             device_id;
    private String             strInfo;

    public SoapSendImgTaskAsync(){}

    public interface SoapSendImgTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSendImgTaskAsync.SoapSendImgTaskAsyncResponse delegate = null;
    public SoapSendImgTaskAsync(SoapSendImgTaskAsync.SoapSendImgTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String device_id){
        Log.d("[GudangGaram]", "SoapSendImageTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.device_id      = device_id;
        this.pd = new ProgressDialog(this.context);
    }

    public void setParam(DS_ItemImg oDSI){
        xp_Im = new DS_ItemImg();
        xp_Im = oDSI;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapSendImgTaskAsync :: onPreExecute");
        pd.setMessage("Sending Image To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        if(pd.isShowing()){
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
        Log.d("[GudangGaram]", "SoapSendImgTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSendImgTaskAsync :: onPostExecute >> " + output);
        delegate.PostSentAction(output);
        Toast.makeText(context,"Send Image Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;
        String  xp_img_id  = "";
        byte[]  xp_bin;
        String  xp_file_data;
        String  xp_attachment_id;
        String  xp_lob_id;
        Integer xp_seq;
        String  xp_device_id;
        String  xp_creation_date;
        String  xp_fileid;
        String  xp_itemid;
        String  xp_pic_nik;
        String  xp_itemCode;

        //String response = null;
        String NAMESPACE        = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS     = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME   = "Insert_Img";

        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;                  //"http://gudanggaramtbk.com/InsertToOracle";
        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "Soap Address   : " + SOAP_ADDRESS);
        Log.d("[GudangGaram]", "Soap Operation : " + NAMESPACE + OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        xp_itemid        = xp_Im.getItemID();
        xp_itemCode      = xp_Im.getItemCode();
        xp_attachment_id = xp_Im.getAttachmentID();
        xp_lob_id        = xp_Im.getLobID();
        xp_seq           = xp_Im.getAttachmentNo();
        xp_file_data     = Base64.encodeToString(xp_Im.getFileData(), Base64.DEFAULT);
        xp_device_id     = device_id;
        xp_pic_nik       = xp_Im.getPicNik();
        xp_creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());

        /*
        PROCEDURE INSERT_IMG(
            p_item_id           IN NUMBER,
            p_item_code         IN VARCHAR2,
            p_attachment_id     IN NUMBER,
            p_lob_id            IN NUMBER,
            p_seq               IN NUMBER,
            p_file_data         IN BLOB,
            p_device_id         IN VARCHAR2,
            p_reason_id         IN VARCHAR2,
            p_pic_nik           IN VARCHAR2,
            p_creation_date     IN VARCHAR2,
            p_img_id           OUT NUMBER
        )
        */

        Log.d("[GudangGaram]", "SoapSendImgTaskAsync :: begin doInBackground");

        // =============== p_item_id ========================
        PropertyInfo prop_p_item_id = new PropertyInfo();
        prop_p_item_id.setName("p_item_id");
        prop_p_item_id.setValue(xp_itemid);
        prop_p_item_id.setType(Number.class);
        request.addProperty(prop_p_item_id);


        // =============== p_item_code ========================
        PropertyInfo prop_p_item_code = new PropertyInfo();
        prop_p_item_code.setName("p_item_code");
        prop_p_item_code.setValue(xp_itemCode);
        prop_p_item_code.setType(Number.class);
        request.addProperty(prop_p_item_code);

        // =============== p_attachment_id =====================
        PropertyInfo prop_p_attachment_id = new PropertyInfo();
        prop_p_attachment_id.setName("p_attachment_id");
        prop_p_attachment_id.setValue(xp_attachment_id);
        prop_p_attachment_id.setType(String.class);
        request.addProperty(prop_p_attachment_id);

        // =============== p_lob_id ===================
        PropertyInfo prop_p_lob_id = new PropertyInfo();
        prop_p_lob_id.setName("p_lob_id");
        prop_p_lob_id.setValue(xp_lob_id);
        prop_p_lob_id.setType(String.class);
        request.addProperty(prop_p_lob_id);

        // =============== p_seq =====================
        PropertyInfo prop_p_seq = new PropertyInfo();
        prop_p_seq.setName("p_seq");
        prop_p_seq.setValue(xp_seq);
        prop_p_seq.setType(String.class);
        request.addProperty(prop_p_seq);

        // =============== p_file_data ===================
        PropertyInfo prop_p_file_data = new PropertyInfo();
        prop_p_file_data.setName("p_file_data");
        prop_p_file_data.setValue(xp_file_data);
        prop_p_file_data.setType(String.class);
        request.addProperty(prop_p_file_data);

        // =============== p_device_id ====================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(xp_device_id);
        prop_p_device_id.setType(String.class);
        request.addProperty(prop_p_device_id);

        // =============== p_pic_nik ========================
        PropertyInfo prop_p_pic_nik = new PropertyInfo();
        prop_p_pic_nik.setName("p_pic_nik");
        prop_p_pic_nik.setValue(xp_pic_nik);
        prop_p_pic_nik.setType(String.class);
        request.addProperty(prop_p_pic_nik);


        // =============== p_created_date ===================
        PropertyInfo prop_p_created_date = new PropertyInfo();
        prop_p_created_date.setName("p_creation_date");
        prop_p_created_date.setValue(xp_creation_date);
        prop_p_created_date.setType(String.class);
        request.addProperty(prop_p_created_date);

        Log.d("[GudangGaram]", "SoapSendImgTaskAsync :: doInBackground Parameters: ");
        Log.d("[GudangGaram]", "==============================================================");
        Log.d("[GudangGaram]", "p_item_code       : " + xp_itemid);
        Log.d("[GudangGaram]", "p_item_code       : " + xp_itemCode);
        Log.d("[GudangGaram]", "p_attachment_id   : " + xp_attachment_id);
        Log.d("[GudangGaram]", "p_lob_id          : " + xp_lob_id);
        Log.d("[GudangGaram]", "p_seq             : " + xp_seq);
        Log.d("[GudangGaram]", "p_file_data       : " + xp_file_data);
        Log.d("[GudangGaram]", "p_device_id       : " + xp_device_id);
        Log.d("[GudangGaram]", "p_pic_nik         : " + xp_pic_nik);
        Log.d("[GudangGaram]", "p_creation_date   : " + xp_creation_date);
        Log.d("[GudangGaram]", "==============================================================");

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.implicitTypes = false;
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS, 60000);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            if (envelope.bodyIn instanceof SoapFault) {
                String str= ((SoapFault) envelope.bodyIn).faultstring;
                Log.d("[GudangGaran]","SoapSendImgTaskAsync Fault: " + str);
                SentResponse = str;
            }
            else {
                SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                Log.d("[GudangGaran]","SoapSendImgTaskAsync Result: " + String.valueOf(resultsRequestSOAP));
                xp_img_id = resultsRequestSOAP.getProperty(0).toString();
                SentResponse = xp_img_id;

                publishProgress("Sending Image Item " + xp_itemCode + " [ " + xp_seq + " of 3 ]");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception ex) {
            SentResponse = ex.getMessage().toString();
            Log.d("[GudangGaram]", "Catch : " + SentResponse);
            //response = "<?xml version='1.0' encoding='utf-8'?><Message response='FAIL' ErrorMessage='The System is under maintenance or having some techical issue.'></Message>";
        }

        Log.d("[GudangGaram]", "SentResponse :" + SentResponse);
        Log.d("[GudangGaram]", "SoapSendImgTaskAsync  :: end doInBackground");

        return SentResponse;
    }
}
