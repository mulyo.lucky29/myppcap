package com.gudanggaramtbk.myppcap;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import static org.kobjects.base64.Base64.decode;

/**
 * Created by luckym on 8/28/2018.
 */

public class SoapPopulateInfo {
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper    dbHelper;
    CapturePSActivity         pActivity;
    List<DS_ItemInfo>         TResult;

    // override constructor
    public SoapPopulateInfo(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapPopulateInfo :: Constructor");
        this.config           = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapPopulateInfo :: SetContext");
        this.context = context;
    }
    public void setParentActivity(CapturePSActivity pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapPopulateInfo :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void LoadImageView(Integer idx, byte[] filedata){
        Bitmap bmp       = BitmapFactory.decodeByteArray(filedata, 0, filedata.length);
        Log.d("[GudangGaram]", "LoadImageView " + idx);
        if(idx == 1){
            pActivity.setImg_ps_1(bmp);
        }
        else if(idx == 2){
            pActivity.setImg_ps_2(bmp);
        }
        else if(idx == 3){
            pActivity.setImg_ps_3(bmp);
        }
    }

    public List<DS_ItemInfo> Retrieve(final String pType, final String pItemCode) {
        Log.d("[GudangGaram]", "SoapPopulateInfo :: Retrieve (" +  pType + " ) >> " + pItemCode);
        try {
            SoapPopulateInfoTaskAsync SoapRequest = new SoapPopulateInfoTaskAsync(new SoapPopulateInfoTaskAsync.SoapPopulateInfoTaskAsyncResponse(){
                @Override
                public void PostSentAction(List<DS_ItemInfo> output) {
                    String v_itemID;
                    String v_itemCode;
                    String v_lotNumber;
                    String v_itemDesc;
                    String v_itemBrand;
                    String v_itemSpec;

                    ArrayList<Integer> vfs = new ArrayList<Integer>();

                    TResult = output;
                    v_lotNumber = pActivity.getTxt_ps_LotNumber();
                    // clear item info
                    pActivity.flushInfo();
                    // clear all image view
                    pActivity.flushImg();

                    if(!pType.equals("ItemCode")){
                        pActivity.setTxt_ps_LotNumber(v_lotNumber);
                    }

                    try{
                        if(output.size() > 0){
                            v_itemID    = output.get(0).getItemID().toString();
                            v_itemCode  = output.get(0).getItemCode().toString();
                            v_itemDesc  = output.get(0).getItemDesc().toString();
                            v_itemBrand = output.get(0).getItemBrand().toString();
                            v_itemSpec  = output.get(0).getItemSpec().toString();

                            // reload item info information
                            pActivity.setTxt_ps_ItemID(v_itemID);
                            pActivity.setTxt_ps_ItemCode(v_itemCode);
                            pActivity.setTxt_ps_itemDesc(v_itemDesc);
                            pActivity.setTxt_ps_brand(v_itemBrand);
                            pActivity.setTxt_ps_spec(v_itemSpec);

                            // create array file size
                            vfs.clear();
                            vfs.add(output.get(0).getFS_Img1());
                            vfs.add(output.get(0).getFS_Img2());
                            vfs.add(output.get(0).getFS_Img3());

                            for(int iter=0; iter <3; iter ++){
                                final int idx = iter + 1;
                                // if file attachment size > 0
                                if(vfs.get(iter) > 0){
                                    try{
                                        // summon web service imgage per seeuence
                                        SoapPopulateInfoImgTaskAsync sri = new SoapPopulateInfoImgTaskAsync(new SoapPopulateInfoImgTaskAsync.SoapPopulateInfoImgTaskAsyncResponse() {
                                            @Override
                                            public void PostSentAction(List<DS_ItemImg> outputimg) {
                                                Log.d("[GudangGaram]", "SoapPopulateInfoImgTaskAsync :: PostSentAction Count : " + outputimg.size());
                                                if(outputimg.size() > 0) {
                                                    // load image
                                                    LoadImageView(idx, outputimg.get(0).getFileData());
                                                    // load information attachment id
                                                    if(outputimg.get(0).getAttachmentNo() == 1){
                                                        pActivity.setTxt_ps_attachmentID1(outputimg.get(0).getAttachmentID().toString());
                                                        pActivity.setTxt_ps_blobID1(outputimg.get(0).getImgID());
                                                    }
                                                    else if(outputimg.get(0).getAttachmentNo() == 2){
                                                        pActivity.setTxt_ps_attachmentID2(outputimg.get(0).getAttachmentID().toString());
                                                        pActivity.setTxt_ps_blobID2(outputimg.get(0).getImgID());
                                                    }
                                                    else if(outputimg.get(0).getAttachmentNo() == 3) {
                                                        pActivity.setTxt_ps_attachmentID3(outputimg.get(0).getAttachmentID().toString());
                                                        pActivity.setTxt_ps_blobID3(outputimg.get(0).getImgID());
                                                    }
                                                }
                                            }
                                        });
                                        sri.setAttribute(context, dbHelper,config,pType, pItemCode,idx);
                                        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                                            //work on sgs3 android 4.0.4
                                            sri.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                                        }
                                        else {
                                            sri.execute(); // work on sgs2 android 2.3
                                        }
                                    }
                                    catch(Exception e){
                                        Log.d("[GudangGaram]", "Exception : " + e.getMessage().toString());
                                    }
                                    finally {
                                    }
                                } // end if
                                else{
                                    // blank byte
                                    byte[] tByte = {0};
                                    LoadImageView(idx, tByte);
                                }
                            } // end for
                        } // end if
                        Log.d("[GudangGaram]", "SoapPopulateInfo :: PostSentAction Output : " + output.size());
                    } // end try
                    catch(Exception e){
                        Log.d("[GudangGaram]", "SoapPopulateInfo :: PostSentAction Output : " + output.size());
                    }
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config,pType, pItemCode);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapPopulateInfo :: Retrieve Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {
        }
        return TResult;
    }
}
