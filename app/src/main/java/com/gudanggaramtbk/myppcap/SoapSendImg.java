package com.gudanggaramtbk.myppcap;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luckym on 8/29/2018.
 */

public class SoapSendImg {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper    dbHelper;
    private String            DeviceID;
    CapturePSActivity         pActivity;

    // override constructor
    public SoapSendImg(SharedPreferences PConfig) {
        Log.d("[GudangGaram]", "SoapSendImg :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context) {
        Log.d("[GudangGaram]", "SoapSendImg :: SetContext");
        this.context = context;
    }
    public void setAttribute(String pDeviceID){
        this.DeviceID = pDeviceID;
    }
    public void setParentActivity(CapturePSActivity pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapSendImg :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Send(List<DS_ItemImg> lstDSI) {
        Log.d("[GudangGaram]", "SoapSendImg :: Send " + lstDSI.size());
        try{
            if(lstDSI.size() > 0){
                for(int idx=0; idx < lstDSI.size(); idx++){
                    // load info passed by
                    DS_ItemImg odii = new DS_ItemImg();
                    odii.setItemID(lstDSI.get(idx).getItemID().toString());
                    odii.setItemCode(lstDSI.get(idx).getItemCode().toString());
                    odii.setAttachmentID(lstDSI.get(idx).getAttachmentID());
                    odii.setLobID(lstDSI.get(idx).getLobID());
                    odii.setAttachmentNo(lstDSI.get(idx).getAttachmentNo());
                    odii.setFileData(lstDSI.get(idx).getFileData());
                    odii.setPicNik(lstDSI.get(idx).getPicNik().toString());

                    // call WS To send Header To Oracle
                    SoapSendImgTaskAsync SoapRequest = new SoapSendImgTaskAsync(new SoapSendImgTaskAsync.SoapSendImgTaskAsyncResponse() {
                        @Override
                        public void PostSentAction(String output) {
                            Log.d("[GudangGaram]", "PostSentAction : " + output);
                            // clear display
                            pActivity.flushInfo();
                            pActivity.flushImg();
                        }
                    });
                    SoapRequest.setParam(odii);
                    SoapRequest.setAttribute(context, dbHelper, config, DeviceID);
                    if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                        //work on sgs3 android 4.0.4
                        //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                        Log.d("[GudangGaram]", "Using HC Higer");
                        SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                    }
                    else {
                        Log.d("[GudangGaram]", "Using HC Lower");
                        SoapRequest.execute(); // work on sgs2 android 2.3
                    }
                } // end loop
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SentDOHeaderToOracle Exception " + e.getMessage().toString());
        }
    }
}
